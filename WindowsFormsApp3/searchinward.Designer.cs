﻿namespace WindowsFormsApp3
{
    partial class searchinward
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(searchinward));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.displaygrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.insertwardbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.insertwardid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.warddatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warddatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(3, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(408, 38);
            this.label2.TabIndex = 23;
            this.label2.Text = "As WARD_ID is the primary key of table, So searching\r\n will be performed against " +
    "it.";
            // 
            // displaygrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.displaygrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.displaygrid.BackgroundColor = System.Drawing.Color.White;
            this.displaygrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.displaygrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.displaygrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.displaygrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displaygrid.DoubleBuffered = true;
            this.displaygrid.EnableHeadersVisualStyles = false;
            this.displaygrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.Location = new System.Drawing.Point(-1, 57);
            this.displaygrid.Name = "displaygrid";
            this.displaygrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.displaygrid.Size = new System.Drawing.Size(564, 262);
            this.displaygrid.TabIndex = 43;
            // 
            // insertwardbutton
            // 
            this.insertwardbutton.ActiveBorderThickness = 1;
            this.insertwardbutton.ActiveCornerRadius = 20;
            this.insertwardbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.BackColor = System.Drawing.Color.White;
            this.insertwardbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insertwardbutton.BackgroundImage")));
            this.insertwardbutton.ButtonText = "Search";
            this.insertwardbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertwardbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleBorderThickness = 1;
            this.insertwardbutton.IdleCornerRadius = 20;
            this.insertwardbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleForecolor = System.Drawing.Color.White;
            this.insertwardbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.Location = new System.Drawing.Point(410, 326);
            this.insertwardbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.insertwardbutton.Name = "insertwardbutton";
            this.insertwardbutton.Size = new System.Drawing.Size(93, 51);
            this.insertwardbutton.TabIndex = 46;
            this.insertwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertwardbutton.Click += new System.EventHandler(this.insertwardbutton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 349);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 19);
            this.label1.TabIndex = 45;
            this.label1.Text = "WARD ID :";
            // 
            // insertwardid
            // 
            this.insertwardid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardid.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardid.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardid.HintText = "";
            this.insertwardid.isPassword = false;
            this.insertwardid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineThickness = 3;
            this.insertwardid.Location = new System.Drawing.Point(141, 330);
            this.insertwardid.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardid.MaxLength = 32767;
            this.insertwardid.Name = "insertwardid";
            this.insertwardid.Size = new System.Drawing.Size(204, 38);
            this.insertwardid.TabIndex = 44;
            this.insertwardid.Text = "Varchar";
            this.insertwardid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // warddatagrid
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.warddatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.warddatagrid.BackgroundColor = System.Drawing.Color.White;
            this.warddatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.warddatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.warddatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.warddatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.warddatagrid.DoubleBuffered = true;
            this.warddatagrid.EnableHeadersVisualStyles = false;
            this.warddatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.warddatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.warddatagrid.Location = new System.Drawing.Point(-1, 396);
            this.warddatagrid.Name = "warddatagrid";
            this.warddatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.warddatagrid.Size = new System.Drawing.Size(563, 150);
            this.warddatagrid.TabIndex = 47;
            // 
            // searchinward
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.warddatagrid);
            this.Controls.Add(this.insertwardbutton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.insertwardid);
            this.Controls.Add(this.displaygrid);
            this.Controls.Add(this.label2);
            this.Name = "searchinward";
            this.Size = new System.Drawing.Size(566, 586);
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warddatagrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid displaygrid;
        private Bunifu.Framework.UI.BunifuThinButton2 insertwardbutton;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardid;
        private Bunifu.Framework.UI.BunifuCustomDataGrid warddatagrid;
    }
}
