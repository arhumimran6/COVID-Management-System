﻿namespace WindowsFormsApp3
{
    partial class wardcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wardview = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.wardview)).BeginInit();
            this.SuspendLayout();
            // 
            // wardview
            // 
            this.wardview.BackgroundColor = System.Drawing.Color.White;
            this.wardview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wardview.Location = new System.Drawing.Point(0, 0);
            this.wardview.Name = "wardview";
            this.wardview.Size = new System.Drawing.Size(505, 319);
            this.wardview.TabIndex = 0;
            // 
            // wardcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.wardview);
            this.Name = "wardcontrol";
            this.Size = new System.Drawing.Size(505, 319);
            ((System.ComponentModel.ISupportInitialize)(this.wardview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView wardview;
    }
}
