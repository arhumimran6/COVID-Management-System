﻿namespace WindowsFormsApp3
{
    partial class admin_display_patient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_display_patient));
            this.patientdatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.displaypatientbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.patientdatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // patientdatagrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.patientdatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.patientdatagrid.BackgroundColor = System.Drawing.Color.White;
            this.patientdatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientdatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.patientdatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.patientdatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientdatagrid.DoubleBuffered = true;
            this.patientdatagrid.EnableHeadersVisualStyles = false;
            this.patientdatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.patientdatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.patientdatagrid.Location = new System.Drawing.Point(0, 0);
            this.patientdatagrid.Name = "patientdatagrid";
            this.patientdatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.patientdatagrid.Size = new System.Drawing.Size(588, 407);
            this.patientdatagrid.TabIndex = 2;
            // 
            // displaypatientbutton
            // 
            this.displaypatientbutton.ActiveBorderThickness = 1;
            this.displaypatientbutton.ActiveCornerRadius = 20;
            this.displaypatientbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.BackColor = System.Drawing.Color.White;
            this.displaypatientbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("displaypatientbutton.BackgroundImage")));
            this.displaypatientbutton.ButtonText = "display";
            this.displaypatientbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.displaypatientbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displaypatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.IdleBorderThickness = 1;
            this.displaypatientbutton.IdleCornerRadius = 20;
            this.displaypatientbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.IdleForecolor = System.Drawing.Color.White;
            this.displaypatientbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.Location = new System.Drawing.Point(463, 451);
            this.displaypatientbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.displaypatientbutton.Name = "displaypatientbutton";
            this.displaypatientbutton.Size = new System.Drawing.Size(93, 51);
            this.displaypatientbutton.TabIndex = 96;
            this.displaypatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.displaypatientbutton.Click += new System.EventHandler(this.updatepatientbutton_Click);
            // 
            // admin_display_patient
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.displaypatientbutton);
            this.Controls.Add(this.patientdatagrid);
            this.Name = "admin_display_patient";
            this.Size = new System.Drawing.Size(588, 522);
            ((System.ComponentModel.ISupportInitialize)(this.patientdatagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomDataGrid patientdatagrid;
        private Bunifu.Framework.UI.BunifuThinButton2 displaypatientbutton;
    }
}
