﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp3
{
    public partial class updatewardcontrol : UserControl
    {
        public updatewardcontrol()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
        private void updatewardcontrol_Load(object sender, EventArgs e)
        {

        }

        private void withpatientcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withnoofventilators_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void withnoofnurses_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void withnoofdoctors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withstaffcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void withnoofwards_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withhaddress_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void withhname_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withhid_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void updatepatientcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofventilators_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofnurses_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void updatestaffcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofdoctors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofwards_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void updatehospitaladdress_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void updatehospitalname_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void updatehospitalid_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void updatenooflabs_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void updatenooftestkits_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void withnooftestkits_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withnooflabs_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofvisitors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void withnoofvisitors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void updatebutton_Click(object sender, EventArgs e)
        {
            sqlcon.Open();

            string q_update_Wid = "update ward set WARD_ID='" + withwid.Text + "' where WARD_ID='" + updatewardid.Text + "'";
            SqlCommand cmd_update_Wid = new SqlCommand(q_update_Wid, sqlcon);
            cmd_update_Wid.ExecuteNonQuery();

            string q_update_wname = "update ward set W_NAME='" + withwname.Text + "'where W_NAME='" + updatewardname.Text + "'";
            SqlCommand cmd_update_wname = new SqlCommand(q_update_wname, sqlcon);
            cmd_update_wname.ExecuteNonQuery();

            string q_update_waddress = "update ward set W_ADDRESS='" + withwaddress.Text + "'where W_ADDRESS='" + updatewardaddress.Text + "'";
            SqlCommand cmd_update_waddress = new SqlCommand(q_update_waddress, sqlcon);
            cmd_update_waddress.ExecuteNonQuery();

            string q_update_staffcount = "update ward set STAFF_COUNT='" + withstaffcount.Text + "'where STAFF_COUNT='" + updatestaffcount.Text + "'";
            SqlCommand cmd_update_staffcount = new SqlCommand(q_update_staffcount, sqlcon);
            cmd_update_staffcount.ExecuteNonQuery();

            string q_update_numberofdoctors = "update ward set NUM_OF_DOC='" + withnoofdoctors.Text + "'where NUM_OF_DOC='" + updatenoofdoctors.Text + "'";
            SqlCommand cmd_update_numberofdoctors = new SqlCommand(q_update_numberofdoctors, sqlcon);
            cmd_update_numberofdoctors.ExecuteNonQuery();

            string q_update_numberofnurses = "update ward set NUM_OF_NURSES='" + withnoofnurses.Text + "'where NUM_OF_NURSES='" + updatenoofnurses.Text + "'";
            SqlCommand cmd_update_numberofnurses = new SqlCommand(q_update_numberofnurses, sqlcon);
            cmd_update_numberofnurses.ExecuteNonQuery();

            string q_update_numberofventilatorsperward = "update ward set NUM_OF_VENTILATORS_PER_WARD='" + withnoofventilatorsperward.Text + "'where NUM_OF_VENTILATORS_PER_WARD='" + updatenoofventilatorsperward.Text + "'";
            SqlCommand cmd_update_numberofventilatorsperward = new SqlCommand(q_update_numberofventilatorsperward, sqlcon);
            cmd_update_numberofventilatorsperward.ExecuteNonQuery();

            sqlcon.Close();
            MessageBox.Show("Updated Successfully");
        }
    }
}
