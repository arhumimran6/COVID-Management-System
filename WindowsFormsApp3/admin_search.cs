﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class admin_search : Form
    {
        public admin_search()
        {
            InitializeComponent();
            sidepanel1.Top = searchadminhospitalbutton.Top;
            sidepanel1.Height = searchadminhospitalbutton.Height;
            searchinpatient1.Hide();
            searchinward1.Hide();
        }

        private void insertadminhospitalbutton_Click(object sender, EventArgs e)
        {
            sidepanel1.Top = searchadminhospitalbutton.Top;
            sidepanel1.Height = searchadminhospitalbutton.Height;
            searchinward1.Hide();
            searchinpatient1.Hide();
            searchinhospital1.Show();
        }

        private void searchadminwardbutton_Click(object sender, EventArgs e)
        {
            sidepanel1.Top = searchadminwardbutton.Top;
            sidepanel1.Height = searchadminwardbutton.Height;
            searchinpatient1.Hide();
            searchinhospital1.Hide();
            searchinward1.Show();
        }

        private void searchadminpatientbutton_Click(object sender, EventArgs e)
        {
            sidepanel1.Top = searchadminpatientbutton.Top;
            sidepanel1.Height = searchadminpatientbutton.Height;
            searchinward1.Hide();
            searchinhospital1.Hide();
            searchinpatient1.Show();
        }
    }
}
