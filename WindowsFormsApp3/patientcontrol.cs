﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp3
{
    public partial class patientcontrol : UserControl
    {
        public patientcontrol()
        {
            InitializeComponent();
        }

        private void insertpatientbutton_Click(object sender, EventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
            if (insertPid.Text != "Varchar" && insertPname.Text != "Varchar" && insertPcontact.Text != "Varchar" && insertPemail.Text != "Varchar" && insertcoronastatus.Text != "Varchar e.g (positive or negative)" && insertpatienthospitalid.Text != "Varchar e.g (HXXX)" && insertpatientwardid.Text != "Varchar e.g (WXXX)" && insertPcond.Text != "Varchar e.g (Fair, critical, Normal)" && insertpatienttestno.Text != "Varchar e.g (TXXX)" && insertPbedno.Text != "INT" && insertPage.Text != "INT" && insertPgender.Text != "Varchar" && insertPhistdesc.Text != "Varchar e.g(any disease before)")
            {
                string q_insert_in_Patient = "insert into patient(P_ID,P_NAME,P_CONTACT,P_ADDRESS,CORONA_STATUS,HOSPITAL_ID,WARD_NO,CONDITION,TEST_NO,BED_NO,AGE,GENDER,HISTORY_DESCRIPTION)values('" + insertPid.Text + "','" + insertPname.Text + "','" + insertPcontact.Text + "','" + insertPaddress.Text + "','" + insertPemail.Text + "','" + insertcoronastatus.Text + "','" + insertpatienthospitalid.Text + "','" + insertpatientwardid.Text + "','" + insertPcond.Text + "','" + insertpatienttestno.Text + "','" + insertPbedno.Text.ToString() + "','" + insertPage.Text.ToString() + "','" + insertPgender.Text + "','" + insertPhistdesc.Text + "')";
                SqlCommand cmd = new SqlCommand(q_insert_in_Patient, sqlcon);
                cmd.ExecuteNonQuery();
                sqlcon.Close();
                insertPid.Text = "";
                insertPname.Text = "";
                insertPcontact.Text = "";
                insertPaddress.Text = "";
                insertPemail.Text = "";
                insertcoronastatus.Text = "";
                insertPcond.Text = "";
                insertPbedno.Text = "";
                insertPage.Text = "";
                insertPgender.Text = "";
                insertPhistdesc.Text = "";
                insertpatienthospitalid.Text = "";
                insertpatientwardid.Text = "";
                insertpatienttestno.Text = "";
                MessageBox.Show("Data Written Successfully");
            }
            else
                MessageBox.Show("Please Fill out all the Boxes :)");

        }
    }
}
