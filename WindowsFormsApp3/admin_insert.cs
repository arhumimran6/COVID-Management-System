﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class admin_insert : Form
    {
        public admin_insert()
        {
            InitializeComponent();
            patientcontrol1.Hide();
            insertwardcontrol1.Hide();
            sidepanel1.Top = insertadminhospitalbutton.Top;
            sidepanel1.Height = insertadminhospitalbutton.Height;
            inserthospitalcontrol1.Show();
        }

        SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
        private void insertadminhomebutton_Click(object sender, EventArgs e)
        {
            patientcontrol1.Hide();
            insertwardcontrol1.Hide();
            sidepanel1.Top = insertadminhospitalbutton.Top;
            sidepanel1.Height = insertadminhospitalbutton.Height;
            inserthospitalcontrol1.Show();

        }

        private void insertadminwardbutton_Click(object sender, EventArgs e)
        {
            patientcontrol1.Hide();
            inserthospitalcontrol1.Hide();
            sidepanel1.Top = insertadminwardbutton.Top;
            sidepanel1.Height = insertadminwardbutton.Height;
            insertwardcontrol1.Show();
        }

        private void insertadminpatientbutton_Click(object sender, EventArgs e)
        {
            insertwardcontrol1.Hide();
            inserthospitalcontrol1.Hide();
            sidepanel1.Top = insertadminpatientbutton.Top;
            sidepanel1.Height = insertadminpatientbutton.Height;
            patientcontrol1.Show();
        }

        private void inserthospitalcontrol1_Load(object sender, EventArgs e)
        {

        }
    }
}
