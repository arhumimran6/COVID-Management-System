﻿namespace WindowsFormsApp3
{
    partial class user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(user));
            this.panel1 = new System.Windows.Forms.Panel();
            this.displayuserhospital = new System.Windows.Forms.Button();
            this.sidepanel2 = new System.Windows.Forms.Panel();
            this.displayuserpatientbutton = new System.Windows.Forms.Button();
            this.displayuserwardbutton = new System.Windows.Forms.Button();
            this.userhomebutton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.admin_display_ward1 = new WindowsFormsApp3.admin_display_ward();
            this.admin_display_patient1 = new WindowsFormsApp3.admin_display_patient();
            this.admin_display_hospital2 = new WindowsFormsApp3.admin_display_hospital();
            this.home1 = new WindowsFormsApp3.home();
            this.admin_display_hospital1 = new WindowsFormsApp3.admin_display_hospital();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.displayuserhospital);
            this.panel1.Controls.Add(this.sidepanel2);
            this.panel1.Controls.Add(this.displayuserpatientbutton);
            this.panel1.Controls.Add(this.displayuserwardbutton);
            this.panel1.Controls.Add(this.userhomebutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 602);
            this.panel1.TabIndex = 4;
            // 
            // displayuserhospital
            // 
            this.displayuserhospital.FlatAppearance.BorderSize = 0;
            this.displayuserhospital.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayuserhospital.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayuserhospital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayuserhospital.Location = new System.Drawing.Point(12, 311);
            this.displayuserhospital.Name = "displayuserhospital";
            this.displayuserhospital.Size = new System.Drawing.Size(206, 55);
            this.displayuserhospital.TabIndex = 5;
            this.displayuserhospital.Text = "   Hospital";
            this.displayuserhospital.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayuserhospital.UseVisualStyleBackColor = true;
            this.displayuserhospital.Click += new System.EventHandler(this.displayuserhospital_Click);
            // 
            // sidepanel2
            // 
            this.sidepanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel2.Location = new System.Drawing.Point(0, 128);
            this.sidepanel2.Name = "sidepanel2";
            this.sidepanel2.Size = new System.Drawing.Size(11, 55);
            this.sidepanel2.TabIndex = 4;
            // 
            // displayuserpatientbutton
            // 
            this.displayuserpatientbutton.FlatAppearance.BorderSize = 0;
            this.displayuserpatientbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayuserpatientbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayuserpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayuserpatientbutton.Location = new System.Drawing.Point(15, 250);
            this.displayuserpatientbutton.Name = "displayuserpatientbutton";
            this.displayuserpatientbutton.Size = new System.Drawing.Size(200, 55);
            this.displayuserpatientbutton.TabIndex = 3;
            this.displayuserpatientbutton.Text = "   Patient";
            this.displayuserpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayuserpatientbutton.UseVisualStyleBackColor = true;
            this.displayuserpatientbutton.Click += new System.EventHandler(this.displayuserpatientbutton_Click);
            // 
            // displayuserwardbutton
            // 
            this.displayuserwardbutton.FlatAppearance.BorderSize = 0;
            this.displayuserwardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayuserwardbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayuserwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayuserwardbutton.Location = new System.Drawing.Point(15, 189);
            this.displayuserwardbutton.Name = "displayuserwardbutton";
            this.displayuserwardbutton.Size = new System.Drawing.Size(200, 55);
            this.displayuserwardbutton.TabIndex = 2;
            this.displayuserwardbutton.Text = "   Ward";
            this.displayuserwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayuserwardbutton.UseVisualStyleBackColor = true;
            this.displayuserwardbutton.Click += new System.EventHandler(this.displayuserwardbutton_Click);
            // 
            // userhomebutton
            // 
            this.userhomebutton.FlatAppearance.BorderSize = 0;
            this.userhomebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userhomebutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userhomebutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.userhomebutton.Location = new System.Drawing.Point(12, 128);
            this.userhomebutton.Name = "userhomebutton";
            this.userhomebutton.Size = new System.Drawing.Size(206, 55);
            this.userhomebutton.TabIndex = 1;
            this.userhomebutton.Text = "   Home";
            this.userhomebutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.userhomebutton.UseVisualStyleBackColor = true;
            this.userhomebutton.Click += new System.EventHandler(this.displayuserhospitalbutton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.admin_display_ward1);
            this.panel2.Controls.Add(this.admin_display_patient1);
            this.panel2.Controls.Add(this.admin_display_hospital2);
            this.panel2.Controls.Add(this.home1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(610, 602);
            this.panel2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(221, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 36);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hello User";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(409, 573);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(189, 20);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Go Back to Login screen";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // admin_display_ward1
            // 
            this.admin_display_ward1.BackColor = System.Drawing.Color.White;
            this.admin_display_ward1.Location = new System.Drawing.Point(0, 44);
            this.admin_display_ward1.Name = "admin_display_ward1";
            this.admin_display_ward1.Size = new System.Drawing.Size(610, 522);
            this.admin_display_ward1.TabIndex = 3;
            // 
            // admin_display_patient1
            // 
            this.admin_display_patient1.BackColor = System.Drawing.Color.White;
            this.admin_display_patient1.Location = new System.Drawing.Point(0, 44);
            this.admin_display_patient1.Name = "admin_display_patient1";
            this.admin_display_patient1.Size = new System.Drawing.Size(588, 522);
            this.admin_display_patient1.TabIndex = 2;
            // 
            // admin_display_hospital2
            // 
            this.admin_display_hospital2.BackColor = System.Drawing.Color.White;
            this.admin_display_hospital2.Location = new System.Drawing.Point(0, 44);
            this.admin_display_hospital2.Name = "admin_display_hospital2";
            this.admin_display_hospital2.Size = new System.Drawing.Size(610, 514);
            this.admin_display_hospital2.TabIndex = 1;
            // 
            // home1
            // 
            this.home1.BackColor = System.Drawing.Color.White;
            this.home1.Location = new System.Drawing.Point(0, 44);
            this.home1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.home1.Name = "home1";
            this.home1.Size = new System.Drawing.Size(566, 430);
            this.home1.TabIndex = 0;
            // 
            // admin_display_hospital1
            // 
            this.admin_display_hospital1.BackColor = System.Drawing.Color.White;
            this.admin_display_hospital1.Location = new System.Drawing.Point(276, 155);
            this.admin_display_hospital1.Name = "admin_display_hospital1";
            this.admin_display_hospital1.Size = new System.Drawing.Size(569, 501);
            this.admin_display_hospital1.TabIndex = 1;
            // 
            // user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(831, 602);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.admin_display_hospital1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "user";
            this.Text = "user";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel sidepanel2;
        private System.Windows.Forms.Button displayuserpatientbutton;
        private System.Windows.Forms.Button displayuserwardbutton;
        private System.Windows.Forms.Button userhomebutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button displayuserhospital;
        private System.Windows.Forms.Panel panel2;
        private home home1;
        private admin_display_hospital admin_display_hospital2;
        private admin_display_hospital admin_display_hospital1;
        private admin_display_ward admin_display_ward1;
        private admin_display_patient admin_display_patient1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}