﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class user : Form
    {
        public user()
        {
            InitializeComponent();
            sidepanel2.Top = userhomebutton.Top;
            sidepanel2.Height = userhomebutton.Height;
            home1.Show();
            admin_display_hospital1.Hide();
            admin_display_hospital2.Hide();
            admin_display_patient1.Hide();
            admin_display_ward1.Hide();
        }

        private void displayuserhospitalbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = userhomebutton.Top;
            sidepanel2.Height = userhomebutton.Height;
            admin_display_hospital2.Hide();
            admin_display_patient1.Hide();
            admin_display_ward1.Hide();
            home1.Show();
        }

        private void displayuserwardbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayuserwardbutton.Top;
            sidepanel2.Height = displayuserwardbutton.Height;
            admin_display_hospital2.Hide();
            admin_display_patient1.Hide();
            home1.Hide();
            admin_display_ward1.Show();
        }

        private void displayuserpatientbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayuserpatientbutton.Top;
            sidepanel2.Height = displayuserpatientbutton.Height;
            home1.Hide();
            admin_display_ward1.Hide();
            admin_display_hospital2.Hide();
            admin_display_patient1.Show();
        }

        private void displayuserhospital_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayuserhospital.Top;
            sidepanel2.Height = displayuserhospital.Height;
            home1.Hide();
            admin_display_ward1.Hide();
            admin_display_patient1.Hide();
            admin_display_hospital2.Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            Form1 loginform = new Form1();
            loginform.Show();
        }
    }
}
