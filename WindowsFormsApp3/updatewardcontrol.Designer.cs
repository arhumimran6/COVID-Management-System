﻿namespace WindowsFormsApp3
{
    partial class updatewardcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(updatewardcontrol));
            this.label22 = new System.Windows.Forms.Label();
            this.withavailablebeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withnoofventilatorsperward = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.withnoofnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label17 = new System.Windows.Forms.Label();
            this.withnoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withstaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.withnoofbeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withwaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.withwname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withwid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.updateavailablebeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.updatenoofventilatorsperward = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.updatenoofnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.updatestaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.updatenoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.updatenoofbeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.updatewardaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.updatewardname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.updatewardid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.updatebutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label25 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(321, 485);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(102, 16);
            this.label22.TabIndex = 111;
            this.label22.Text = "Updated with :";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // withavailablebeds
            // 
            this.withavailablebeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withavailablebeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withavailablebeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withavailablebeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withavailablebeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withavailablebeds.ForeColor = System.Drawing.Color.DimGray;
            this.withavailablebeds.HintForeColor = System.Drawing.Color.Empty;
            this.withavailablebeds.HintText = "";
            this.withavailablebeds.isPassword = false;
            this.withavailablebeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withavailablebeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withavailablebeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withavailablebeds.LineThickness = 3;
            this.withavailablebeds.Location = new System.Drawing.Point(432, 473);
            this.withavailablebeds.Margin = new System.Windows.Forms.Padding(5);
            this.withavailablebeds.MaxLength = 32767;
            this.withavailablebeds.Name = "withavailablebeds";
            this.withavailablebeds.Size = new System.Drawing.Size(170, 38);
            this.withavailablebeds.TabIndex = 110;
            this.withavailablebeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withavailablebeds.OnValueChanged += new System.EventHandler(this.withpatientcount_OnValueChanged);
            // 
            // withnoofventilatorsperward
            // 
            this.withnoofventilatorsperward.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofventilatorsperward.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofventilatorsperward.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofventilatorsperward.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofventilatorsperward.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofventilatorsperward.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofventilatorsperward.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofventilatorsperward.HintText = "";
            this.withnoofventilatorsperward.isPassword = false;
            this.withnoofventilatorsperward.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilatorsperward.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilatorsperward.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilatorsperward.LineThickness = 3;
            this.withnoofventilatorsperward.Location = new System.Drawing.Point(430, 418);
            this.withnoofventilatorsperward.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofventilatorsperward.MaxLength = 32767;
            this.withnoofventilatorsperward.Name = "withnoofventilatorsperward";
            this.withnoofventilatorsperward.Size = new System.Drawing.Size(168, 38);
            this.withnoofventilatorsperward.TabIndex = 109;
            this.withnoofventilatorsperward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofventilatorsperward.OnValueChanged += new System.EventHandler(this.withnoofventilators_OnValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label19.Location = new System.Drawing.Point(320, 430);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 16);
            this.label19.TabIndex = 108;
            this.label19.Text = "Updated with :";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(320, 379);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 16);
            this.label20.TabIndex = 107;
            this.label20.Text = "Updated with :";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // withnoofnurses
            // 
            this.withnoofnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofnurses.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofnurses.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofnurses.HintText = "";
            this.withnoofnurses.isPassword = false;
            this.withnoofnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineThickness = 3;
            this.withnoofnurses.Location = new System.Drawing.Point(431, 367);
            this.withnoofnurses.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofnurses.MaxLength = 32767;
            this.withnoofnurses.Name = "withnoofnurses";
            this.withnoofnurses.Size = new System.Drawing.Size(170, 38);
            this.withnoofnurses.TabIndex = 106;
            this.withnoofnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofnurses.OnValueChanged += new System.EventHandler(this.withnoofnurses_OnValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(320, 328);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 16);
            this.label17.TabIndex = 105;
            this.label17.Text = "Updated with :";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // withnoofdoctors
            // 
            this.withnoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofdoctors.HintText = "";
            this.withnoofdoctors.isPassword = false;
            this.withnoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineThickness = 3;
            this.withnoofdoctors.Location = new System.Drawing.Point(431, 316);
            this.withnoofdoctors.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofdoctors.MaxLength = 32767;
            this.withnoofdoctors.Name = "withnoofdoctors";
            this.withnoofdoctors.Size = new System.Drawing.Size(170, 38);
            this.withnoofdoctors.TabIndex = 104;
            this.withnoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofdoctors.OnValueChanged += new System.EventHandler(this.withnoofdoctors_OnValueChanged);
            // 
            // withstaffcount
            // 
            this.withstaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withstaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withstaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withstaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withstaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withstaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.withstaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.withstaffcount.HintText = "";
            this.withstaffcount.isPassword = false;
            this.withstaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineThickness = 3;
            this.withstaffcount.Location = new System.Drawing.Point(430, 266);
            this.withstaffcount.Margin = new System.Windows.Forms.Padding(5);
            this.withstaffcount.MaxLength = 32767;
            this.withstaffcount.Name = "withstaffcount";
            this.withstaffcount.Size = new System.Drawing.Size(168, 38);
            this.withstaffcount.TabIndex = 103;
            this.withstaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withstaffcount.OnValueChanged += new System.EventHandler(this.withstaffcount_OnValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(320, 278);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 16);
            this.label18.TabIndex = 102;
            this.label18.Text = "Updated with :";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(320, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 16);
            this.label15.TabIndex = 101;
            this.label15.Text = "Updated with :";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // withnoofbeds
            // 
            this.withnoofbeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofbeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofbeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofbeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofbeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofbeds.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofbeds.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofbeds.HintText = "";
            this.withnoofbeds.isPassword = false;
            this.withnoofbeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofbeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofbeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofbeds.LineThickness = 3;
            this.withnoofbeds.Location = new System.Drawing.Point(431, 215);
            this.withnoofbeds.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofbeds.MaxLength = 32767;
            this.withnoofbeds.Name = "withnoofbeds";
            this.withnoofbeds.Size = new System.Drawing.Size(170, 38);
            this.withnoofbeds.TabIndex = 100;
            this.withnoofbeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofbeds.OnValueChanged += new System.EventHandler(this.withnoofwards_OnValueChanged);
            // 
            // withwaddress
            // 
            this.withwaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withwaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withwaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withwaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withwaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withwaddress.ForeColor = System.Drawing.Color.DimGray;
            this.withwaddress.HintForeColor = System.Drawing.Color.Empty;
            this.withwaddress.HintText = "";
            this.withwaddress.isPassword = false;
            this.withwaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwaddress.LineThickness = 3;
            this.withwaddress.Location = new System.Drawing.Point(430, 160);
            this.withwaddress.Margin = new System.Windows.Forms.Padding(5);
            this.withwaddress.MaxLength = 32767;
            this.withwaddress.Name = "withwaddress";
            this.withwaddress.Size = new System.Drawing.Size(168, 38);
            this.withwaddress.TabIndex = 99;
            this.withwaddress.Text = "Varchar";
            this.withwaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withwaddress.OnValueChanged += new System.EventHandler(this.withhaddress_OnValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(320, 172);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 16);
            this.label16.TabIndex = 98;
            this.label16.Text = "Updated with :";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(320, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 16);
            this.label14.TabIndex = 97;
            this.label14.Text = "Updated with :";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // withwname
            // 
            this.withwname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withwname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withwname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withwname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withwname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withwname.ForeColor = System.Drawing.Color.DimGray;
            this.withwname.HintForeColor = System.Drawing.Color.Empty;
            this.withwname.HintText = "";
            this.withwname.isPassword = false;
            this.withwname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwname.LineThickness = 3;
            this.withwname.Location = new System.Drawing.Point(431, 110);
            this.withwname.Margin = new System.Windows.Forms.Padding(5);
            this.withwname.MaxLength = 32767;
            this.withwname.Name = "withwname";
            this.withwname.Size = new System.Drawing.Size(170, 38);
            this.withwname.TabIndex = 96;
            this.withwname.Text = "Varchar";
            this.withwname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withwname.OnValueChanged += new System.EventHandler(this.withhname_OnValueChanged);
            // 
            // withwid
            // 
            this.withwid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withwid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withwid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withwid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withwid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withwid.ForeColor = System.Drawing.Color.DimGray;
            this.withwid.HintForeColor = System.Drawing.Color.Empty;
            this.withwid.HintText = "";
            this.withwid.isPassword = false;
            this.withwid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withwid.LineThickness = 3;
            this.withwid.Location = new System.Drawing.Point(430, 65);
            this.withwid.Margin = new System.Windows.Forms.Padding(5);
            this.withwid.MaxLength = 32767;
            this.withwid.Name = "withwid";
            this.withwid.Size = new System.Drawing.Size(168, 38);
            this.withwid.TabIndex = 95;
            this.withwid.Text = "Varchar";
            this.withwid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withwid.OnValueChanged += new System.EventHandler(this.withhid_OnValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(320, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 94;
            this.label1.Text = "Updated with :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(1, 485);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 89;
            this.label10.Text = "Available Beds :";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // updateavailablebeds
            // 
            this.updateavailablebeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updateavailablebeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updateavailablebeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updateavailablebeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updateavailablebeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updateavailablebeds.ForeColor = System.Drawing.Color.DimGray;
            this.updateavailablebeds.HintForeColor = System.Drawing.Color.Empty;
            this.updateavailablebeds.HintText = "";
            this.updateavailablebeds.isPassword = false;
            this.updateavailablebeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updateavailablebeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updateavailablebeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updateavailablebeds.LineThickness = 3;
            this.updateavailablebeds.Location = new System.Drawing.Point(127, 473);
            this.updateavailablebeds.Margin = new System.Windows.Forms.Padding(5);
            this.updateavailablebeds.MaxLength = 32767;
            this.updateavailablebeds.Name = "updateavailablebeds";
            this.updateavailablebeds.Size = new System.Drawing.Size(170, 38);
            this.updateavailablebeds.TabIndex = 88;
            this.updateavailablebeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updateavailablebeds.OnValueChanged += new System.EventHandler(this.updatepatientcount_OnValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(1, 428);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 32);
            this.label9.TabIndex = 87;
            this.label9.Text = "No. of ventilators\r\nper ward:\r\n";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // updatenoofventilatorsperward
            // 
            this.updatenoofventilatorsperward.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofventilatorsperward.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofventilatorsperward.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofventilatorsperward.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofventilatorsperward.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofventilatorsperward.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofventilatorsperward.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofventilatorsperward.HintText = "";
            this.updatenoofventilatorsperward.isPassword = false;
            this.updatenoofventilatorsperward.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilatorsperward.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilatorsperward.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilatorsperward.LineThickness = 3;
            this.updatenoofventilatorsperward.Location = new System.Drawing.Point(127, 416);
            this.updatenoofventilatorsperward.Margin = new System.Windows.Forms.Padding(5);
            this.updatenoofventilatorsperward.MaxLength = 32767;
            this.updatenoofventilatorsperward.Name = "updatenoofventilatorsperward";
            this.updatenoofventilatorsperward.Size = new System.Drawing.Size(170, 38);
            this.updatenoofventilatorsperward.TabIndex = 86;
            this.updatenoofventilatorsperward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofventilatorsperward.OnValueChanged += new System.EventHandler(this.updatenoofventilators_OnValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(1, 379);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 16);
            this.label8.TabIndex = 85;
            this.label8.Text = "No. of Nurses :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // updatenoofnurses
            // 
            this.updatenoofnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofnurses.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofnurses.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofnurses.HintText = "";
            this.updatenoofnurses.isPassword = false;
            this.updatenoofnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineThickness = 3;
            this.updatenoofnurses.Location = new System.Drawing.Point(127, 367);
            this.updatenoofnurses.Margin = new System.Windows.Forms.Padding(5);
            this.updatenoofnurses.MaxLength = 32767;
            this.updatenoofnurses.Name = "updatenoofnurses";
            this.updatenoofnurses.Size = new System.Drawing.Size(170, 38);
            this.updatenoofnurses.TabIndex = 84;
            this.updatenoofnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofnurses.OnValueChanged += new System.EventHandler(this.updatenoofnurses_OnValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(1, 278);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 16);
            this.label7.TabIndex = 83;
            this.label7.Text = "Staff Count :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // updatestaffcount
            // 
            this.updatestaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatestaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatestaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatestaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatestaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatestaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.updatestaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.updatestaffcount.HintText = "";
            this.updatestaffcount.isPassword = false;
            this.updatestaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineThickness = 3;
            this.updatestaffcount.Location = new System.Drawing.Point(127, 266);
            this.updatestaffcount.Margin = new System.Windows.Forms.Padding(5);
            this.updatestaffcount.MaxLength = 32767;
            this.updatestaffcount.Name = "updatestaffcount";
            this.updatestaffcount.Size = new System.Drawing.Size(170, 38);
            this.updatestaffcount.TabIndex = 82;
            this.updatestaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatestaffcount.OnValueChanged += new System.EventHandler(this.updatestaffcount_OnValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(1, 328);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 16);
            this.label6.TabIndex = 81;
            this.label6.Text = "No. of doctors  :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // updatenoofdoctors
            // 
            this.updatenoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofdoctors.HintText = "";
            this.updatenoofdoctors.isPassword = false;
            this.updatenoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineThickness = 3;
            this.updatenoofdoctors.Location = new System.Drawing.Point(127, 316);
            this.updatenoofdoctors.Margin = new System.Windows.Forms.Padding(5);
            this.updatenoofdoctors.MaxLength = 32767;
            this.updatenoofdoctors.Name = "updatenoofdoctors";
            this.updatenoofdoctors.Size = new System.Drawing.Size(170, 38);
            this.updatenoofdoctors.TabIndex = 80;
            this.updatenoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofdoctors.OnValueChanged += new System.EventHandler(this.updatenoofdoctors_OnValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(1, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 79;
            this.label5.Text = "No. of Beds :";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // updatenoofbeds
            // 
            this.updatenoofbeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofbeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofbeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofbeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofbeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofbeds.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofbeds.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofbeds.HintText = "";
            this.updatenoofbeds.isPassword = false;
            this.updatenoofbeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofbeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofbeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofbeds.LineThickness = 3;
            this.updatenoofbeds.Location = new System.Drawing.Point(127, 215);
            this.updatenoofbeds.Margin = new System.Windows.Forms.Padding(5);
            this.updatenoofbeds.MaxLength = 32767;
            this.updatenoofbeds.Name = "updatenoofbeds";
            this.updatenoofbeds.Size = new System.Drawing.Size(170, 38);
            this.updatenoofbeds.TabIndex = 78;
            this.updatenoofbeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofbeds.OnValueChanged += new System.EventHandler(this.updatenoofwards_OnValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(1, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 16);
            this.label4.TabIndex = 77;
            this.label4.Text = "Ward Address :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // updatewardaddress
            // 
            this.updatewardaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatewardaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatewardaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatewardaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatewardaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatewardaddress.ForeColor = System.Drawing.Color.DimGray;
            this.updatewardaddress.HintForeColor = System.Drawing.Color.Empty;
            this.updatewardaddress.HintText = "";
            this.updatewardaddress.isPassword = false;
            this.updatewardaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardaddress.LineThickness = 3;
            this.updatewardaddress.Location = new System.Drawing.Point(127, 160);
            this.updatewardaddress.Margin = new System.Windows.Forms.Padding(5);
            this.updatewardaddress.MaxLength = 32767;
            this.updatewardaddress.Name = "updatewardaddress";
            this.updatewardaddress.Size = new System.Drawing.Size(170, 38);
            this.updatewardaddress.TabIndex = 76;
            this.updatewardaddress.Text = "Varchar";
            this.updatewardaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatewardaddress.OnValueChanged += new System.EventHandler(this.updatehospitaladdress_OnValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(1, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 16);
            this.label3.TabIndex = 75;
            this.label3.Text = "Ward Name :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // updatewardname
            // 
            this.updatewardname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatewardname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatewardname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatewardname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatewardname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatewardname.ForeColor = System.Drawing.Color.DimGray;
            this.updatewardname.HintForeColor = System.Drawing.Color.Empty;
            this.updatewardname.HintText = "";
            this.updatewardname.isPassword = false;
            this.updatewardname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardname.LineThickness = 3;
            this.updatewardname.Location = new System.Drawing.Point(127, 110);
            this.updatewardname.Margin = new System.Windows.Forms.Padding(5);
            this.updatewardname.MaxLength = 32767;
            this.updatewardname.Name = "updatewardname";
            this.updatewardname.Size = new System.Drawing.Size(170, 38);
            this.updatewardname.TabIndex = 74;
            this.updatewardname.Text = "Varchar";
            this.updatewardname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatewardname.OnValueChanged += new System.EventHandler(this.updatehospitalname_OnValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(3, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.TabIndex = 73;
            this.label2.Text = "Ward ID :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // updatewardid
            // 
            this.updatewardid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatewardid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatewardid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatewardid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatewardid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatewardid.ForeColor = System.Drawing.Color.DimGray;
            this.updatewardid.HintForeColor = System.Drawing.Color.Empty;
            this.updatewardid.HintText = "";
            this.updatewardid.isPassword = false;
            this.updatewardid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatewardid.LineThickness = 3;
            this.updatewardid.Location = new System.Drawing.Point(126, 65);
            this.updatewardid.Margin = new System.Windows.Forms.Padding(5);
            this.updatewardid.MaxLength = 32767;
            this.updatewardid.Name = "updatewardid";
            this.updatewardid.Size = new System.Drawing.Size(168, 38);
            this.updatewardid.TabIndex = 72;
            this.updatewardid.Text = "Varchar";
            this.updatewardid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatewardid.OnValueChanged += new System.EventHandler(this.updatehospitalid_OnValueChanged);
            // 
            // updatebutton
            // 
            this.updatebutton.ActiveBorderThickness = 1;
            this.updatebutton.ActiveCornerRadius = 20;
            this.updatebutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.BackColor = System.Drawing.Color.White;
            this.updatebutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updatebutton.BackgroundImage")));
            this.updatebutton.ButtonText = "Update";
            this.updatebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatebutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatebutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.IdleBorderThickness = 1;
            this.updatebutton.IdleCornerRadius = 20;
            this.updatebutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.IdleForecolor = System.Drawing.Color.White;
            this.updatebutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.Location = new System.Drawing.Point(584, 584);
            this.updatebutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updatebutton.Name = "updatebutton";
            this.updatebutton.Size = new System.Drawing.Size(93, 51);
            this.updatebutton.TabIndex = 120;
            this.updatebutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updatebutton.Click += new System.EventHandler(this.updatebutton_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label25.Location = new System.Drawing.Point(4, 10);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(287, 32);
            this.label25.TabIndex = 121;
            this.label25.Text = "Ward Updation Page";
            // 
            // updatewardcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label25);
            this.Controls.Add(this.updatebutton);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.withavailablebeds);
            this.Controls.Add(this.withnoofventilatorsperward);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.withnoofnurses);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.withnoofdoctors);
            this.Controls.Add(this.withstaffcount);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.withnoofbeds);
            this.Controls.Add(this.withwaddress);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.withwname);
            this.Controls.Add(this.withwid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.updateavailablebeds);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updatenoofventilatorsperward);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.updatenoofnurses);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.updatestaffcount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.updatenoofdoctors);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.updatenoofbeds);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.updatewardaddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.updatewardname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.updatewardid);
            this.Name = "updatewardcontrol";
            this.Size = new System.Drawing.Size(694, 742);
            this.Load += new System.EventHandler(this.updatewardcontrol_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label22;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withavailablebeds;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofventilatorsperward;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofnurses;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofdoctors;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withstaffcount;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofbeds;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withwaddress;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withwname;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withwid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updateavailablebeds;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofventilatorsperward;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofnurses;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatestaffcount;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofdoctors;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofbeds;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatewardaddress;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatewardname;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatewardid;
        private Bunifu.Framework.UI.BunifuThinButton2 updatebutton;
        private System.Windows.Forms.Label label25;
    }
}
