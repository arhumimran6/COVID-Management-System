﻿namespace WindowsFormsApp3
{
    partial class admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.admindisplaybutton = new System.Windows.Forms.Button();
            this.adminsearchbutton = new System.Windows.Forms.Button();
            this.adminupdatebutton = new System.Windows.Forms.Button();
            this.admininsertbutton = new System.Windows.Forms.Button();
            this.sidepanel = new System.Windows.Forms.Panel();
            this.adminhomebutton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.home1 = new WindowsFormsApp3.home();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.admindisplaybutton);
            this.panel1.Controls.Add(this.adminsearchbutton);
            this.panel1.Controls.Add(this.adminupdatebutton);
            this.panel1.Controls.Add(this.admininsertbutton);
            this.panel1.Controls.Add(this.sidepanel);
            this.panel1.Controls.Add(this.adminhomebutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 559);
            this.panel1.TabIndex = 0;
            // 
            // admindisplaybutton
            // 
            this.admindisplaybutton.FlatAppearance.BorderSize = 0;
            this.admindisplaybutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.admindisplaybutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admindisplaybutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.admindisplaybutton.Location = new System.Drawing.Point(15, 372);
            this.admindisplaybutton.Name = "admindisplaybutton";
            this.admindisplaybutton.Size = new System.Drawing.Size(203, 55);
            this.admindisplaybutton.TabIndex = 5;
            this.admindisplaybutton.Text = "   Display";
            this.admindisplaybutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.admindisplaybutton.UseVisualStyleBackColor = true;
            this.admindisplaybutton.Click += new System.EventHandler(this.admindisplaybutton_Click);
            // 
            // adminsearchbutton
            // 
            this.adminsearchbutton.FlatAppearance.BorderSize = 0;
            this.adminsearchbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.adminsearchbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminsearchbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.adminsearchbutton.Location = new System.Drawing.Point(15, 311);
            this.adminsearchbutton.Name = "adminsearchbutton";
            this.adminsearchbutton.Size = new System.Drawing.Size(203, 55);
            this.adminsearchbutton.TabIndex = 4;
            this.adminsearchbutton.Text = "   Search";
            this.adminsearchbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.adminsearchbutton.UseVisualStyleBackColor = true;
            this.adminsearchbutton.Click += new System.EventHandler(this.admindeletebutton_Click);
            // 
            // adminupdatebutton
            // 
            this.adminupdatebutton.FlatAppearance.BorderSize = 0;
            this.adminupdatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.adminupdatebutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminupdatebutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.adminupdatebutton.Location = new System.Drawing.Point(15, 250);
            this.adminupdatebutton.Name = "adminupdatebutton";
            this.adminupdatebutton.Size = new System.Drawing.Size(200, 55);
            this.adminupdatebutton.TabIndex = 3;
            this.adminupdatebutton.Text = "   Update";
            this.adminupdatebutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.adminupdatebutton.UseVisualStyleBackColor = true;
            this.adminupdatebutton.Click += new System.EventHandler(this.adminupdatebutton_Click);
            // 
            // admininsertbutton
            // 
            this.admininsertbutton.FlatAppearance.BorderSize = 0;
            this.admininsertbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.admininsertbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admininsertbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.admininsertbutton.Location = new System.Drawing.Point(15, 189);
            this.admininsertbutton.Name = "admininsertbutton";
            this.admininsertbutton.Size = new System.Drawing.Size(200, 55);
            this.admininsertbutton.TabIndex = 2;
            this.admininsertbutton.Text = "   Insert";
            this.admininsertbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.admininsertbutton.UseVisualStyleBackColor = true;
            this.admininsertbutton.Click += new System.EventHandler(this.admininsertbutton_Click);
            // 
            // sidepanel
            // 
            this.sidepanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sidepanel.Location = new System.Drawing.Point(0, 128);
            this.sidepanel.Name = "sidepanel";
            this.sidepanel.Size = new System.Drawing.Size(11, 55);
            this.sidepanel.TabIndex = 1;
            // 
            // adminhomebutton
            // 
            this.adminhomebutton.FlatAppearance.BorderSize = 0;
            this.adminhomebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.adminhomebutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminhomebutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.adminhomebutton.Location = new System.Drawing.Point(12, 128);
            this.adminhomebutton.Name = "adminhomebutton";
            this.adminhomebutton.Size = new System.Drawing.Size(206, 55);
            this.adminhomebutton.TabIndex = 1;
            this.adminhomebutton.Text = "   Home";
            this.adminhomebutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.adminhomebutton.UseVisualStyleBackColor = true;
            this.adminhomebutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.home1);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(531, 559);
            this.panel2.TabIndex = 1;
            // 
            // home1
            // 
            this.home1.BackColor = System.Drawing.Color.White;
            this.home1.Location = new System.Drawing.Point(6, 57);
            this.home1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.home1.Name = "home1";
            this.home1.Size = new System.Drawing.Size(521, 430);
            this.home1.TabIndex = 4;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(330, 490);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(189, 20);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Go Back to Login screen";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(83, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome Back Admin";
            // 
            // admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 559);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button admindisplaybutton;
        private System.Windows.Forms.Button adminsearchbutton;
        private System.Windows.Forms.Button adminupdatebutton;
        private System.Windows.Forms.Button admininsertbutton;
        private System.Windows.Forms.Panel sidepanel;
        private System.Windows.Forms.Button adminhomebutton;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private home home1;
    }
}