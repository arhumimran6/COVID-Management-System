﻿namespace WindowsFormsApp3
{
    partial class displaycontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.displayhospitalbutton = new System.Windows.Forms.LinkLabel();
            this.displaypatientbutton = new System.Windows.Forms.LinkLabel();
            this.displaywardbutton = new System.Windows.Forms.LinkLabel();
            
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(340, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Which Table you want to display ?";
            // 
            // displayhospitalbutton
            // 
            this.displayhospitalbutton.AutoSize = true;
            this.displayhospitalbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayhospitalbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displayhospitalbutton.Location = new System.Drawing.Point(4, 179);
            this.displayhospitalbutton.Name = "displayhospitalbutton";
            this.displayhospitalbutton.Size = new System.Drawing.Size(110, 20);
            this.displayhospitalbutton.TabIndex = 12;
            this.displayhospitalbutton.TabStop = true;
            this.displayhospitalbutton.Text = "Hospital Table";
            this.displayhospitalbutton.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.displayhospitalbutton_LinkClicked);
            // 
            // displaypatientbutton
            // 
            this.displaypatientbutton.AutoSize = true;
            this.displaypatientbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displaypatientbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaypatientbutton.Location = new System.Drawing.Point(4, 125);
            this.displaypatientbutton.Name = "displaypatientbutton";
            this.displaypatientbutton.Size = new System.Drawing.Size(102, 20);
            this.displaypatientbutton.TabIndex = 11;
            this.displaypatientbutton.TabStop = true;
            this.displaypatientbutton.Text = "Patient Table";
            // 
            // displaywardbutton
            // 
            this.displaywardbutton.AutoSize = true;
            this.displaywardbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displaywardbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.displaywardbutton.Location = new System.Drawing.Point(4, 69);
            this.displaywardbutton.Name = "displaywardbutton";
            this.displaywardbutton.Size = new System.Drawing.Size(90, 20);
            this.displaywardbutton.TabIndex = 10;
            this.displaywardbutton.TabStop = true;
            this.displaywardbutton.Text = "Ward Table";
            this.displaywardbutton.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.displaywardbutton_LinkClicked);
            // 
            // hospitalcontrol1
            // 
            
            // 
            // wardcontrol1
            // 
            
            // 
            // patientcontrol1
            // 
            
            // 
            // displaycontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.displayhospitalbutton);
            this.Controls.Add(this.displaypatientbutton);
            this.Controls.Add(this.displaywardbutton);
            this.Controls.Add(this.label1);
            this.Name = "displaycontrol";
            this.Size = new System.Drawing.Size(507, 321);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel displayhospitalbutton;
        private System.Windows.Forms.LinkLabel displaypatientbutton;
        private System.Windows.Forms.LinkLabel displaywardbutton;
        
    }
}
