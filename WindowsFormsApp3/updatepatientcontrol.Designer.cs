﻿namespace WindowsFormsApp3
{
    partial class updatepatientcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(updatepatientcontrol));
            this.updatepatientbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label14 = new System.Windows.Forms.Label();
            this.updatePcontact = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.updatePaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.updatePname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.updatePid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.updatePhistdesc = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.updatePgender = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.updatePage = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.updatePbedno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.updatecoronastatus = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.updatePcond = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.updatePemail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label22 = new System.Windows.Forms.Label();
            this.withPage = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withPbedno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.withPcond = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label17 = new System.Windows.Forms.Label();
            this.withPcoronastatus = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withPemail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.withPaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withPcontact = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.withPname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withPid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.withPhistdesc = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withPgender = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label23 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // updatepatientbutton
            // 
            this.updatepatientbutton.ActiveBorderThickness = 1;
            this.updatepatientbutton.ActiveCornerRadius = 20;
            this.updatepatientbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.BackColor = System.Drawing.Color.White;
            this.updatepatientbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updatepatientbutton.BackgroundImage")));
            this.updatepatientbutton.ButtonText = "update";
            this.updatepatientbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatepatientbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatepatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleBorderThickness = 1;
            this.updatepatientbutton.IdleCornerRadius = 20;
            this.updatepatientbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleForecolor = System.Drawing.Color.White;
            this.updatepatientbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.Location = new System.Drawing.Point(569, 633);
            this.updatepatientbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updatepatientbutton.Name = "updatepatientbutton";
            this.updatepatientbutton.Size = new System.Drawing.Size(93, 51);
            this.updatepatientbutton.TabIndex = 93;
            this.updatepatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updatepatientbutton.Click += new System.EventHandler(this.updatepatientbutton_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(3, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 16);
            this.label14.TabIndex = 86;
            this.label14.Text = "Patient Contact :";
            // 
            // updatePcontact
            // 
            this.updatePcontact.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePcontact.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePcontact.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePcontact.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePcontact.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePcontact.ForeColor = System.Drawing.Color.DimGray;
            this.updatePcontact.HintForeColor = System.Drawing.Color.Empty;
            this.updatePcontact.HintText = "";
            this.updatePcontact.isPassword = false;
            this.updatePcontact.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcontact.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcontact.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcontact.LineThickness = 3;
            this.updatePcontact.Location = new System.Drawing.Point(116, 139);
            this.updatePcontact.Margin = new System.Windows.Forms.Padding(5);
            this.updatePcontact.MaxLength = 32767;
            this.updatePcontact.Name = "updatePcontact";
            this.updatePcontact.Size = new System.Drawing.Size(204, 38);
            this.updatePcontact.TabIndex = 85;
            this.updatePcontact.Text = "Varchar";
            this.updatePcontact.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(3, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 16);
            this.label4.TabIndex = 84;
            this.label4.Text = "Patient Address :";
            // 
            // updatePaddress
            // 
            this.updatePaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePaddress.ForeColor = System.Drawing.Color.DimGray;
            this.updatePaddress.HintForeColor = System.Drawing.Color.Empty;
            this.updatePaddress.HintText = "";
            this.updatePaddress.isPassword = false;
            this.updatePaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePaddress.LineThickness = 3;
            this.updatePaddress.Location = new System.Drawing.Point(116, 183);
            this.updatePaddress.Margin = new System.Windows.Forms.Padding(5);
            this.updatePaddress.MaxLength = 32767;
            this.updatePaddress.Name = "updatePaddress";
            this.updatePaddress.Size = new System.Drawing.Size(204, 38);
            this.updatePaddress.TabIndex = 83;
            this.updatePaddress.Text = "Varchar";
            this.updatePaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(3, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 82;
            this.label3.Text = "Patient Name :";
            // 
            // updatePname
            // 
            this.updatePname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePname.ForeColor = System.Drawing.Color.DimGray;
            this.updatePname.HintForeColor = System.Drawing.Color.Empty;
            this.updatePname.HintText = "";
            this.updatePname.isPassword = false;
            this.updatePname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePname.LineThickness = 3;
            this.updatePname.Location = new System.Drawing.Point(116, 89);
            this.updatePname.Margin = new System.Windows.Forms.Padding(5);
            this.updatePname.MaxLength = 32767;
            this.updatePname.Name = "updatePname";
            this.updatePname.Size = new System.Drawing.Size(204, 38);
            this.updatePname.TabIndex = 81;
            this.updatePname.Text = "Varchar";
            this.updatePname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.TabIndex = 80;
            this.label2.Text = "Patient ID :";
            // 
            // updatePid
            // 
            this.updatePid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePid.ForeColor = System.Drawing.Color.DimGray;
            this.updatePid.HintForeColor = System.Drawing.Color.Empty;
            this.updatePid.HintText = "";
            this.updatePid.isPassword = false;
            this.updatePid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePid.LineThickness = 3;
            this.updatePid.Location = new System.Drawing.Point(116, 37);
            this.updatePid.Margin = new System.Windows.Forms.Padding(5);
            this.updatePid.MaxLength = 32767;
            this.updatePid.Name = "updatePid";
            this.updatePid.Size = new System.Drawing.Size(204, 38);
            this.updatePid.TabIndex = 79;
            this.updatePid.Text = "Varchar";
            this.updatePid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(306, 32);
            this.label1.TabIndex = 78;
            this.label1.Text = "Patient Updation Page";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(5, 549);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 16);
            this.label11.TabIndex = 77;
            this.label11.Text = "History Description :";
            // 
            // updatePhistdesc
            // 
            this.updatePhistdesc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePhistdesc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePhistdesc.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePhistdesc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePhistdesc.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePhistdesc.ForeColor = System.Drawing.Color.DimGray;
            this.updatePhistdesc.HintForeColor = System.Drawing.Color.Empty;
            this.updatePhistdesc.HintText = "";
            this.updatePhistdesc.isPassword = false;
            this.updatePhistdesc.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePhistdesc.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePhistdesc.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePhistdesc.LineThickness = 3;
            this.updatePhistdesc.Location = new System.Drawing.Point(145, 530);
            this.updatePhistdesc.Margin = new System.Windows.Forms.Padding(5);
            this.updatePhistdesc.MaxLength = 32767;
            this.updatePhistdesc.Name = "updatePhistdesc";
            this.updatePhistdesc.Size = new System.Drawing.Size(231, 38);
            this.updatePhistdesc.TabIndex = 76;
            this.updatePhistdesc.Text = "Varchar e.g(any disease before)";
            this.updatePhistdesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(5, 497);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 16);
            this.label10.TabIndex = 75;
            this.label10.Text = "Patient Gender :";
            // 
            // updatePgender
            // 
            this.updatePgender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePgender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePgender.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePgender.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePgender.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePgender.ForeColor = System.Drawing.Color.DimGray;
            this.updatePgender.HintForeColor = System.Drawing.Color.Empty;
            this.updatePgender.HintText = "";
            this.updatePgender.isPassword = false;
            this.updatePgender.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePgender.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePgender.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePgender.LineThickness = 3;
            this.updatePgender.Location = new System.Drawing.Point(145, 477);
            this.updatePgender.Margin = new System.Windows.Forms.Padding(5);
            this.updatePgender.MaxLength = 32767;
            this.updatePgender.Name = "updatePgender";
            this.updatePgender.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.updatePgender.Size = new System.Drawing.Size(178, 38);
            this.updatePgender.TabIndex = 74;
            this.updatePgender.Text = "Varchar";
            this.updatePgender.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(6, 448);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 16);
            this.label9.TabIndex = 73;
            this.label9.Text = "Patient Age :";
            // 
            // updatePage
            // 
            this.updatePage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePage.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePage.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePage.ForeColor = System.Drawing.Color.DimGray;
            this.updatePage.HintForeColor = System.Drawing.Color.Empty;
            this.updatePage.HintText = "";
            this.updatePage.isPassword = false;
            this.updatePage.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePage.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePage.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePage.LineThickness = 3;
            this.updatePage.Location = new System.Drawing.Point(146, 428);
            this.updatePage.Margin = new System.Windows.Forms.Padding(5);
            this.updatePage.MaxLength = 32767;
            this.updatePage.Name = "updatePage";
            this.updatePage.Size = new System.Drawing.Size(177, 38);
            this.updatePage.TabIndex = 72;
            this.updatePage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(6, 399);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 16);
            this.label8.TabIndex = 71;
            this.label8.Text = "Patient Bed No. :";
            // 
            // updatePbedno
            // 
            this.updatePbedno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePbedno.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePbedno.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePbedno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePbedno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePbedno.ForeColor = System.Drawing.Color.DimGray;
            this.updatePbedno.HintForeColor = System.Drawing.Color.Empty;
            this.updatePbedno.HintText = "";
            this.updatePbedno.isPassword = false;
            this.updatePbedno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePbedno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePbedno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePbedno.LineThickness = 3;
            this.updatePbedno.Location = new System.Drawing.Point(146, 379);
            this.updatePbedno.Margin = new System.Windows.Forms.Padding(5);
            this.updatePbedno.MaxLength = 32767;
            this.updatePbedno.Name = "updatePbedno";
            this.updatePbedno.Size = new System.Drawing.Size(177, 38);
            this.updatePbedno.TabIndex = 70;
            this.updatePbedno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(6, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 16);
            this.label7.TabIndex = 69;
            this.label7.Text = "Corona Status :";
            // 
            // updatecoronastatus
            // 
            this.updatecoronastatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatecoronastatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatecoronastatus.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatecoronastatus.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatecoronastatus.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatecoronastatus.ForeColor = System.Drawing.Color.DimGray;
            this.updatecoronastatus.HintForeColor = System.Drawing.Color.Empty;
            this.updatecoronastatus.HintText = "";
            this.updatecoronastatus.isPassword = false;
            this.updatecoronastatus.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatecoronastatus.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatecoronastatus.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatecoronastatus.LineThickness = 3;
            this.updatecoronastatus.Location = new System.Drawing.Point(119, 281);
            this.updatecoronastatus.Margin = new System.Windows.Forms.Padding(5);
            this.updatecoronastatus.MaxLength = 32767;
            this.updatecoronastatus.Name = "updatecoronastatus";
            this.updatecoronastatus.Size = new System.Drawing.Size(233, 38);
            this.updatecoronastatus.TabIndex = 68;
            this.updatecoronastatus.Text = "Varchar e.g (positive or negative)";
            this.updatecoronastatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(6, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 16);
            this.label6.TabIndex = 67;
            this.label6.Text = "Condition  :";
            // 
            // updatePcond
            // 
            this.updatePcond.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePcond.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePcond.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePcond.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePcond.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePcond.ForeColor = System.Drawing.Color.DimGray;
            this.updatePcond.HintForeColor = System.Drawing.Color.Empty;
            this.updatePcond.HintText = "";
            this.updatePcond.isPassword = false;
            this.updatePcond.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcond.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcond.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePcond.LineThickness = 3;
            this.updatePcond.Location = new System.Drawing.Point(119, 331);
            this.updatePcond.Margin = new System.Windows.Forms.Padding(5);
            this.updatePcond.MaxLength = 32767;
            this.updatePcond.Name = "updatePcond";
            this.updatePcond.Size = new System.Drawing.Size(233, 38);
            this.updatePcond.TabIndex = 66;
            this.updatePcond.Text = "Varchar e.g (Fair, critical, Normal)";
            this.updatePcond.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(6, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 65;
            this.label5.Text = "Patient Email :";
            // 
            // updatePemail
            // 
            this.updatePemail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatePemail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatePemail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatePemail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatePemail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatePemail.ForeColor = System.Drawing.Color.DimGray;
            this.updatePemail.HintForeColor = System.Drawing.Color.Empty;
            this.updatePemail.HintText = "";
            this.updatePemail.isPassword = false;
            this.updatePemail.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePemail.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePemail.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatePemail.LineThickness = 3;
            this.updatePemail.Location = new System.Drawing.Point(119, 230);
            this.updatePemail.Margin = new System.Windows.Forms.Padding(5);
            this.updatePemail.MaxLength = 32767;
            this.updatePemail.Name = "updatePemail";
            this.updatePemail.Size = new System.Drawing.Size(204, 38);
            this.updatePemail.TabIndex = 64;
            this.updatePemail.Text = "Varchar";
            this.updatePemail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(364, 450);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(102, 16);
            this.label22.TabIndex = 129;
            this.label22.Text = "Updated with :";
            // 
            // withPage
            // 
            this.withPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPage.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPage.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPage.ForeColor = System.Drawing.Color.DimGray;
            this.withPage.HintForeColor = System.Drawing.Color.Empty;
            this.withPage.HintText = "";
            this.withPage.isPassword = false;
            this.withPage.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPage.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPage.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPage.LineThickness = 3;
            this.withPage.Location = new System.Drawing.Point(475, 438);
            this.withPage.Margin = new System.Windows.Forms.Padding(5);
            this.withPage.MaxLength = 32767;
            this.withPage.Name = "withPage";
            this.withPage.Size = new System.Drawing.Size(170, 38);
            this.withPage.TabIndex = 128;
            this.withPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // withPbedno
            // 
            this.withPbedno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPbedno.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPbedno.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPbedno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPbedno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPbedno.ForeColor = System.Drawing.Color.DimGray;
            this.withPbedno.HintForeColor = System.Drawing.Color.Empty;
            this.withPbedno.HintText = "";
            this.withPbedno.isPassword = false;
            this.withPbedno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPbedno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPbedno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPbedno.LineThickness = 3;
            this.withPbedno.Location = new System.Drawing.Point(474, 389);
            this.withPbedno.Margin = new System.Windows.Forms.Padding(5);
            this.withPbedno.MaxLength = 32767;
            this.withPbedno.Name = "withPbedno";
            this.withPbedno.Size = new System.Drawing.Size(168, 38);
            this.withPbedno.TabIndex = 127;
            this.withPbedno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label19.Location = new System.Drawing.Point(364, 401);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 16);
            this.label19.TabIndex = 126;
            this.label19.Text = "Updated with :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(364, 356);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 16);
            this.label20.TabIndex = 125;
            this.label20.Text = "Updated with :";
            // 
            // withPcond
            // 
            this.withPcond.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPcond.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPcond.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPcond.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPcond.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPcond.ForeColor = System.Drawing.Color.DimGray;
            this.withPcond.HintForeColor = System.Drawing.Color.Empty;
            this.withPcond.HintText = "";
            this.withPcond.isPassword = false;
            this.withPcond.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcond.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcond.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcond.LineThickness = 3;
            this.withPcond.Location = new System.Drawing.Point(475, 344);
            this.withPcond.Margin = new System.Windows.Forms.Padding(5);
            this.withPcond.MaxLength = 32767;
            this.withPcond.Name = "withPcond";
            this.withPcond.Size = new System.Drawing.Size(170, 38);
            this.withPcond.TabIndex = 124;
            this.withPcond.Text = "Varchar";
            this.withPcond.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(364, 305);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 16);
            this.label17.TabIndex = 123;
            this.label17.Text = "Updated with :";
            // 
            // withPcoronastatus
            // 
            this.withPcoronastatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPcoronastatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPcoronastatus.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPcoronastatus.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPcoronastatus.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPcoronastatus.ForeColor = System.Drawing.Color.DimGray;
            this.withPcoronastatus.HintForeColor = System.Drawing.Color.Empty;
            this.withPcoronastatus.HintText = "";
            this.withPcoronastatus.isPassword = false;
            this.withPcoronastatus.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcoronastatus.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcoronastatus.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcoronastatus.LineThickness = 3;
            this.withPcoronastatus.Location = new System.Drawing.Point(475, 293);
            this.withPcoronastatus.Margin = new System.Windows.Forms.Padding(5);
            this.withPcoronastatus.MaxLength = 32767;
            this.withPcoronastatus.Name = "withPcoronastatus";
            this.withPcoronastatus.Size = new System.Drawing.Size(170, 38);
            this.withPcoronastatus.TabIndex = 122;
            this.withPcoronastatus.Text = "Varchar ";
            this.withPcoronastatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // withPemail
            // 
            this.withPemail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPemail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPemail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPemail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPemail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPemail.ForeColor = System.Drawing.Color.DimGray;
            this.withPemail.HintForeColor = System.Drawing.Color.Empty;
            this.withPemail.HintText = "";
            this.withPemail.isPassword = false;
            this.withPemail.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPemail.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPemail.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPemail.LineThickness = 3;
            this.withPemail.Location = new System.Drawing.Point(474, 243);
            this.withPemail.Margin = new System.Windows.Forms.Padding(5);
            this.withPemail.MaxLength = 32767;
            this.withPemail.Name = "withPemail";
            this.withPemail.Size = new System.Drawing.Size(168, 38);
            this.withPemail.TabIndex = 121;
            this.withPemail.Text = "Varchar";
            this.withPemail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(364, 255);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 16);
            this.label18.TabIndex = 120;
            this.label18.Text = "Updated with :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(364, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 16);
            this.label15.TabIndex = 119;
            this.label15.Text = "Updated with :";
            // 
            // withPaddress
            // 
            this.withPaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPaddress.ForeColor = System.Drawing.Color.DimGray;
            this.withPaddress.HintForeColor = System.Drawing.Color.Empty;
            this.withPaddress.HintText = "";
            this.withPaddress.isPassword = false;
            this.withPaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPaddress.LineThickness = 3;
            this.withPaddress.Location = new System.Drawing.Point(475, 192);
            this.withPaddress.Margin = new System.Windows.Forms.Padding(5);
            this.withPaddress.MaxLength = 32767;
            this.withPaddress.Name = "withPaddress";
            this.withPaddress.Size = new System.Drawing.Size(170, 38);
            this.withPaddress.TabIndex = 118;
            this.withPaddress.Text = "Varchar";
            this.withPaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // withPcontact
            // 
            this.withPcontact.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPcontact.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPcontact.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPcontact.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPcontact.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPcontact.ForeColor = System.Drawing.Color.DimGray;
            this.withPcontact.HintForeColor = System.Drawing.Color.Empty;
            this.withPcontact.HintText = "";
            this.withPcontact.isPassword = false;
            this.withPcontact.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcontact.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcontact.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPcontact.LineThickness = 3;
            this.withPcontact.Location = new System.Drawing.Point(474, 137);
            this.withPcontact.Margin = new System.Windows.Forms.Padding(5);
            this.withPcontact.MaxLength = 32767;
            this.withPcontact.Name = "withPcontact";
            this.withPcontact.Size = new System.Drawing.Size(168, 38);
            this.withPcontact.TabIndex = 117;
            this.withPcontact.Text = "Varchar";
            this.withPcontact.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(364, 149);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 16);
            this.label16.TabIndex = 116;
            this.label16.Text = "Updated with :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(364, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 16);
            this.label12.TabIndex = 115;
            this.label12.Text = "Updated with :";
            // 
            // withPname
            // 
            this.withPname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPname.ForeColor = System.Drawing.Color.DimGray;
            this.withPname.HintForeColor = System.Drawing.Color.Empty;
            this.withPname.HintText = "";
            this.withPname.isPassword = false;
            this.withPname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPname.LineThickness = 3;
            this.withPname.Location = new System.Drawing.Point(475, 87);
            this.withPname.Margin = new System.Windows.Forms.Padding(5);
            this.withPname.MaxLength = 32767;
            this.withPname.Name = "withPname";
            this.withPname.Size = new System.Drawing.Size(170, 38);
            this.withPname.TabIndex = 114;
            this.withPname.Text = "Varchar";
            this.withPname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // withPid
            // 
            this.withPid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPid.ForeColor = System.Drawing.Color.DimGray;
            this.withPid.HintForeColor = System.Drawing.Color.Empty;
            this.withPid.HintText = "";
            this.withPid.isPassword = false;
            this.withPid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPid.LineThickness = 3;
            this.withPid.Location = new System.Drawing.Point(474, 42);
            this.withPid.Margin = new System.Windows.Forms.Padding(5);
            this.withPid.MaxLength = 32767;
            this.withPid.Name = "withPid";
            this.withPid.Size = new System.Drawing.Size(168, 38);
            this.withPid.TabIndex = 113;
            this.withPid.Text = "Varchar";
            this.withPid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(364, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 16);
            this.label13.TabIndex = 112;
            this.label13.Text = "Updated with :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label21.Location = new System.Drawing.Point(365, 552);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 16);
            this.label21.TabIndex = 133;
            this.label21.Text = "Updated with :";
            // 
            // withPhistdesc
            // 
            this.withPhistdesc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPhistdesc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPhistdesc.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPhistdesc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPhistdesc.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPhistdesc.ForeColor = System.Drawing.Color.DimGray;
            this.withPhistdesc.HintForeColor = System.Drawing.Color.Empty;
            this.withPhistdesc.HintText = "";
            this.withPhistdesc.isPassword = false;
            this.withPhistdesc.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPhistdesc.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPhistdesc.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPhistdesc.LineThickness = 3;
            this.withPhistdesc.Location = new System.Drawing.Point(476, 540);
            this.withPhistdesc.Margin = new System.Windows.Forms.Padding(5);
            this.withPhistdesc.MaxLength = 32767;
            this.withPhistdesc.Name = "withPhistdesc";
            this.withPhistdesc.Size = new System.Drawing.Size(170, 38);
            this.withPhistdesc.TabIndex = 132;
            this.withPhistdesc.Text = "Varchar";
            this.withPhistdesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // withPgender
            // 
            this.withPgender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withPgender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withPgender.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withPgender.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withPgender.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withPgender.ForeColor = System.Drawing.Color.DimGray;
            this.withPgender.HintForeColor = System.Drawing.Color.Empty;
            this.withPgender.HintText = "";
            this.withPgender.isPassword = false;
            this.withPgender.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPgender.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPgender.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withPgender.LineThickness = 3;
            this.withPgender.Location = new System.Drawing.Point(474, 485);
            this.withPgender.Margin = new System.Windows.Forms.Padding(5);
            this.withPgender.MaxLength = 32767;
            this.withPgender.Name = "withPgender";
            this.withPgender.Size = new System.Drawing.Size(168, 38);
            this.withPgender.TabIndex = 131;
            this.withPgender.Text = "Varchar";
            this.withPgender.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label23.Location = new System.Drawing.Point(364, 497);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 16);
            this.label23.TabIndex = 130;
            this.label23.Text = "Updated with :";
            // 
            // updatepatientcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label21);
            this.Controls.Add(this.withPhistdesc);
            this.Controls.Add(this.withPgender);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.withPage);
            this.Controls.Add(this.withPbedno);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.withPcond);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.withPcoronastatus);
            this.Controls.Add(this.withPemail);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.withPaddress);
            this.Controls.Add(this.withPcontact);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.withPname);
            this.Controls.Add(this.withPid);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.updatepatientbutton);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.updatePcontact);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.updatePaddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.updatePname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.updatePid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.updatePhistdesc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.updatePgender);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updatePage);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.updatePbedno);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.updatecoronastatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.updatePcond);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.updatePemail);
            this.Name = "updatepatientcontrol";
            this.Size = new System.Drawing.Size(694, 742);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 updatepatientbutton;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePcontact;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePaddress;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePname;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePhistdesc;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePgender;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePage;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePbedno;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatecoronastatus;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePcond;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatePemail;
        private System.Windows.Forms.Label label22;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPage;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPbedno;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPcond;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPcoronastatus;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPemail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPaddress;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPcontact;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPname;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPid;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPhistdesc;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withPgender;
        private System.Windows.Forms.Label label23;
    }
}
