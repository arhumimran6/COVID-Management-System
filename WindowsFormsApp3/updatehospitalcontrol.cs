﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace WindowsFormsApp3
{
    public partial class updatehospitalcontrol : UserControl
    {
        public updatehospitalcontrol()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
        private void updatebutton_Click(object sender, EventArgs e)
        {
            sqlcon.Open();

            string q_update_hid = "update hospital set H_ID='" + withhid.Text + "' where H_ID='" + updatehospitalid.Text + "'";
            SqlCommand cmd_update_hid = new SqlCommand(q_update_hid, sqlcon);
            cmd_update_hid.ExecuteNonQuery();

            string q_update_Hname = "update hospital set H_NAME='" + withhname.Text + "'where H_NAME='" + updatehospitalname.Text + "'";
            SqlCommand cmd_update_Hname = new SqlCommand(q_update_Hname, sqlcon);
            cmd_update_Hname.ExecuteNonQuery();

            string q_update_haddress = "update hospital set H_ADDRESS='" + withhaddress.Text + "'where H_ADDRESS='" + updatehospitaladdress.Text + "'";
            SqlCommand cmd_update_haddress = new SqlCommand(q_update_haddress, sqlcon);
            cmd_update_haddress.ExecuteNonQuery();

            string q_update_numberofwards = "update hospital set NUMBER_OF_WARDS='" + withnoofwards.Text.ToString() + "'where NUMBER_OF_WARDS='" + updatenoofwards.Text.ToString() + "'";
            SqlCommand cmd_update_numberofwards = new SqlCommand(q_update_numberofwards, sqlcon);
            cmd_update_numberofwards.ExecuteNonQuery();

            string q_update_staffcount = "update hospital set STAFF_COUNT='" + withstaffcount.Text + "'where STAFF_COUNT='" + updatestaffcount.Text + "'";
            SqlCommand cmd_update_staffcount = new SqlCommand(q_update_staffcount, sqlcon);
            cmd_update_staffcount.ExecuteNonQuery();

            string q_update_numberofdoctors = "update hospital set NUMBER_OF_DOCTORS='" + withnoofdoctors.Text + "'where NUMBER_OF_DOCTORS='" + updatenoofdoctors.Text + "'";
            SqlCommand cmd_update_numberofdoctors = new SqlCommand(q_update_numberofdoctors, sqlcon);
            cmd_update_numberofdoctors.ExecuteNonQuery();

            string q_update_numberofnurses = "update hospital set NUMBER_OF_NURSES='" + withnoofnurses.Text + "'where NUMBER_OF_NURSES='" + updatenoofnurses.Text + "'";
            SqlCommand cmd_update_numberofnurses = new SqlCommand(q_update_numberofnurses, sqlcon);
            cmd_update_numberofnurses.ExecuteNonQuery();

            string q_update_numberofventilators = "update hospital set NUMBER_OF_VENTILATORS='" + withnoofventilators.Text + "'where NUMBER_OF_NURSES='" + updatenoofventilators.Text + "'";
            SqlCommand cmd_update_numberofventilators = new SqlCommand(q_update_numberofventilators, sqlcon);
            cmd_update_numberofventilators.ExecuteNonQuery();
            
            string q_update_patientcount = "update hospital set PATIENT_COUNT='" + withpatientcount.Text + "'where PATIENT_COUNT='" + updatepatientcount.Text + "'";
            SqlCommand cmd_update_patientcount = new SqlCommand(q_update_patientcount, sqlcon);
            cmd_update_patientcount.ExecuteNonQuery();
            
            string q_update_numberoflabs = "update hospital set NUMBER_OF_LABS='" + withnooflabs.Text.ToString() + "'where NUMBER_OF_LABS='" + updatenooflabs.Text.ToString() + "'";
            SqlCommand cmd_update_numberoflabs = new SqlCommand(q_update_numberoflabs, sqlcon);
            cmd_update_numberoflabs.ExecuteNonQuery();
            
            string q_update_numberoftestkits = "update hospital set NUMBER_OF_TESTKITS='" + withnooftestkits.Text + "'where NUMBER_OF_TESTKITS='" + updatenooftestkits.Text + "'";
            SqlCommand cmd_update_numberoftestkits = new SqlCommand(q_update_numberoftestkits, sqlcon);
            cmd_update_numberoftestkits.ExecuteNonQuery();
            
            string q_update_numberofvisitors = "update hospital set NUMBER_OF_VISITORS='" + withnoofvisitors.Text + "'where NUMBER_OF_VISITORS='" + updatenoofvisitors.Text + "'";
            SqlCommand cmd_update_numberofvisitors = new SqlCommand(q_update_numberofvisitors, sqlcon);
            cmd_update_numberofvisitors.ExecuteNonQuery();

            sqlcon.Close();
            MessageBox.Show("Updated Successfully");
        }

        private void withstaffcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void withnooftestkits_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withnooflabs_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void withpatientcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withnoofvisitors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void withnoofnurses_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void withnoofdoctors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void withnoofwards_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withhaddress_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofvisitors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void updatenooftestkits_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void updatenooflabs_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void updatepatientcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofventilators_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofnurses_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void updatestaffcount_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofdoctors_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void updatenoofwards_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void updatehospitaladdress_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void withnoofventilators_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void updatehospitalname_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void updatehospitalid_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void withhname_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void withhid_OnValueChanged(object sender, EventArgs e)
        {

        }
    }
}
