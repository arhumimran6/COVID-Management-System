﻿namespace WindowsFormsApp3
{
    partial class searchinhospital
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(searchinhospital));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.inserthospitalid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.hospitaldatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.displaygrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.insertwardbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.hospitaldatagrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(3, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(465, 38);
            this.label2.TabIndex = 22;
            this.label2.Text = "As H_ID(Hospital ID) is the primary key of table, So searching\r\n will be performe" +
    "d against it.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 19);
            this.label1.TabIndex = 24;
            this.label1.Text = "Hospital ID :";
            // 
            // inserthospitalid
            // 
            this.inserthospitalid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.inserthospitalid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.inserthospitalid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.inserthospitalid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.inserthospitalid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.inserthospitalid.ForeColor = System.Drawing.Color.DimGray;
            this.inserthospitalid.HintForeColor = System.Drawing.Color.Empty;
            this.inserthospitalid.HintText = "";
            this.inserthospitalid.isPassword = false;
            this.inserthospitalid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineThickness = 3;
            this.inserthospitalid.Location = new System.Drawing.Point(142, 326);
            this.inserthospitalid.Margin = new System.Windows.Forms.Padding(5);
            this.inserthospitalid.MaxLength = 32767;
            this.inserthospitalid.Name = "inserthospitalid";
            this.inserthospitalid.Size = new System.Drawing.Size(204, 38);
            this.inserthospitalid.TabIndex = 23;
            this.inserthospitalid.Text = "Varchar";
            this.inserthospitalid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // hospitaldatagrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.hospitaldatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.hospitaldatagrid.BackgroundColor = System.Drawing.Color.White;
            this.hospitaldatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.hospitaldatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.hospitaldatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.hospitaldatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hospitaldatagrid.DoubleBuffered = true;
            this.hospitaldatagrid.EnableHeadersVisualStyles = false;
            this.hospitaldatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.hospitaldatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.hospitaldatagrid.Location = new System.Drawing.Point(0, 382);
            this.hospitaldatagrid.Name = "hospitaldatagrid";
            this.hospitaldatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.hospitaldatagrid.Size = new System.Drawing.Size(563, 150);
            this.hospitaldatagrid.TabIndex = 25;
            // 
            // displaygrid
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.displaygrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.displaygrid.BackgroundColor = System.Drawing.Color.White;
            this.displaygrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.displaygrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.displaygrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.displaygrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displaygrid.DoubleBuffered = true;
            this.displaygrid.EnableHeadersVisualStyles = false;
            this.displaygrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.Location = new System.Drawing.Point(-1, 56);
            this.displaygrid.Name = "displaygrid";
            this.displaygrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.displaygrid.Size = new System.Drawing.Size(564, 262);
            this.displaygrid.TabIndex = 42;
            // 
            // insertwardbutton
            // 
            this.insertwardbutton.ActiveBorderThickness = 1;
            this.insertwardbutton.ActiveCornerRadius = 20;
            this.insertwardbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.BackColor = System.Drawing.Color.White;
            this.insertwardbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insertwardbutton.BackgroundImage")));
            this.insertwardbutton.ButtonText = "Search";
            this.insertwardbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertwardbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleBorderThickness = 1;
            this.insertwardbutton.IdleCornerRadius = 20;
            this.insertwardbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleForecolor = System.Drawing.Color.White;
            this.insertwardbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.Location = new System.Drawing.Point(416, 326);
            this.insertwardbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.insertwardbutton.Name = "insertwardbutton";
            this.insertwardbutton.Size = new System.Drawing.Size(93, 51);
            this.insertwardbutton.TabIndex = 41;
            this.insertwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertwardbutton.Click += new System.EventHandler(this.insertwardbutton_Click);
            // 
            // searchinhospital
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.displaygrid);
            this.Controls.Add(this.insertwardbutton);
            this.Controls.Add(this.hospitaldatagrid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inserthospitalid);
            this.Controls.Add(this.label2);
            this.Name = "searchinhospital";
            this.Size = new System.Drawing.Size(566, 586);
            ((System.ComponentModel.ISupportInitialize)(this.hospitaldatagrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox inserthospitalid;
        private Bunifu.Framework.UI.BunifuCustomDataGrid hospitaldatagrid;
        private Bunifu.Framework.UI.BunifuThinButton2 insertwardbutton;
        private Bunifu.Framework.UI.BunifuCustomDataGrid displaygrid;
    }
}
