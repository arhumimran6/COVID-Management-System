﻿namespace WindowsFormsApp3
{
    partial class admin_update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_update));
            this.panel1 = new System.Windows.Forms.Panel();
            this.sidepanel2 = new System.Windows.Forms.Panel();
            this.updateadminpatientbutton = new System.Windows.Forms.Button();
            this.updateadminwardbutton = new System.Windows.Forms.Button();
            this.updateadminhospitalbutton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.updatewardcontrol1 = new WindowsFormsApp3.updatewardcontrol();
            this.updatehospitalcontrol1 = new WindowsFormsApp3.updatehospitalcontrol();
            this.updatepatientcontrol1 = new WindowsFormsApp3.updatepatientcontrol();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.sidepanel2);
            this.panel1.Controls.Add(this.updateadminpatientbutton);
            this.panel1.Controls.Add(this.updateadminwardbutton);
            this.panel1.Controls.Add(this.updateadminhospitalbutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 738);
            this.panel1.TabIndex = 2;
            // 
            // sidepanel2
            // 
            this.sidepanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel2.Location = new System.Drawing.Point(0, 128);
            this.sidepanel2.Name = "sidepanel2";
            this.sidepanel2.Size = new System.Drawing.Size(11, 55);
            this.sidepanel2.TabIndex = 4;
            // 
            // updateadminpatientbutton
            // 
            this.updateadminpatientbutton.FlatAppearance.BorderSize = 0;
            this.updateadminpatientbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateadminpatientbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateadminpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.updateadminpatientbutton.Location = new System.Drawing.Point(15, 250);
            this.updateadminpatientbutton.Name = "updateadminpatientbutton";
            this.updateadminpatientbutton.Size = new System.Drawing.Size(200, 55);
            this.updateadminpatientbutton.TabIndex = 3;
            this.updateadminpatientbutton.Text = "   Patient";
            this.updateadminpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateadminpatientbutton.UseVisualStyleBackColor = true;
            this.updateadminpatientbutton.Click += new System.EventHandler(this.updateadminpatientbutton_Click);
            // 
            // updateadminwardbutton
            // 
            this.updateadminwardbutton.FlatAppearance.BorderSize = 0;
            this.updateadminwardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateadminwardbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateadminwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.updateadminwardbutton.Location = new System.Drawing.Point(15, 189);
            this.updateadminwardbutton.Name = "updateadminwardbutton";
            this.updateadminwardbutton.Size = new System.Drawing.Size(200, 55);
            this.updateadminwardbutton.TabIndex = 2;
            this.updateadminwardbutton.Text = "   Ward";
            this.updateadminwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateadminwardbutton.UseVisualStyleBackColor = true;
            this.updateadminwardbutton.Click += new System.EventHandler(this.updateadminwardbutton_Click);
            // 
            // updateadminhospitalbutton
            // 
            this.updateadminhospitalbutton.FlatAppearance.BorderSize = 0;
            this.updateadminhospitalbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateadminhospitalbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateadminhospitalbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.updateadminhospitalbutton.Location = new System.Drawing.Point(12, 128);
            this.updateadminhospitalbutton.Name = "updateadminhospitalbutton";
            this.updateadminhospitalbutton.Size = new System.Drawing.Size(206, 55);
            this.updateadminhospitalbutton.TabIndex = 1;
            this.updateadminhospitalbutton.Text = "   Hospital";
            this.updateadminhospitalbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.updateadminhospitalbutton.UseVisualStyleBackColor = true;
            this.updateadminhospitalbutton.Click += new System.EventHandler(this.insertadminhospitalbutton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.updatepatientcontrol1);
            this.panel2.Controls.Add(this.updatewardcontrol1);
            this.panel2.Controls.Add(this.updatehospitalcontrol1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(721, 738);
            this.panel2.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // updatewardcontrol1
            // 
            this.updatewardcontrol1.BackColor = System.Drawing.Color.White;
            this.updatewardcontrol1.Location = new System.Drawing.Point(0, 0);
            this.updatewardcontrol1.Name = "updatewardcontrol1";
            this.updatewardcontrol1.Size = new System.Drawing.Size(694, 742);
            this.updatewardcontrol1.TabIndex = 4;
            // 
            // updatehospitalcontrol1
            // 
            this.updatehospitalcontrol1.BackColor = System.Drawing.Color.White;
            this.updatehospitalcontrol1.Location = new System.Drawing.Point(0, -16);
            this.updatehospitalcontrol1.Name = "updatehospitalcontrol1";
            this.updatehospitalcontrol1.Size = new System.Drawing.Size(694, 742);
            this.updatehospitalcontrol1.TabIndex = 0;
            // 
            // updatepatientcontrol1
            // 
            this.updatepatientcontrol1.BackColor = System.Drawing.Color.White;
            this.updatepatientcontrol1.Location = new System.Drawing.Point(0, 0);
            this.updatepatientcontrol1.Name = "updatepatientcontrol1";
            this.updatepatientcontrol1.Size = new System.Drawing.Size(694, 742);
            this.updatepatientcontrol1.TabIndex = 4;
            // 
            // admin_update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(942, 738);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "admin_update";
            this.Text = "admin_update";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel sidepanel2;
        private System.Windows.Forms.Button updateadminpatientbutton;
        private System.Windows.Forms.Button updateadminwardbutton;
        private System.Windows.Forms.Button updateadminhospitalbutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private updatehospitalcontrol updatehospitalcontrol1;
        private updatewardcontrol updatewardcontrol1;
        private updatepatientcontrol updatepatientcontrol1;
    }
}