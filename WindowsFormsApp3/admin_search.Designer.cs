﻿namespace WindowsFormsApp3
{
    partial class admin_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_search));
            this.panel1 = new System.Windows.Forms.Panel();
            this.sidepanel1 = new System.Windows.Forms.Panel();
            this.searchadminpatientbutton = new System.Windows.Forms.Button();
            this.searchadminwardbutton = new System.Windows.Forms.Button();
            this.searchadminhospitalbutton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.searchinward1 = new WindowsFormsApp3.searchinward();
            this.searchinhospital1 = new WindowsFormsApp3.searchinhospital();
            this.searchinpatient1 = new WindowsFormsApp3.searchinpatient();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.sidepanel1);
            this.panel1.Controls.Add(this.searchadminpatientbutton);
            this.panel1.Controls.Add(this.searchadminwardbutton);
            this.panel1.Controls.Add(this.searchadminhospitalbutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 586);
            this.panel1.TabIndex = 2;
            // 
            // sidepanel1
            // 
            this.sidepanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel1.Location = new System.Drawing.Point(0, 128);
            this.sidepanel1.Name = "sidepanel1";
            this.sidepanel1.Size = new System.Drawing.Size(11, 55);
            this.sidepanel1.TabIndex = 4;
            // 
            // searchadminpatientbutton
            // 
            this.searchadminpatientbutton.FlatAppearance.BorderSize = 0;
            this.searchadminpatientbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchadminpatientbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchadminpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.searchadminpatientbutton.Location = new System.Drawing.Point(15, 250);
            this.searchadminpatientbutton.Name = "searchadminpatientbutton";
            this.searchadminpatientbutton.Size = new System.Drawing.Size(200, 55);
            this.searchadminpatientbutton.TabIndex = 3;
            this.searchadminpatientbutton.Text = "   Patient";
            this.searchadminpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.searchadminpatientbutton.UseVisualStyleBackColor = true;
            this.searchadminpatientbutton.Click += new System.EventHandler(this.searchadminpatientbutton_Click);
            // 
            // searchadminwardbutton
            // 
            this.searchadminwardbutton.FlatAppearance.BorderSize = 0;
            this.searchadminwardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchadminwardbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchadminwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.searchadminwardbutton.Location = new System.Drawing.Point(15, 189);
            this.searchadminwardbutton.Name = "searchadminwardbutton";
            this.searchadminwardbutton.Size = new System.Drawing.Size(200, 55);
            this.searchadminwardbutton.TabIndex = 2;
            this.searchadminwardbutton.Text = "   Ward";
            this.searchadminwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.searchadminwardbutton.UseVisualStyleBackColor = true;
            this.searchadminwardbutton.Click += new System.EventHandler(this.searchadminwardbutton_Click);
            // 
            // searchadminhospitalbutton
            // 
            this.searchadminhospitalbutton.FlatAppearance.BorderSize = 0;
            this.searchadminhospitalbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchadminhospitalbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchadminhospitalbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.searchadminhospitalbutton.Location = new System.Drawing.Point(12, 128);
            this.searchadminhospitalbutton.Name = "searchadminhospitalbutton";
            this.searchadminhospitalbutton.Size = new System.Drawing.Size(206, 55);
            this.searchadminhospitalbutton.TabIndex = 1;
            this.searchadminhospitalbutton.Text = "   Hospital";
            this.searchadminhospitalbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.searchadminhospitalbutton.UseVisualStyleBackColor = true;
            this.searchadminhospitalbutton.Click += new System.EventHandler(this.insertadminhospitalbutton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.searchinpatient1);
            this.panel2.Controls.Add(this.searchinward1);
            this.panel2.Controls.Add(this.searchinhospital1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(566, 586);
            this.panel2.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // searchinward1
            // 
            this.searchinward1.BackColor = System.Drawing.Color.White;
            this.searchinward1.Location = new System.Drawing.Point(0, 3);
            this.searchinward1.Name = "searchinward1";
            this.searchinward1.Size = new System.Drawing.Size(566, 586);
            this.searchinward1.TabIndex = 5;
            // 
            // searchinhospital1
            // 
            this.searchinhospital1.BackColor = System.Drawing.Color.White;
            this.searchinhospital1.Location = new System.Drawing.Point(0, 0);
            this.searchinhospital1.Name = "searchinhospital1";
            this.searchinhospital1.Size = new System.Drawing.Size(566, 586);
            this.searchinhospital1.TabIndex = 0;
            // 
            // searchinpatient1
            // 
            this.searchinpatient1.BackColor = System.Drawing.Color.White;
            this.searchinpatient1.Location = new System.Drawing.Point(0, 0);
            this.searchinpatient1.Name = "searchinpatient1";
            this.searchinpatient1.Size = new System.Drawing.Size(566, 586);
            this.searchinpatient1.TabIndex = 6;
            // 
            // admin_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(787, 586);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "admin_search";
            this.Text = "admin_search";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel sidepanel1;
        private System.Windows.Forms.Button searchadminpatientbutton;
        private System.Windows.Forms.Button searchadminwardbutton;
        private System.Windows.Forms.Button searchadminhospitalbutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private searchinhospital searchinhospital1;
        private searchinward searchinward1;
        private searchinpatient searchinpatient1;
    }
}