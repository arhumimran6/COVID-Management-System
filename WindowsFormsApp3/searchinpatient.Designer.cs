﻿namespace WindowsFormsApp3
{
    partial class searchinpatient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(searchinpatient));
            this.label2 = new System.Windows.Forms.Label();
            this.displaygrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.patientdatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.insertwardbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.insertpid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientdatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(3, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(454, 38);
            this.label2.TabIndex = 24;
            this.label2.Text = "As P_ID(Patient ID) is the primary key of table, So searching\r\n will be performed" +
    " against it.";
            // 
            // displaygrid
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.displaygrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.displaygrid.BackgroundColor = System.Drawing.Color.White;
            this.displaygrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.displaygrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.displaygrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.displaygrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displaygrid.DoubleBuffered = true;
            this.displaygrid.EnableHeadersVisualStyles = false;
            this.displaygrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.displaygrid.Location = new System.Drawing.Point(-1, 56);
            this.displaygrid.Name = "displaygrid";
            this.displaygrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.displaygrid.Size = new System.Drawing.Size(564, 262);
            this.displaygrid.TabIndex = 44;
            // 
            // patientdatagrid
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.patientdatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.patientdatagrid.BackgroundColor = System.Drawing.Color.White;
            this.patientdatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.patientdatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.patientdatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.patientdatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientdatagrid.DoubleBuffered = true;
            this.patientdatagrid.EnableHeadersVisualStyles = false;
            this.patientdatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.patientdatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.patientdatagrid.Location = new System.Drawing.Point(-1, 397);
            this.patientdatagrid.Name = "patientdatagrid";
            this.patientdatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.patientdatagrid.Size = new System.Drawing.Size(563, 150);
            this.patientdatagrid.TabIndex = 51;
            // 
            // insertwardbutton
            // 
            this.insertwardbutton.ActiveBorderThickness = 1;
            this.insertwardbutton.ActiveCornerRadius = 20;
            this.insertwardbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.BackColor = System.Drawing.Color.White;
            this.insertwardbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insertwardbutton.BackgroundImage")));
            this.insertwardbutton.ButtonText = "Search";
            this.insertwardbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertwardbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleBorderThickness = 1;
            this.insertwardbutton.IdleCornerRadius = 20;
            this.insertwardbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleForecolor = System.Drawing.Color.White;
            this.insertwardbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.Location = new System.Drawing.Point(410, 327);
            this.insertwardbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.insertwardbutton.Name = "insertwardbutton";
            this.insertwardbutton.Size = new System.Drawing.Size(93, 51);
            this.insertwardbutton.TabIndex = 50;
            this.insertwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertwardbutton.Click += new System.EventHandler(this.insertwardbutton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 350);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 19);
            this.label1.TabIndex = 49;
            this.label1.Text = "PATIENT ID :";
            // 
            // insertpid
            // 
            this.insertpid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertpid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertpid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertpid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertpid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertpid.ForeColor = System.Drawing.Color.DimGray;
            this.insertpid.HintForeColor = System.Drawing.Color.Empty;
            this.insertpid.HintText = "";
            this.insertpid.isPassword = false;
            this.insertpid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpid.LineThickness = 3;
            this.insertpid.Location = new System.Drawing.Point(141, 331);
            this.insertpid.Margin = new System.Windows.Forms.Padding(5);
            this.insertpid.MaxLength = 32767;
            this.insertpid.Name = "insertpid";
            this.insertpid.Size = new System.Drawing.Size(204, 38);
            this.insertpid.TabIndex = 48;
            this.insertpid.Text = "Varchar";
            this.insertpid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // searchinpatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.patientdatagrid);
            this.Controls.Add(this.insertwardbutton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.insertpid);
            this.Controls.Add(this.displaygrid);
            this.Controls.Add(this.label2);
            this.Name = "searchinpatient";
            this.Size = new System.Drawing.Size(566, 586);
            ((System.ComponentModel.ISupportInitialize)(this.displaygrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientdatagrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid displaygrid;
        private Bunifu.Framework.UI.BunifuCustomDataGrid patientdatagrid;
        private Bunifu.Framework.UI.BunifuThinButton2 insertwardbutton;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertpid;
    }
}
