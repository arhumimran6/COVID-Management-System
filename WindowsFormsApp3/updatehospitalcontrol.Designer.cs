﻿namespace WindowsFormsApp3
{
    partial class updatehospitalcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(updatehospitalcontrol));
            this.label11 = new System.Windows.Forms.Label();
            this.updatenooflabs = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.updatepatientcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.updatenoofventilators = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.updatenoofnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.updatestaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.updatenoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.updatenoofwards = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.updatehospitaladdress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.updatehospitalname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.updatehospitalid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label13 = new System.Windows.Forms.Label();
            this.updatenoofvisitors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.updatenooftestkits = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.withhid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label14 = new System.Windows.Forms.Label();
            this.withhname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label15 = new System.Windows.Forms.Label();
            this.withnoofwards = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withhaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.withnoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withstaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label18 = new System.Windows.Forms.Label();
            this.withnoofventilators = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.withnoofnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withnooflabs = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.withpatientcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.withnoofvisitors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.withnooftestkits = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.updatebutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label25 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(2, 556);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 45;
            this.label11.Text = "No. of Labs :";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // updatenooflabs
            // 
            this.updatenooflabs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenooflabs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenooflabs.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenooflabs.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenooflabs.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenooflabs.ForeColor = System.Drawing.Color.DimGray;
            this.updatenooflabs.HintForeColor = System.Drawing.Color.Empty;
            this.updatenooflabs.HintText = "";
            this.updatenooflabs.isPassword = false;
            this.updatenooflabs.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooflabs.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooflabs.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooflabs.LineThickness = 3;
            this.updatenooflabs.Location = new System.Drawing.Point(128, 544);
            this.updatenooflabs.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenooflabs.MaxLength = 32767;
            this.updatenooflabs.Name = "updatenooflabs";
            this.updatenooflabs.Size = new System.Drawing.Size(170, 38);
            this.updatenooflabs.TabIndex = 44;
            this.updatenooflabs.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenooflabs.OnValueChanged += new System.EventHandler(this.updatenooflabs_OnValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(2, 504);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 16);
            this.label10.TabIndex = 43;
            this.label10.Text = "Patient Count :";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // updatepatientcount
            // 
            this.updatepatientcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatepatientcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatepatientcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatepatientcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatepatientcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatepatientcount.ForeColor = System.Drawing.Color.DimGray;
            this.updatepatientcount.HintForeColor = System.Drawing.Color.Empty;
            this.updatepatientcount.HintText = "";
            this.updatepatientcount.isPassword = false;
            this.updatepatientcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientcount.LineThickness = 3;
            this.updatepatientcount.Location = new System.Drawing.Point(128, 492);
            this.updatepatientcount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatepatientcount.MaxLength = 32767;
            this.updatepatientcount.Name = "updatepatientcount";
            this.updatepatientcount.Size = new System.Drawing.Size(170, 38);
            this.updatepatientcount.TabIndex = 42;
            this.updatepatientcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatepatientcount.OnValueChanged += new System.EventHandler(this.updatepatientcount_OnValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(3, 455);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 16);
            this.label9.TabIndex = 41;
            this.label9.Text = "No. of ventilators :";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // updatenoofventilators
            // 
            this.updatenoofventilators.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofventilators.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofventilators.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofventilators.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofventilators.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofventilators.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofventilators.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofventilators.HintText = "";
            this.updatenoofventilators.isPassword = false;
            this.updatenoofventilators.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilators.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilators.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofventilators.LineThickness = 3;
            this.updatenoofventilators.Location = new System.Drawing.Point(129, 443);
            this.updatenoofventilators.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenoofventilators.MaxLength = 32767;
            this.updatenoofventilators.Name = "updatenoofventilators";
            this.updatenoofventilators.Size = new System.Drawing.Size(170, 38);
            this.updatenoofventilators.TabIndex = 40;
            this.updatenoofventilators.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofventilators.OnValueChanged += new System.EventHandler(this.updatenoofventilators_OnValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(3, 406);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 16);
            this.label8.TabIndex = 39;
            this.label8.Text = "No. of Nurses :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // updatenoofnurses
            // 
            this.updatenoofnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofnurses.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofnurses.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofnurses.HintText = "";
            this.updatenoofnurses.isPassword = false;
            this.updatenoofnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofnurses.LineThickness = 3;
            this.updatenoofnurses.Location = new System.Drawing.Point(129, 394);
            this.updatenoofnurses.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenoofnurses.MaxLength = 32767;
            this.updatenoofnurses.Name = "updatenoofnurses";
            this.updatenoofnurses.Size = new System.Drawing.Size(170, 38);
            this.updatenoofnurses.TabIndex = 38;
            this.updatenoofnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofnurses.OnValueChanged += new System.EventHandler(this.updatenoofnurses_OnValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(3, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 16);
            this.label7.TabIndex = 37;
            this.label7.Text = "Staff Count :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // updatestaffcount
            // 
            this.updatestaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatestaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatestaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatestaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatestaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatestaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.updatestaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.updatestaffcount.HintText = "";
            this.updatestaffcount.isPassword = false;
            this.updatestaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatestaffcount.LineThickness = 3;
            this.updatestaffcount.Location = new System.Drawing.Point(129, 293);
            this.updatestaffcount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatestaffcount.MaxLength = 32767;
            this.updatestaffcount.Name = "updatestaffcount";
            this.updatestaffcount.Size = new System.Drawing.Size(170, 38);
            this.updatestaffcount.TabIndex = 36;
            this.updatestaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatestaffcount.OnValueChanged += new System.EventHandler(this.updatestaffcount_OnValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(3, 355);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 16);
            this.label6.TabIndex = 35;
            this.label6.Text = "No. of doctors  :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // updatenoofdoctors
            // 
            this.updatenoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofdoctors.HintText = "";
            this.updatenoofdoctors.isPassword = false;
            this.updatenoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofdoctors.LineThickness = 3;
            this.updatenoofdoctors.Location = new System.Drawing.Point(129, 343);
            this.updatenoofdoctors.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenoofdoctors.MaxLength = 32767;
            this.updatenoofdoctors.Name = "updatenoofdoctors";
            this.updatenoofdoctors.Size = new System.Drawing.Size(232, 38);
            this.updatenoofdoctors.TabIndex = 34;
            this.updatenoofdoctors.Text = "Varchar e.g (X doctors on board)";
            this.updatenoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofdoctors.OnValueChanged += new System.EventHandler(this.updatenoofdoctors_OnValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(3, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 16);
            this.label5.TabIndex = 33;
            this.label5.Text = "No. of wards :";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // updatenoofwards
            // 
            this.updatenoofwards.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofwards.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofwards.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofwards.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofwards.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofwards.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofwards.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofwards.HintText = "";
            this.updatenoofwards.isPassword = false;
            this.updatenoofwards.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofwards.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofwards.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofwards.LineThickness = 3;
            this.updatenoofwards.Location = new System.Drawing.Point(129, 242);
            this.updatenoofwards.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenoofwards.MaxLength = 32767;
            this.updatenoofwards.Name = "updatenoofwards";
            this.updatenoofwards.Size = new System.Drawing.Size(170, 38);
            this.updatenoofwards.TabIndex = 32;
            this.updatenoofwards.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofwards.OnValueChanged += new System.EventHandler(this.updatenoofwards_OnValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(3, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "Hospital Address :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // updatehospitaladdress
            // 
            this.updatehospitaladdress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatehospitaladdress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatehospitaladdress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatehospitaladdress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatehospitaladdress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatehospitaladdress.ForeColor = System.Drawing.Color.DimGray;
            this.updatehospitaladdress.HintForeColor = System.Drawing.Color.Empty;
            this.updatehospitaladdress.HintText = "";
            this.updatehospitaladdress.isPassword = false;
            this.updatehospitaladdress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitaladdress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitaladdress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitaladdress.LineThickness = 3;
            this.updatehospitaladdress.Location = new System.Drawing.Point(129, 187);
            this.updatehospitaladdress.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatehospitaladdress.MaxLength = 32767;
            this.updatehospitaladdress.Name = "updatehospitaladdress";
            this.updatehospitaladdress.Size = new System.Drawing.Size(170, 38);
            this.updatehospitaladdress.TabIndex = 30;
            this.updatehospitaladdress.Text = "Varchar";
            this.updatehospitaladdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatehospitaladdress.OnValueChanged += new System.EventHandler(this.updatehospitaladdress_OnValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(3, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "Hospital Name :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // updatehospitalname
            // 
            this.updatehospitalname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatehospitalname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatehospitalname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatehospitalname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatehospitalname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatehospitalname.ForeColor = System.Drawing.Color.DimGray;
            this.updatehospitalname.HintForeColor = System.Drawing.Color.Empty;
            this.updatehospitalname.HintText = "";
            this.updatehospitalname.isPassword = false;
            this.updatehospitalname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalname.LineThickness = 3;
            this.updatehospitalname.Location = new System.Drawing.Point(129, 137);
            this.updatehospitalname.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatehospitalname.MaxLength = 32767;
            this.updatehospitalname.Name = "updatehospitalname";
            this.updatehospitalname.Size = new System.Drawing.Size(170, 38);
            this.updatehospitalname.TabIndex = 28;
            this.updatehospitalname.Text = "Varchar";
            this.updatehospitalname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatehospitalname.OnValueChanged += new System.EventHandler(this.updatehospitalname_OnValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(5, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 27;
            this.label2.Text = "Hospital ID :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // updatehospitalid
            // 
            this.updatehospitalid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatehospitalid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatehospitalid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatehospitalid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatehospitalid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatehospitalid.ForeColor = System.Drawing.Color.DimGray;
            this.updatehospitalid.HintForeColor = System.Drawing.Color.Empty;
            this.updatehospitalid.HintText = "";
            this.updatehospitalid.isPassword = false;
            this.updatehospitalid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatehospitalid.LineThickness = 3;
            this.updatehospitalid.Location = new System.Drawing.Point(128, 92);
            this.updatehospitalid.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatehospitalid.MaxLength = 32767;
            this.updatehospitalid.Name = "updatehospitalid";
            this.updatehospitalid.Size = new System.Drawing.Size(168, 38);
            this.updatehospitalid.TabIndex = 26;
            this.updatehospitalid.Text = "Varchar";
            this.updatehospitalid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatehospitalid.OnValueChanged += new System.EventHandler(this.updatehospitalid_OnValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(0, 654);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 16);
            this.label13.TabIndex = 49;
            this.label13.Text = "No. of Visitors :";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // updatenoofvisitors
            // 
            this.updatenoofvisitors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenoofvisitors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenoofvisitors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenoofvisitors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenoofvisitors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenoofvisitors.ForeColor = System.Drawing.Color.DimGray;
            this.updatenoofvisitors.HintForeColor = System.Drawing.Color.Empty;
            this.updatenoofvisitors.HintText = "";
            this.updatenoofvisitors.isPassword = false;
            this.updatenoofvisitors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofvisitors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofvisitors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenoofvisitors.LineThickness = 3;
            this.updatenoofvisitors.Location = new System.Drawing.Point(126, 642);
            this.updatenoofvisitors.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenoofvisitors.MaxLength = 32767;
            this.updatenoofvisitors.Name = "updatenoofvisitors";
            this.updatenoofvisitors.Size = new System.Drawing.Size(170, 38);
            this.updatenoofvisitors.TabIndex = 48;
            this.updatenoofvisitors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenoofvisitors.OnValueChanged += new System.EventHandler(this.updatenoofvisitors_OnValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(0, 605);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "No. of Test kits";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // updatenooftestkits
            // 
            this.updatenooftestkits.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.updatenooftestkits.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.updatenooftestkits.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.updatenooftestkits.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatenooftestkits.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.updatenooftestkits.ForeColor = System.Drawing.Color.DimGray;
            this.updatenooftestkits.HintForeColor = System.Drawing.Color.Empty;
            this.updatenooftestkits.HintText = "";
            this.updatenooftestkits.isPassword = false;
            this.updatenooftestkits.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooftestkits.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooftestkits.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatenooftestkits.LineThickness = 3;
            this.updatenooftestkits.Location = new System.Drawing.Point(126, 593);
            this.updatenooftestkits.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.updatenooftestkits.MaxLength = 32767;
            this.updatenooftestkits.Name = "updatenooftestkits";
            this.updatenooftestkits.Size = new System.Drawing.Size(170, 38);
            this.updatenooftestkits.TabIndex = 46;
            this.updatenooftestkits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.updatenooftestkits.OnValueChanged += new System.EventHandler(this.updatenooftestkits_OnValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(322, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 50;
            this.label1.Text = "Updated with :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // withhid
            // 
            this.withhid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withhid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withhid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withhid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withhid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withhid.ForeColor = System.Drawing.Color.DimGray;
            this.withhid.HintForeColor = System.Drawing.Color.Empty;
            this.withhid.HintText = "";
            this.withhid.isPassword = false;
            this.withhid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhid.LineThickness = 3;
            this.withhid.Location = new System.Drawing.Point(432, 92);
            this.withhid.Margin = new System.Windows.Forms.Padding(5);
            this.withhid.MaxLength = 32767;
            this.withhid.Name = "withhid";
            this.withhid.Size = new System.Drawing.Size(168, 38);
            this.withhid.TabIndex = 51;
            this.withhid.Text = "Varchar";
            this.withhid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withhid.OnValueChanged += new System.EventHandler(this.withhid_OnValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(322, 149);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 16);
            this.label14.TabIndex = 53;
            this.label14.Text = "Updated with :";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // withhname
            // 
            this.withhname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withhname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withhname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withhname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withhname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withhname.ForeColor = System.Drawing.Color.DimGray;
            this.withhname.HintForeColor = System.Drawing.Color.Empty;
            this.withhname.HintText = "";
            this.withhname.isPassword = false;
            this.withhname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhname.LineThickness = 3;
            this.withhname.Location = new System.Drawing.Point(433, 137);
            this.withhname.Margin = new System.Windows.Forms.Padding(5);
            this.withhname.MaxLength = 32767;
            this.withhname.Name = "withhname";
            this.withhname.Size = new System.Drawing.Size(170, 38);
            this.withhname.TabIndex = 52;
            this.withhname.Text = "Varchar";
            this.withhname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withhname.OnValueChanged += new System.EventHandler(this.withhname_OnValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(322, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 16);
            this.label15.TabIndex = 57;
            this.label15.Text = "Updated with :";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // withnoofwards
            // 
            this.withnoofwards.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofwards.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofwards.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofwards.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofwards.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofwards.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofwards.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofwards.HintText = "";
            this.withnoofwards.isPassword = false;
            this.withnoofwards.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofwards.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofwards.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofwards.LineThickness = 3;
            this.withnoofwards.Location = new System.Drawing.Point(433, 242);
            this.withnoofwards.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofwards.MaxLength = 32767;
            this.withnoofwards.Name = "withnoofwards";
            this.withnoofwards.Size = new System.Drawing.Size(170, 38);
            this.withnoofwards.TabIndex = 56;
            this.withnoofwards.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofwards.OnValueChanged += new System.EventHandler(this.withnoofwards_OnValueChanged);
            // 
            // withhaddress
            // 
            this.withhaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withhaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withhaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withhaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withhaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withhaddress.ForeColor = System.Drawing.Color.DimGray;
            this.withhaddress.HintForeColor = System.Drawing.Color.Empty;
            this.withhaddress.HintText = "";
            this.withhaddress.isPassword = false;
            this.withhaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withhaddress.LineThickness = 3;
            this.withhaddress.Location = new System.Drawing.Point(432, 187);
            this.withhaddress.Margin = new System.Windows.Forms.Padding(5);
            this.withhaddress.MaxLength = 32767;
            this.withhaddress.Name = "withhaddress";
            this.withhaddress.Size = new System.Drawing.Size(168, 38);
            this.withhaddress.TabIndex = 55;
            this.withhaddress.Text = "Varchar";
            this.withhaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withhaddress.OnValueChanged += new System.EventHandler(this.withhaddress_OnValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(322, 199);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 16);
            this.label16.TabIndex = 54;
            this.label16.Text = "Updated with :";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(369, 355);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 16);
            this.label17.TabIndex = 61;
            this.label17.Text = "Updated with :";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // withnoofdoctors
            // 
            this.withnoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofdoctors.HintText = "";
            this.withnoofdoctors.isPassword = false;
            this.withnoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofdoctors.LineThickness = 3;
            this.withnoofdoctors.Location = new System.Drawing.Point(480, 343);
            this.withnoofdoctors.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofdoctors.MaxLength = 32767;
            this.withnoofdoctors.Name = "withnoofdoctors";
            this.withnoofdoctors.Size = new System.Drawing.Size(170, 38);
            this.withnoofdoctors.TabIndex = 60;
            this.withnoofdoctors.Text = "Varchar e.g (X doctors on board)";
            this.withnoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofdoctors.OnValueChanged += new System.EventHandler(this.withnoofdoctors_OnValueChanged);
            // 
            // withstaffcount
            // 
            this.withstaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withstaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withstaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withstaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withstaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withstaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.withstaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.withstaffcount.HintText = "";
            this.withstaffcount.isPassword = false;
            this.withstaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withstaffcount.LineThickness = 3;
            this.withstaffcount.Location = new System.Drawing.Point(432, 293);
            this.withstaffcount.Margin = new System.Windows.Forms.Padding(5);
            this.withstaffcount.MaxLength = 32767;
            this.withstaffcount.Name = "withstaffcount";
            this.withstaffcount.Size = new System.Drawing.Size(168, 38);
            this.withstaffcount.TabIndex = 59;
            this.withstaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withstaffcount.OnValueChanged += new System.EventHandler(this.withstaffcount_OnValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label18.Location = new System.Drawing.Point(322, 305);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 16);
            this.label18.TabIndex = 58;
            this.label18.Text = "Updated with :";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // withnoofventilators
            // 
            this.withnoofventilators.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofventilators.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofventilators.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofventilators.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofventilators.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofventilators.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofventilators.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofventilators.HintText = "";
            this.withnoofventilators.isPassword = false;
            this.withnoofventilators.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilators.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilators.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofventilators.LineThickness = 3;
            this.withnoofventilators.Location = new System.Drawing.Point(432, 445);
            this.withnoofventilators.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofventilators.MaxLength = 32767;
            this.withnoofventilators.Name = "withnoofventilators";
            this.withnoofventilators.Size = new System.Drawing.Size(168, 38);
            this.withnoofventilators.TabIndex = 65;
            this.withnoofventilators.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofventilators.OnValueChanged += new System.EventHandler(this.withnoofventilators_OnValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label19.Location = new System.Drawing.Point(322, 457);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 16);
            this.label19.TabIndex = 64;
            this.label19.Text = "Updated with :";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label20.Location = new System.Drawing.Point(322, 406);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 16);
            this.label20.TabIndex = 63;
            this.label20.Text = "Updated with :";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // withnoofnurses
            // 
            this.withnoofnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofnurses.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofnurses.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofnurses.HintText = "";
            this.withnoofnurses.isPassword = false;
            this.withnoofnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofnurses.LineThickness = 3;
            this.withnoofnurses.Location = new System.Drawing.Point(433, 394);
            this.withnoofnurses.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofnurses.MaxLength = 32767;
            this.withnoofnurses.Name = "withnoofnurses";
            this.withnoofnurses.Size = new System.Drawing.Size(170, 38);
            this.withnoofnurses.TabIndex = 62;
            this.withnoofnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofnurses.OnValueChanged += new System.EventHandler(this.withnoofnurses_OnValueChanged);
            // 
            // withnooflabs
            // 
            this.withnooflabs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnooflabs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnooflabs.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnooflabs.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnooflabs.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnooflabs.ForeColor = System.Drawing.Color.DimGray;
            this.withnooflabs.HintForeColor = System.Drawing.Color.Empty;
            this.withnooflabs.HintText = "";
            this.withnooflabs.isPassword = false;
            this.withnooflabs.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooflabs.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooflabs.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooflabs.LineThickness = 3;
            this.withnooflabs.Location = new System.Drawing.Point(432, 543);
            this.withnooflabs.Margin = new System.Windows.Forms.Padding(5);
            this.withnooflabs.MaxLength = 32767;
            this.withnooflabs.Name = "withnooflabs";
            this.withnooflabs.Size = new System.Drawing.Size(168, 38);
            this.withnooflabs.TabIndex = 69;
            this.withnooflabs.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnooflabs.OnValueChanged += new System.EventHandler(this.withnooflabs_OnValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label21.Location = new System.Drawing.Point(322, 555);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 16);
            this.label21.TabIndex = 68;
            this.label21.Text = "Updated with :";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label22.Location = new System.Drawing.Point(322, 504);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(102, 16);
            this.label22.TabIndex = 67;
            this.label22.Text = "Updated with :";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // withpatientcount
            // 
            this.withpatientcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withpatientcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withpatientcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withpatientcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withpatientcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withpatientcount.ForeColor = System.Drawing.Color.DimGray;
            this.withpatientcount.HintForeColor = System.Drawing.Color.Empty;
            this.withpatientcount.HintText = "";
            this.withpatientcount.isPassword = false;
            this.withpatientcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withpatientcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withpatientcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withpatientcount.LineThickness = 3;
            this.withpatientcount.Location = new System.Drawing.Point(433, 492);
            this.withpatientcount.Margin = new System.Windows.Forms.Padding(5);
            this.withpatientcount.MaxLength = 32767;
            this.withpatientcount.Name = "withpatientcount";
            this.withpatientcount.Size = new System.Drawing.Size(170, 38);
            this.withpatientcount.TabIndex = 66;
            this.withpatientcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withpatientcount.OnValueChanged += new System.EventHandler(this.withpatientcount_OnValueChanged);
            // 
            // withnoofvisitors
            // 
            this.withnoofvisitors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnoofvisitors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnoofvisitors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnoofvisitors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnoofvisitors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnoofvisitors.ForeColor = System.Drawing.Color.DimGray;
            this.withnoofvisitors.HintForeColor = System.Drawing.Color.Empty;
            this.withnoofvisitors.HintText = "";
            this.withnoofvisitors.isPassword = false;
            this.withnoofvisitors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofvisitors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofvisitors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnoofvisitors.LineThickness = 3;
            this.withnoofvisitors.Location = new System.Drawing.Point(432, 644);
            this.withnoofvisitors.Margin = new System.Windows.Forms.Padding(5);
            this.withnoofvisitors.MaxLength = 32767;
            this.withnoofvisitors.Name = "withnoofvisitors";
            this.withnoofvisitors.Size = new System.Drawing.Size(168, 38);
            this.withnoofvisitors.TabIndex = 73;
            this.withnoofvisitors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnoofvisitors.OnValueChanged += new System.EventHandler(this.withnoofvisitors_OnValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label23.Location = new System.Drawing.Point(322, 656);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 16);
            this.label23.TabIndex = 72;
            this.label23.Text = "Updated with :";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label24.Location = new System.Drawing.Point(322, 605);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(102, 16);
            this.label24.TabIndex = 71;
            this.label24.Text = "Updated with :";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // withnooftestkits
            // 
            this.withnooftestkits.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.withnooftestkits.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.withnooftestkits.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.withnooftestkits.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.withnooftestkits.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.withnooftestkits.ForeColor = System.Drawing.Color.DimGray;
            this.withnooftestkits.HintForeColor = System.Drawing.Color.Empty;
            this.withnooftestkits.HintText = "";
            this.withnooftestkits.isPassword = false;
            this.withnooftestkits.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooftestkits.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooftestkits.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.withnooftestkits.LineThickness = 3;
            this.withnooftestkits.Location = new System.Drawing.Point(433, 593);
            this.withnooftestkits.Margin = new System.Windows.Forms.Padding(5);
            this.withnooftestkits.MaxLength = 32767;
            this.withnooftestkits.Name = "withnooftestkits";
            this.withnooftestkits.Size = new System.Drawing.Size(170, 38);
            this.withnooftestkits.TabIndex = 70;
            this.withnooftestkits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.withnooftestkits.OnValueChanged += new System.EventHandler(this.withnooftestkits_OnValueChanged);
            // 
            // updatebutton
            // 
            this.updatebutton.ActiveBorderThickness = 1;
            this.updatebutton.ActiveCornerRadius = 20;
            this.updatebutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.BackColor = System.Drawing.Color.White;
            this.updatebutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updatebutton.BackgroundImage")));
            this.updatebutton.ButtonText = "Update";
            this.updatebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatebutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatebutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.IdleBorderThickness = 1;
            this.updatebutton.IdleCornerRadius = 20;
            this.updatebutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.IdleForecolor = System.Drawing.Color.White;
            this.updatebutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatebutton.Location = new System.Drawing.Point(586, 687);
            this.updatebutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updatebutton.Name = "updatebutton";
            this.updatebutton.Size = new System.Drawing.Size(93, 51);
            this.updatebutton.TabIndex = 74;
            this.updatebutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updatebutton.Click += new System.EventHandler(this.updatebutton_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label25.Location = new System.Drawing.Point(4, 32);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(321, 32);
            this.label25.TabIndex = 75;
            this.label25.Text = "Hospital Updation Page";
            // 
            // updatehospitalcontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label25);
            this.Controls.Add(this.updatebutton);
            this.Controls.Add(this.withnoofvisitors);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.withnooftestkits);
            this.Controls.Add(this.withnooflabs);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.withpatientcount);
            this.Controls.Add(this.withnoofventilators);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.withnoofnurses);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.withnoofdoctors);
            this.Controls.Add(this.withstaffcount);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.withnoofwards);
            this.Controls.Add(this.withhaddress);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.withhname);
            this.Controls.Add(this.withhid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.updatenoofvisitors);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.updatenooftestkits);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.updatenooflabs);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.updatepatientcount);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updatenoofventilators);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.updatenoofnurses);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.updatestaffcount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.updatenoofdoctors);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.updatenoofwards);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.updatehospitaladdress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.updatehospitalname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.updatehospitalid);
            this.Name = "updatehospitalcontrol";
            this.Size = new System.Drawing.Size(694, 742);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenooflabs;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatepatientcount;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofventilators;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofnurses;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatestaffcount;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofdoctors;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofwards;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatehospitaladdress;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatehospitalname;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatehospitalid;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenoofvisitors;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuMaterialTextbox updatenooftestkits;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withhid;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withhname;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofwards;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withhaddress;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofdoctors;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withstaffcount;
        private System.Windows.Forms.Label label18;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofventilators;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofnurses;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnooflabs;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withpatientcount;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnoofvisitors;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Bunifu.Framework.UI.BunifuMaterialTextbox withnooftestkits;
        private Bunifu.Framework.UI.BunifuThinButton2 updatebutton;
        private System.Windows.Forms.Label label25;
    }
}
