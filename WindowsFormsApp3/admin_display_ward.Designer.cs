﻿namespace WindowsFormsApp3
{
    partial class admin_display_ward
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_display_ward));
            this.warddatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.updatepatientbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.warddatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // warddatagrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.warddatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.warddatagrid.BackgroundColor = System.Drawing.Color.White;
            this.warddatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.warddatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.warddatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.warddatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.warddatagrid.DoubleBuffered = true;
            this.warddatagrid.EnableHeadersVisualStyles = false;
            this.warddatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.warddatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.warddatagrid.Location = new System.Drawing.Point(0, 0);
            this.warddatagrid.Name = "warddatagrid";
            this.warddatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.warddatagrid.Size = new System.Drawing.Size(588, 407);
            this.warddatagrid.TabIndex = 1;
            // 
            // updatepatientbutton
            // 
            this.updatepatientbutton.ActiveBorderThickness = 1;
            this.updatepatientbutton.ActiveCornerRadius = 20;
            this.updatepatientbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.BackColor = System.Drawing.Color.White;
            this.updatepatientbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updatepatientbutton.BackgroundImage")));
            this.updatepatientbutton.ButtonText = "display";
            this.updatepatientbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatepatientbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatepatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleBorderThickness = 1;
            this.updatepatientbutton.IdleCornerRadius = 20;
            this.updatepatientbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleForecolor = System.Drawing.Color.White;
            this.updatepatientbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.Location = new System.Drawing.Point(463, 451);
            this.updatepatientbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updatepatientbutton.Name = "updatepatientbutton";
            this.updatepatientbutton.Size = new System.Drawing.Size(93, 51);
            this.updatepatientbutton.TabIndex = 95;
            this.updatepatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updatepatientbutton.Click += new System.EventHandler(this.updatepatientbutton_Click);
            // 
            // admin_display_ward
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.updatepatientbutton);
            this.Controls.Add(this.warddatagrid);
            this.Name = "admin_display_ward";
            this.Size = new System.Drawing.Size(588, 522);
            ((System.ComponentModel.ISupportInitialize)(this.warddatagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomDataGrid warddatagrid;
        private Bunifu.Framework.UI.BunifuThinButton2 updatepatientbutton;
    }
}
