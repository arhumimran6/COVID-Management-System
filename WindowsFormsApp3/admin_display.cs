﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class admin_display : Form
    {
        public admin_display()
        {
            InitializeComponent();
            admin_display_ward1.Hide();
            admin_display_patient1.Hide();
            sidepanel2.Top = displayadminhospitalbutton.Top;
            sidepanel2.Height = displayadminhospitalbutton.Height;
        }

        private void updateadminhospitalbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayadminhospitalbutton.Top;
            sidepanel2.Height = displayadminhospitalbutton.Height;
            admin_display_ward1.Hide();
            admin_display_patient1.Hide();
            admin_display_hospital1.Show();
        }

        private void displayadminwardbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayadminwardbutton.Top;
            sidepanel2.Height = displayadminwardbutton.Height;
            admin_display_hospital1.Hide();
            admin_display_patient1.Hide();
            admin_display_ward1.Show();
        }

        private void displayadminpatientbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = displayadminpatientbutton.Top;
            sidepanel2.Height = displayadminpatientbutton.Height;
            admin_display_hospital1.Hide();
            admin_display_ward1.Hide();
            admin_display_patient1.Show();
        }
    }
}
