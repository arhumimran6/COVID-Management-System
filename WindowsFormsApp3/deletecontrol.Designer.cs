﻿namespace WindowsFormsApp3
{
    partial class deletecontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.deletewardbutton = new System.Windows.Forms.LinkLabel();
            this.deletepatientbutton = new System.Windows.Forms.LinkLabel();
            this.deletehospitalbutton = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Which table do you want to display ?";
            // 
            // deletewardbutton
            // 
            this.deletewardbutton.AutoSize = true;
            this.deletewardbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletewardbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.deletewardbutton.Location = new System.Drawing.Point(4, 74);
            this.deletewardbutton.Name = "deletewardbutton";
            this.deletewardbutton.Size = new System.Drawing.Size(90, 20);
            this.deletewardbutton.TabIndex = 7;
            this.deletewardbutton.TabStop = true;
            this.deletewardbutton.Text = "Ward Table";
            // 
            // deletepatientbutton
            // 
            this.deletepatientbutton.AutoSize = true;
            this.deletepatientbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletepatientbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.deletepatientbutton.Location = new System.Drawing.Point(4, 130);
            this.deletepatientbutton.Name = "deletepatientbutton";
            this.deletepatientbutton.Size = new System.Drawing.Size(102, 20);
            this.deletepatientbutton.TabIndex = 8;
            this.deletepatientbutton.TabStop = true;
            this.deletepatientbutton.Text = "Patient Table";
            // 
            // deletehospitalbutton
            // 
            this.deletehospitalbutton.AutoSize = true;
            this.deletehospitalbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletehospitalbutton.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.deletehospitalbutton.Location = new System.Drawing.Point(4, 184);
            this.deletehospitalbutton.Name = "deletehospitalbutton";
            this.deletehospitalbutton.Size = new System.Drawing.Size(110, 20);
            this.deletehospitalbutton.TabIndex = 9;
            this.deletehospitalbutton.TabStop = true;
            this.deletehospitalbutton.Text = "Hospital Table";
            // 
            // deletecontrol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.deletehospitalbutton);
            this.Controls.Add(this.deletepatientbutton);
            this.Controls.Add(this.deletewardbutton);
            this.Controls.Add(this.label1);
            this.Name = "deletecontrol";
            this.Size = new System.Drawing.Size(503, 282);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel deletewardbutton;
        private System.Windows.Forms.LinkLabel deletepatientbutton;
        private System.Windows.Forms.LinkLabel deletehospitalbutton;
    }
}
