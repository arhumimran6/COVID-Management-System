﻿namespace WindowsFormsApp3
{
    partial class insertwardcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(insertwardcontrol));
            this.label8 = new System.Windows.Forms.Label();
            this.insertwardaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.insertwardnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.insertnoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.insertnoofbeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.insertwardhospitalid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.insertwardname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.insertwardid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.insertwardstaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.insertavailablebeds = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label11 = new System.Windows.Forms.Label();
            this.insertventilatorsperward = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.insertwardbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(3, 308);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(188, 19);
            this.label8.TabIndex = 33;
            this.label8.Text = "Ward Hospital Address :";
            // 
            // insertwardaddress
            // 
            this.insertwardaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardaddress.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardaddress.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardaddress.HintText = "";
            this.insertwardaddress.isPassword = false;
            this.insertwardaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardaddress.LineThickness = 3;
            this.insertwardaddress.Location = new System.Drawing.Point(236, 287);
            this.insertwardaddress.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardaddress.MaxLength = 32767;
            this.insertwardaddress.Name = "insertwardaddress";
            this.insertwardaddress.Size = new System.Drawing.Size(204, 38);
            this.insertwardaddress.TabIndex = 32;
            this.insertwardaddress.Text = "Varchar";
            this.insertwardaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(3, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 19);
            this.label7.TabIndex = 31;
            this.label7.Text = "No. of Nurses :";
            // 
            // insertwardnurses
            // 
            this.insertwardnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardnurses.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardnurses.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardnurses.HintText = "";
            this.insertwardnurses.isPassword = false;
            this.insertwardnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardnurses.LineThickness = 3;
            this.insertwardnurses.Location = new System.Drawing.Point(236, 188);
            this.insertwardnurses.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardnurses.MaxLength = 32767;
            this.insertwardnurses.Name = "insertwardnurses";
            this.insertwardnurses.Size = new System.Drawing.Size(204, 38);
            this.insertwardnurses.TabIndex = 30;
            this.insertwardnurses.Text = "INT";
            this.insertwardnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(3, 257);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 19);
            this.label6.TabIndex = 29;
            this.label6.Text = "No. of doctors  :";
            // 
            // insertnoofdoctors
            // 
            this.insertnoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofdoctors.HintText = "";
            this.insertnoofdoctors.isPassword = false;
            this.insertnoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineThickness = 3;
            this.insertnoofdoctors.Location = new System.Drawing.Point(236, 238);
            this.insertnoofdoctors.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofdoctors.MaxLength = 32767;
            this.insertnoofdoctors.Name = "insertnoofdoctors";
            this.insertnoofdoctors.Size = new System.Drawing.Size(204, 38);
            this.insertnoofdoctors.TabIndex = 28;
            this.insertnoofdoctors.Text = "INT";
            this.insertnoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(3, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 19);
            this.label5.TabIndex = 27;
            this.label5.Text = "No. of beds :";
            // 
            // insertnoofbeds
            // 
            this.insertnoofbeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofbeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofbeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofbeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofbeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofbeds.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofbeds.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofbeds.HintText = "";
            this.insertnoofbeds.isPassword = false;
            this.insertnoofbeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofbeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofbeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofbeds.LineThickness = 3;
            this.insertnoofbeds.Location = new System.Drawing.Point(236, 137);
            this.insertnoofbeds.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofbeds.MaxLength = 32767;
            this.insertnoofbeds.Name = "insertnoofbeds";
            this.insertnoofbeds.Size = new System.Drawing.Size(204, 38);
            this.insertnoofbeds.TabIndex = 26;
            this.insertnoofbeds.Text = "INT";
            this.insertnoofbeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // insertwardhospitalid
            // 
            this.insertwardhospitalid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardhospitalid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardhospitalid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardhospitalid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardhospitalid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardhospitalid.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardhospitalid.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardhospitalid.HintText = "";
            this.insertwardhospitalid.isPassword = false;
            this.insertwardhospitalid.LineFocusedColor = System.Drawing.Color.White;
            this.insertwardhospitalid.LineIdleColor = System.Drawing.Color.White;
            this.insertwardhospitalid.LineMouseHoverColor = System.Drawing.Color.White;
            this.insertwardhospitalid.LineThickness = 3;
            this.insertwardhospitalid.Location = new System.Drawing.Point(314, 582);
            this.insertwardhospitalid.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardhospitalid.MaxLength = 32767;
            this.insertwardhospitalid.Name = "insertwardhospitalid";
            this.insertwardhospitalid.Size = new System.Drawing.Size(96, 38);
            this.insertwardhospitalid.TabIndex = 24;
            this.insertwardhospitalid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(3, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 19);
            this.label3.TabIndex = 23;
            this.label3.Text = "Ward Name :";
            // 
            // insertwardname
            // 
            this.insertwardname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardname.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardname.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardname.HintText = "";
            this.insertwardname.isPassword = false;
            this.insertwardname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardname.LineThickness = 3;
            this.insertwardname.Location = new System.Drawing.Point(236, 89);
            this.insertwardname.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardname.MaxLength = 32767;
            this.insertwardname.Name = "insertwardname";
            this.insertwardname.Size = new System.Drawing.Size(204, 38);
            this.insertwardname.TabIndex = 22;
            this.insertwardname.Text = "Varchar";
            this.insertwardname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 19);
            this.label2.TabIndex = 21;
            this.label2.Text = "Ward ID :";
            // 
            // insertwardid
            // 
            this.insertwardid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardid.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardid.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardid.HintText = "";
            this.insertwardid.isPassword = false;
            this.insertwardid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardid.LineThickness = 3;
            this.insertwardid.Location = new System.Drawing.Point(236, 37);
            this.insertwardid.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardid.MaxLength = 32767;
            this.insertwardid.Name = "insertwardid";
            this.insertwardid.Size = new System.Drawing.Size(204, 38);
            this.insertwardid.TabIndex = 20;
            this.insertwardid.Text = "Varchar";
            this.insertwardid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 32);
            this.label1.TabIndex = 19;
            this.label1.Text = "Ward Insertion Page";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(3, 417);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 19);
            this.label9.TabIndex = 37;
            this.label9.Text = "Staff count :";
            // 
            // insertwardstaffcount
            // 
            this.insertwardstaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertwardstaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertwardstaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertwardstaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertwardstaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertwardstaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.insertwardstaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.insertwardstaffcount.HintText = "";
            this.insertwardstaffcount.isPassword = false;
            this.insertwardstaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardstaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardstaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardstaffcount.LineThickness = 3;
            this.insertwardstaffcount.Location = new System.Drawing.Point(236, 396);
            this.insertwardstaffcount.Margin = new System.Windows.Forms.Padding(5);
            this.insertwardstaffcount.MaxLength = 32767;
            this.insertwardstaffcount.Name = "insertwardstaffcount";
            this.insertwardstaffcount.Size = new System.Drawing.Size(204, 38);
            this.insertwardstaffcount.TabIndex = 36;
            this.insertwardstaffcount.Text = "INT";
            this.insertwardstaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(3, 366);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 19);
            this.label10.TabIndex = 35;
            this.label10.Text = "Available Beds :";
            // 
            // insertavailablebeds
            // 
            this.insertavailablebeds.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertavailablebeds.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertavailablebeds.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertavailablebeds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertavailablebeds.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertavailablebeds.ForeColor = System.Drawing.Color.DimGray;
            this.insertavailablebeds.HintForeColor = System.Drawing.Color.Empty;
            this.insertavailablebeds.HintText = "";
            this.insertavailablebeds.isPassword = false;
            this.insertavailablebeds.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertavailablebeds.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertavailablebeds.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertavailablebeds.LineThickness = 3;
            this.insertavailablebeds.Location = new System.Drawing.Point(236, 345);
            this.insertavailablebeds.Margin = new System.Windows.Forms.Padding(5);
            this.insertavailablebeds.MaxLength = 32767;
            this.insertavailablebeds.Name = "insertavailablebeds";
            this.insertavailablebeds.Size = new System.Drawing.Size(204, 38);
            this.insertavailablebeds.TabIndex = 34;
            this.insertavailablebeds.Text = "INT";
            this.insertavailablebeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(3, 466);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(225, 19);
            this.label11.TabIndex = 39;
            this.label11.Text = "No. of Ventialators per ward:";
            // 
            // insertventilatorsperward
            // 
            this.insertventilatorsperward.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertventilatorsperward.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertventilatorsperward.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertventilatorsperward.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertventilatorsperward.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertventilatorsperward.ForeColor = System.Drawing.Color.DimGray;
            this.insertventilatorsperward.HintForeColor = System.Drawing.Color.Empty;
            this.insertventilatorsperward.HintText = "";
            this.insertventilatorsperward.isPassword = false;
            this.insertventilatorsperward.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertventilatorsperward.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertventilatorsperward.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertventilatorsperward.LineThickness = 3;
            this.insertventilatorsperward.Location = new System.Drawing.Point(236, 447);
            this.insertventilatorsperward.Margin = new System.Windows.Forms.Padding(5);
            this.insertventilatorsperward.MaxLength = 32767;
            this.insertventilatorsperward.Name = "insertventilatorsperward";
            this.insertventilatorsperward.Size = new System.Drawing.Size(204, 38);
            this.insertventilatorsperward.TabIndex = 38;
            this.insertventilatorsperward.Text = "INT";
            this.insertventilatorsperward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // insertwardbutton
            // 
            this.insertwardbutton.ActiveBorderThickness = 1;
            this.insertwardbutton.ActiveCornerRadius = 20;
            this.insertwardbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.BackColor = System.Drawing.Color.White;
            this.insertwardbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insertwardbutton.BackgroundImage")));
            this.insertwardbutton.ButtonText = "insert";
            this.insertwardbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertwardbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleBorderThickness = 1;
            this.insertwardbutton.IdleCornerRadius = 20;
            this.insertwardbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.IdleForecolor = System.Drawing.Color.White;
            this.insertwardbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertwardbutton.Location = new System.Drawing.Point(493, 618);
            this.insertwardbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.insertwardbutton.Name = "insertwardbutton";
            this.insertwardbutton.Size = new System.Drawing.Size(93, 51);
            this.insertwardbutton.TabIndex = 40;
            this.insertwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertwardbutton.Click += new System.EventHandler(this.insertwardbutton_Click);
            // 
            // insertwardcontrol
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.insertwardbutton);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.insertventilatorsperward);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.insertwardstaffcount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.insertavailablebeds);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.insertwardaddress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.insertwardnurses);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.insertnoofdoctors);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.insertnoofbeds);
            this.Controls.Add(this.insertwardhospitalid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.insertwardname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.insertwardid);
            this.Controls.Add(this.label1);
            this.Name = "insertwardcontrol";
            this.Size = new System.Drawing.Size(600, 673);
            this.Load += new System.EventHandler(this.insertwardcontrol_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardaddress;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardnurses;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofdoctors;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofbeds;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardhospitalid;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardname;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertwardstaffcount;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertavailablebeds;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertventilatorsperward;
        private Bunifu.Framework.UI.BunifuThinButton2 insertwardbutton;
    }
}
