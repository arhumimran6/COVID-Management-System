﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp3
{
    public partial class admin_display_hospital : UserControl
    {
        public admin_display_hospital()
        {
            InitializeComponent();
        }

        private void admin_display_hospital_Load(object sender, EventArgs e)
        {

        }
        SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
        public void display_hospital()
        {
            sqlcon.Open();
            string q = "select * from hospital";
            SqlCommand cmd = new SqlCommand(q, sqlcon);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            hospitaldatagrid.DataSource = dt;
            sqlcon.Close();
        }
        private void updatepatientbutton_Click(object sender, EventArgs e)
        {
            display_hospital();
        }
    }
}
