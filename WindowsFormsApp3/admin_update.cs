﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class admin_update : Form
    {
        public admin_update()
        {
            InitializeComponent();
            updatewardcontrol1.Hide();
            updatepatientcontrol1.Hide();
            sidepanel2.Top = updateadminhospitalbutton.Top;
            sidepanel2.Height = updateadminhospitalbutton.Height;
        }

        private void insertadminhospitalbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = updateadminhospitalbutton.Top;
            sidepanel2.Height = updateadminhospitalbutton.Height;
            updatewardcontrol1.Hide();
            updatehospitalcontrol1.Show();
        }

        private void updateadminwardbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = updateadminwardbutton.Top;
            sidepanel2.Height = updateadminwardbutton.Height;
            updatehospitalcontrol1.Hide();
            updatepatientcontrol1.Hide();
            updatewardcontrol1.Show();
        }

        private void updateadminpatientbutton_Click(object sender, EventArgs e)
        {
            sidepanel2.Top = updateadminpatientbutton.Top;
            sidepanel2.Height = updateadminpatientbutton.Height;
            updatehospitalcontrol1.Hide();
            updatewardcontrol1.Hide();
            updatepatientcontrol1.Show();
        }
    }
}
