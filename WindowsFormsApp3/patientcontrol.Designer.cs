﻿namespace WindowsFormsApp3
{
    partial class patientcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(patientcontrol));
            this.label11 = new System.Windows.Forms.Label();
            this.insertPhistdesc = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.insertPgender = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.insertPage = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.insertPbedno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.insertcoronastatus = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.insertPcond = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.insertPemail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.insertPaddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.insertPname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.insertPid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.insertPcontact = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label15 = new System.Windows.Forms.Label();
            this.insertpatienthospitalid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label16 = new System.Windows.Forms.Label();
            this.insertpatientwardid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label17 = new System.Windows.Forms.Label();
            this.insertpatienttestno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.insertpatientbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(5, 549);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(155, 19);
            this.label11.TabIndex = 43;
            this.label11.Text = "History Description :";
            // 
            // insertPhistdesc
            // 
            this.insertPhistdesc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPhistdesc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPhistdesc.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPhistdesc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPhistdesc.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPhistdesc.ForeColor = System.Drawing.Color.DimGray;
            this.insertPhistdesc.HintForeColor = System.Drawing.Color.Empty;
            this.insertPhistdesc.HintText = "";
            this.insertPhistdesc.isPassword = false;
            this.insertPhistdesc.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPhistdesc.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPhistdesc.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPhistdesc.LineThickness = 3;
            this.insertPhistdesc.Location = new System.Drawing.Point(167, 531);
            this.insertPhistdesc.Margin = new System.Windows.Forms.Padding(5);
            this.insertPhistdesc.MaxLength = 32767;
            this.insertPhistdesc.Name = "insertPhistdesc";
            this.insertPhistdesc.Size = new System.Drawing.Size(231, 38);
            this.insertPhistdesc.TabIndex = 42;
            this.insertPhistdesc.Text = "Varchar e.g(any disease before)";
            this.insertPhistdesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(5, 497);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 19);
            this.label10.TabIndex = 41;
            this.label10.Text = "Patient Gender :";
            // 
            // insertPgender
            // 
            this.insertPgender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPgender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPgender.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPgender.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPgender.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPgender.ForeColor = System.Drawing.Color.DimGray;
            this.insertPgender.HintForeColor = System.Drawing.Color.Empty;
            this.insertPgender.HintText = "";
            this.insertPgender.isPassword = false;
            this.insertPgender.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPgender.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPgender.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPgender.LineThickness = 3;
            this.insertPgender.Location = new System.Drawing.Point(167, 478);
            this.insertPgender.Margin = new System.Windows.Forms.Padding(5);
            this.insertPgender.MaxLength = 32767;
            this.insertPgender.Name = "insertPgender";
            this.insertPgender.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.insertPgender.Size = new System.Drawing.Size(78, 38);
            this.insertPgender.TabIndex = 40;
            this.insertPgender.Text = "Varchar";
            this.insertPgender.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(6, 448);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 19);
            this.label9.TabIndex = 39;
            this.label9.Text = "Patient Age :";
            // 
            // insertPage
            // 
            this.insertPage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPage.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPage.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPage.ForeColor = System.Drawing.Color.DimGray;
            this.insertPage.HintForeColor = System.Drawing.Color.Empty;
            this.insertPage.HintText = "";
            this.insertPage.isPassword = false;
            this.insertPage.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPage.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPage.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPage.LineThickness = 3;
            this.insertPage.Location = new System.Drawing.Point(168, 429);
            this.insertPage.Margin = new System.Windows.Forms.Padding(5);
            this.insertPage.MaxLength = 32767;
            this.insertPage.Name = "insertPage";
            this.insertPage.Size = new System.Drawing.Size(77, 38);
            this.insertPage.TabIndex = 38;
            this.insertPage.Text = "INT";
            this.insertPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(6, 399);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 19);
            this.label8.TabIndex = 37;
            this.label8.Text = "Patient Bed No. :";
            // 
            // insertPbedno
            // 
            this.insertPbedno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPbedno.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPbedno.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPbedno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPbedno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPbedno.ForeColor = System.Drawing.Color.DimGray;
            this.insertPbedno.HintForeColor = System.Drawing.Color.Empty;
            this.insertPbedno.HintText = "";
            this.insertPbedno.isPassword = false;
            this.insertPbedno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPbedno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPbedno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPbedno.LineThickness = 3;
            this.insertPbedno.Location = new System.Drawing.Point(168, 380);
            this.insertPbedno.Margin = new System.Windows.Forms.Padding(5);
            this.insertPbedno.MaxLength = 32767;
            this.insertPbedno.Name = "insertPbedno";
            this.insertPbedno.Size = new System.Drawing.Size(77, 38);
            this.insertPbedno.TabIndex = 36;
            this.insertPbedno.Text = "INT";
            this.insertPbedno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(6, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 19);
            this.label7.TabIndex = 35;
            this.label7.Text = "Corona Status :";
            // 
            // insertcoronastatus
            // 
            this.insertcoronastatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertcoronastatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertcoronastatus.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertcoronastatus.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertcoronastatus.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertcoronastatus.ForeColor = System.Drawing.Color.DimGray;
            this.insertcoronastatus.HintForeColor = System.Drawing.Color.Empty;
            this.insertcoronastatus.HintText = "";
            this.insertcoronastatus.isPassword = false;
            this.insertcoronastatus.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertcoronastatus.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertcoronastatus.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertcoronastatus.LineThickness = 3;
            this.insertcoronastatus.Location = new System.Drawing.Point(148, 279);
            this.insertcoronastatus.Margin = new System.Windows.Forms.Padding(5);
            this.insertcoronastatus.MaxLength = 32767;
            this.insertcoronastatus.Name = "insertcoronastatus";
            this.insertcoronastatus.Size = new System.Drawing.Size(233, 38);
            this.insertcoronastatus.TabIndex = 34;
            this.insertcoronastatus.Text = "Varchar e.g (positive or negative)";
            this.insertcoronastatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(6, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 19);
            this.label6.TabIndex = 33;
            this.label6.Text = "Condition  :";
            // 
            // insertPcond
            // 
            this.insertPcond.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPcond.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPcond.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPcond.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPcond.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPcond.ForeColor = System.Drawing.Color.DimGray;
            this.insertPcond.HintForeColor = System.Drawing.Color.Empty;
            this.insertPcond.HintText = "";
            this.insertPcond.isPassword = false;
            this.insertPcond.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcond.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcond.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcond.LineThickness = 3;
            this.insertPcond.Location = new System.Drawing.Point(148, 329);
            this.insertPcond.Margin = new System.Windows.Forms.Padding(5);
            this.insertPcond.MaxLength = 32767;
            this.insertPcond.Name = "insertPcond";
            this.insertPcond.Size = new System.Drawing.Size(233, 38);
            this.insertPcond.TabIndex = 32;
            this.insertPcond.Text = "Varchar e.g (Fair, critical, Normal)";
            this.insertPcond.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(6, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 19);
            this.label5.TabIndex = 31;
            this.label5.Text = "Patient Email :";
            // 
            // insertPemail
            // 
            this.insertPemail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPemail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPemail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPemail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPemail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPemail.ForeColor = System.Drawing.Color.DimGray;
            this.insertPemail.HintForeColor = System.Drawing.Color.Empty;
            this.insertPemail.HintText = "";
            this.insertPemail.isPassword = false;
            this.insertPemail.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPemail.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPemail.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPemail.LineThickness = 3;
            this.insertPemail.Location = new System.Drawing.Point(148, 228);
            this.insertPemail.Margin = new System.Windows.Forms.Padding(5);
            this.insertPemail.MaxLength = 32767;
            this.insertPemail.Name = "insertPemail";
            this.insertPemail.Size = new System.Drawing.Size(204, 38);
            this.insertPemail.TabIndex = 30;
            this.insertPemail.Text = "Varchar";
            this.insertPemail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(3, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 19);
            this.label4.TabIndex = 54;
            this.label4.Text = "Patient Address :";
            // 
            // insertPaddress
            // 
            this.insertPaddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPaddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPaddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPaddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPaddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPaddress.ForeColor = System.Drawing.Color.DimGray;
            this.insertPaddress.HintForeColor = System.Drawing.Color.Empty;
            this.insertPaddress.HintText = "";
            this.insertPaddress.isPassword = false;
            this.insertPaddress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPaddress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPaddress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPaddress.LineThickness = 3;
            this.insertPaddress.Location = new System.Drawing.Point(145, 181);
            this.insertPaddress.Margin = new System.Windows.Forms.Padding(5);
            this.insertPaddress.MaxLength = 32767;
            this.insertPaddress.Name = "insertPaddress";
            this.insertPaddress.Size = new System.Drawing.Size(204, 38);
            this.insertPaddress.TabIndex = 53;
            this.insertPaddress.Text = "Varchar";
            this.insertPaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(3, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 19);
            this.label3.TabIndex = 52;
            this.label3.Text = "Patient Name :";
            // 
            // insertPname
            // 
            this.insertPname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPname.ForeColor = System.Drawing.Color.DimGray;
            this.insertPname.HintForeColor = System.Drawing.Color.Empty;
            this.insertPname.HintText = "";
            this.insertPname.isPassword = false;
            this.insertPname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPname.LineThickness = 3;
            this.insertPname.Location = new System.Drawing.Point(145, 87);
            this.insertPname.Margin = new System.Windows.Forms.Padding(5);
            this.insertPname.MaxLength = 32767;
            this.insertPname.Name = "insertPname";
            this.insertPname.Size = new System.Drawing.Size(204, 38);
            this.insertPname.TabIndex = 51;
            this.insertPname.Text = "Varchar";
            this.insertPname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 19);
            this.label2.TabIndex = 50;
            this.label2.Text = "Patient ID :";
            // 
            // insertPid
            // 
            this.insertPid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPid.ForeColor = System.Drawing.Color.DimGray;
            this.insertPid.HintForeColor = System.Drawing.Color.Empty;
            this.insertPid.HintText = "";
            this.insertPid.isPassword = false;
            this.insertPid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPid.LineThickness = 3;
            this.insertPid.Location = new System.Drawing.Point(145, 35);
            this.insertPid.Margin = new System.Windows.Forms.Padding(5);
            this.insertPid.MaxLength = 32767;
            this.insertPid.Name = "insertPid";
            this.insertPid.Size = new System.Drawing.Size(204, 38);
            this.insertPid.TabIndex = 49;
            this.insertPid.Text = "Varchar";
            this.insertPid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 32);
            this.label1.TabIndex = 48;
            this.label1.Text = "Patient Insertion Page";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label14.Location = new System.Drawing.Point(3, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 19);
            this.label14.TabIndex = 56;
            this.label14.Text = "Patient Contact :";
            // 
            // insertPcontact
            // 
            this.insertPcontact.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertPcontact.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertPcontact.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertPcontact.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertPcontact.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertPcontact.ForeColor = System.Drawing.Color.DimGray;
            this.insertPcontact.HintForeColor = System.Drawing.Color.Empty;
            this.insertPcontact.HintText = "";
            this.insertPcontact.isPassword = false;
            this.insertPcontact.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcontact.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcontact.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertPcontact.LineThickness = 3;
            this.insertPcontact.Location = new System.Drawing.Point(145, 137);
            this.insertPcontact.Margin = new System.Windows.Forms.Padding(5);
            this.insertPcontact.MaxLength = 32767;
            this.insertPcontact.Name = "insertPcontact";
            this.insertPcontact.Size = new System.Drawing.Size(204, 38);
            this.insertPcontact.TabIndex = 55;
            this.insertPcontact.Text = "Varchar";
            this.insertPcontact.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label15.Location = new System.Drawing.Point(294, 448);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 19);
            this.label15.TabIndex = 58;
            this.label15.Text = "Hospital ID :";
            // 
            // insertpatienthospitalid
            // 
            this.insertpatienthospitalid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertpatienthospitalid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertpatienthospitalid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertpatienthospitalid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertpatienthospitalid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertpatienthospitalid.ForeColor = System.Drawing.Color.DimGray;
            this.insertpatienthospitalid.HintForeColor = System.Drawing.Color.Empty;
            this.insertpatienthospitalid.HintText = "";
            this.insertpatienthospitalid.isPassword = false;
            this.insertpatienthospitalid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienthospitalid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienthospitalid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienthospitalid.LineThickness = 3;
            this.insertpatienthospitalid.Location = new System.Drawing.Point(399, 429);
            this.insertpatienthospitalid.Margin = new System.Windows.Forms.Padding(5);
            this.insertpatienthospitalid.MaxLength = 32767;
            this.insertpatienthospitalid.Name = "insertpatienthospitalid";
            this.insertpatienthospitalid.Size = new System.Drawing.Size(196, 38);
            this.insertpatienthospitalid.TabIndex = 57;
            this.insertpatienthospitalid.Text = "Varchar e.g (HXXX)";
            this.insertpatienthospitalid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label16.Location = new System.Drawing.Point(314, 497);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 19);
            this.label16.TabIndex = 60;
            this.label16.Text = "Ward ID :";
            // 
            // insertpatientwardid
            // 
            this.insertpatientwardid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertpatientwardid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertpatientwardid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertpatientwardid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertpatientwardid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertpatientwardid.ForeColor = System.Drawing.Color.DimGray;
            this.insertpatientwardid.HintForeColor = System.Drawing.Color.Empty;
            this.insertpatientwardid.HintText = "";
            this.insertpatientwardid.isPassword = false;
            this.insertpatientwardid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientwardid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientwardid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientwardid.LineThickness = 3;
            this.insertpatientwardid.Location = new System.Drawing.Point(399, 478);
            this.insertpatientwardid.Margin = new System.Windows.Forms.Padding(5);
            this.insertpatientwardid.MaxLength = 32767;
            this.insertpatientwardid.Name = "insertpatientwardid";
            this.insertpatientwardid.Size = new System.Drawing.Size(196, 38);
            this.insertpatientwardid.TabIndex = 59;
            this.insertpatientwardid.Text = "Varchar e.g (WXXX)";
            this.insertpatientwardid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label17.Location = new System.Drawing.Point(323, 399);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 19);
            this.label17.TabIndex = 62;
            this.label17.Text = "Test no :";
            // 
            // insertpatienttestno
            // 
            this.insertpatienttestno.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertpatienttestno.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertpatienttestno.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertpatienttestno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertpatienttestno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertpatienttestno.ForeColor = System.Drawing.Color.DimGray;
            this.insertpatienttestno.HintForeColor = System.Drawing.Color.Empty;
            this.insertpatienttestno.HintText = "";
            this.insertpatienttestno.isPassword = false;
            this.insertpatienttestno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienttestno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienttestno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatienttestno.LineThickness = 3;
            this.insertpatienttestno.Location = new System.Drawing.Point(399, 381);
            this.insertpatienttestno.Margin = new System.Windows.Forms.Padding(5);
            this.insertpatienttestno.MaxLength = 32767;
            this.insertpatienttestno.Name = "insertpatienttestno";
            this.insertpatienttestno.Size = new System.Drawing.Size(196, 38);
            this.insertpatienttestno.TabIndex = 61;
            this.insertpatienttestno.Text = "Varchar e.g (TXXX)";
            this.insertpatienttestno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // insertpatientbutton
            // 
            this.insertpatientbutton.ActiveBorderThickness = 1;
            this.insertpatientbutton.ActiveCornerRadius = 20;
            this.insertpatientbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.BackColor = System.Drawing.Color.White;
            this.insertpatientbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insertpatientbutton.BackgroundImage")));
            this.insertpatientbutton.ButtonText = "insert";
            this.insertpatientbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertpatientbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.IdleBorderThickness = 1;
            this.insertpatientbutton.IdleCornerRadius = 20;
            this.insertpatientbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.IdleForecolor = System.Drawing.Color.White;
            this.insertpatientbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientbutton.Location = new System.Drawing.Point(462, 618);
            this.insertpatientbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.insertpatientbutton.Name = "insertpatientbutton";
            this.insertpatientbutton.Size = new System.Drawing.Size(93, 51);
            this.insertpatientbutton.TabIndex = 63;
            this.insertpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertpatientbutton.Click += new System.EventHandler(this.insertpatientbutton_Click);
            // 
            // patientcontrol
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.insertpatientbutton);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.insertpatienttestno);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.insertpatientwardid);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.insertpatienthospitalid);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.insertPcontact);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.insertPaddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.insertPname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.insertPid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.insertPhistdesc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.insertPgender);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.insertPage);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.insertPbedno);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.insertcoronastatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.insertPcond);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.insertPemail);
            this.Name = "patientcontrol";
            this.Size = new System.Drawing.Size(600, 673);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPhistdesc;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPgender;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPage;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPbedno;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertcoronastatus;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPcond;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPemail;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPaddress;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPname;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertPcontact;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertpatienthospitalid;
        private System.Windows.Forms.Label label16;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertpatientwardid;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertpatienttestno;
        private Bunifu.Framework.UI.BunifuThinButton2 insertpatientbutton;
    }
}
