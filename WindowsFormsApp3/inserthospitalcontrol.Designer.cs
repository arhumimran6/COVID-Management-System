﻿namespace WindowsFormsApp3
{
    partial class inserthospitalcontrol
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(inserthospitalcontrol));
            this.label1 = new System.Windows.Forms.Label();
            this.inserthospitalid = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.inserthospitalname = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label4 = new System.Windows.Forms.Label();
            this.inserthospitaladdress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.insertnoofwards = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label6 = new System.Windows.Forms.Label();
            this.insertnoofdoctors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.insertstaffcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.insertnoofnurses = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label9 = new System.Windows.Forms.Label();
            this.insertnoofventilators = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.insertpatientcount = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label11 = new System.Windows.Forms.Label();
            this.insertnooflabs = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.insertnooftestkits = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label13 = new System.Windows.Forms.Label();
            this.insertnoofvisitors = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(311, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hospital Insertion Page";
            // 
            // inserthospitalid
            // 
            this.inserthospitalid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.inserthospitalid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.inserthospitalid.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.inserthospitalid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.inserthospitalid.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.inserthospitalid.ForeColor = System.Drawing.Color.DimGray;
            this.inserthospitalid.HintForeColor = System.Drawing.Color.Empty;
            this.inserthospitalid.HintText = "";
            this.inserthospitalid.isPassword = false;
            this.inserthospitalid.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalid.LineThickness = 3;
            this.inserthospitalid.Location = new System.Drawing.Point(145, 35);
            this.inserthospitalid.Margin = new System.Windows.Forms.Padding(5);
            this.inserthospitalid.MaxLength = 32767;
            this.inserthospitalid.Name = "inserthospitalid";
            this.inserthospitalid.Size = new System.Drawing.Size(204, 38);
            this.inserthospitalid.TabIndex = 5;
            this.inserthospitalid.Text = "Varchar";
            this.inserthospitalid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Hospital ID :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label3.Location = new System.Drawing.Point(3, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Hospital Name :";
            // 
            // inserthospitalname
            // 
            this.inserthospitalname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.inserthospitalname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.inserthospitalname.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.inserthospitalname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.inserthospitalname.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.inserthospitalname.ForeColor = System.Drawing.Color.DimGray;
            this.inserthospitalname.HintForeColor = System.Drawing.Color.Empty;
            this.inserthospitalname.HintText = "";
            this.inserthospitalname.isPassword = false;
            this.inserthospitalname.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalname.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalname.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitalname.LineThickness = 3;
            this.inserthospitalname.Location = new System.Drawing.Point(145, 87);
            this.inserthospitalname.Margin = new System.Windows.Forms.Padding(5);
            this.inserthospitalname.MaxLength = 32767;
            this.inserthospitalname.Name = "inserthospitalname";
            this.inserthospitalname.Size = new System.Drawing.Size(204, 38);
            this.inserthospitalname.TabIndex = 7;
            this.inserthospitalname.Text = "Varchar";
            this.inserthospitalname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label4.Location = new System.Drawing.Point(3, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 19);
            this.label4.TabIndex = 10;
            this.label4.Text = "Hospital Address :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // inserthospitaladdress
            // 
            this.inserthospitaladdress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.inserthospitaladdress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.inserthospitaladdress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.inserthospitaladdress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.inserthospitaladdress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.inserthospitaladdress.ForeColor = System.Drawing.Color.DimGray;
            this.inserthospitaladdress.HintForeColor = System.Drawing.Color.Empty;
            this.inserthospitaladdress.HintText = "";
            this.inserthospitaladdress.isPassword = false;
            this.inserthospitaladdress.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitaladdress.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitaladdress.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.inserthospitaladdress.LineThickness = 3;
            this.inserthospitaladdress.Location = new System.Drawing.Point(145, 140);
            this.inserthospitaladdress.Margin = new System.Windows.Forms.Padding(5);
            this.inserthospitaladdress.MaxLength = 32767;
            this.inserthospitaladdress.Name = "inserthospitaladdress";
            this.inserthospitaladdress.Size = new System.Drawing.Size(204, 38);
            this.inserthospitaladdress.TabIndex = 9;
            this.inserthospitaladdress.Text = "Varchar";
            this.inserthospitaladdress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label5.Location = new System.Drawing.Point(3, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "No. of wards :";
            // 
            // insertnoofwards
            // 
            this.insertnoofwards.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofwards.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofwards.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofwards.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofwards.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofwards.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofwards.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofwards.HintText = "";
            this.insertnoofwards.isPassword = false;
            this.insertnoofwards.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofwards.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofwards.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofwards.LineThickness = 3;
            this.insertnoofwards.Location = new System.Drawing.Point(145, 195);
            this.insertnoofwards.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofwards.MaxLength = 32767;
            this.insertnoofwards.Name = "insertnoofwards";
            this.insertnoofwards.Size = new System.Drawing.Size(204, 38);
            this.insertnoofwards.TabIndex = 11;
            this.insertnoofwards.Text = "INT";
            this.insertnoofwards.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label6.Location = new System.Drawing.Point(3, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 19);
            this.label6.TabIndex = 14;
            this.label6.Text = "No. of doctors  :";
            // 
            // insertnoofdoctors
            // 
            this.insertnoofdoctors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofdoctors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofdoctors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofdoctors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofdoctors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofdoctors.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofdoctors.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofdoctors.HintText = "";
            this.insertnoofdoctors.isPassword = false;
            this.insertnoofdoctors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofdoctors.LineThickness = 3;
            this.insertnoofdoctors.Location = new System.Drawing.Point(145, 296);
            this.insertnoofdoctors.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofdoctors.MaxLength = 32767;
            this.insertnoofdoctors.Name = "insertnoofdoctors";
            this.insertnoofdoctors.Size = new System.Drawing.Size(233, 38);
            this.insertnoofdoctors.TabIndex = 13;
            this.insertnoofdoctors.Text = "Varchar e.g (X doctors on board)";
            this.insertnoofdoctors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label7.Location = new System.Drawing.Point(3, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 19);
            this.label7.TabIndex = 16;
            this.label7.Text = "Staff Count :";
            // 
            // insertstaffcount
            // 
            this.insertstaffcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertstaffcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertstaffcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertstaffcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertstaffcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertstaffcount.ForeColor = System.Drawing.Color.DimGray;
            this.insertstaffcount.HintForeColor = System.Drawing.Color.Empty;
            this.insertstaffcount.HintText = "";
            this.insertstaffcount.isPassword = false;
            this.insertstaffcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertstaffcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertstaffcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertstaffcount.LineThickness = 3;
            this.insertstaffcount.Location = new System.Drawing.Point(145, 246);
            this.insertstaffcount.Margin = new System.Windows.Forms.Padding(5);
            this.insertstaffcount.MaxLength = 32767;
            this.insertstaffcount.Name = "insertstaffcount";
            this.insertstaffcount.Size = new System.Drawing.Size(204, 38);
            this.insertstaffcount.TabIndex = 15;
            this.insertstaffcount.Text = "INT";
            this.insertstaffcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label8.Location = new System.Drawing.Point(3, 366);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 19);
            this.label8.TabIndex = 18;
            this.label8.Text = "No. of Nurses :";
            // 
            // insertnoofnurses
            // 
            this.insertnoofnurses.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofnurses.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofnurses.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofnurses.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofnurses.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofnurses.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofnurses.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofnurses.HintText = "";
            this.insertnoofnurses.isPassword = false;
            this.insertnoofnurses.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofnurses.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofnurses.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofnurses.LineThickness = 3;
            this.insertnoofnurses.Location = new System.Drawing.Point(145, 347);
            this.insertnoofnurses.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofnurses.MaxLength = 32767;
            this.insertnoofnurses.Name = "insertnoofnurses";
            this.insertnoofnurses.Size = new System.Drawing.Size(204, 38);
            this.insertnoofnurses.TabIndex = 17;
            this.insertnoofnurses.Text = "INT";
            this.insertnoofnurses.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label9.Location = new System.Drawing.Point(3, 415);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(142, 19);
            this.label9.TabIndex = 20;
            this.label9.Text = "No. of ventilators :";
            // 
            // insertnoofventilators
            // 
            this.insertnoofventilators.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofventilators.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofventilators.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofventilators.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofventilators.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofventilators.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofventilators.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofventilators.HintText = "";
            this.insertnoofventilators.isPassword = false;
            this.insertnoofventilators.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofventilators.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofventilators.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofventilators.LineThickness = 3;
            this.insertnoofventilators.Location = new System.Drawing.Point(145, 396);
            this.insertnoofventilators.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofventilators.MaxLength = 32767;
            this.insertnoofventilators.Name = "insertnoofventilators";
            this.insertnoofventilators.Size = new System.Drawing.Size(204, 38);
            this.insertnoofventilators.TabIndex = 19;
            this.insertnoofventilators.Text = "INT";
            this.insertnoofventilators.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label10.Location = new System.Drawing.Point(2, 464);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 19);
            this.label10.TabIndex = 22;
            this.label10.Text = "Patient Count :";
            // 
            // insertpatientcount
            // 
            this.insertpatientcount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertpatientcount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertpatientcount.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertpatientcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertpatientcount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertpatientcount.ForeColor = System.Drawing.Color.DimGray;
            this.insertpatientcount.HintForeColor = System.Drawing.Color.Empty;
            this.insertpatientcount.HintText = "";
            this.insertpatientcount.isPassword = false;
            this.insertpatientcount.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientcount.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientcount.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertpatientcount.LineThickness = 3;
            this.insertpatientcount.Location = new System.Drawing.Point(144, 445);
            this.insertpatientcount.Margin = new System.Windows.Forms.Padding(5);
            this.insertpatientcount.MaxLength = 32767;
            this.insertpatientcount.Name = "insertpatientcount";
            this.insertpatientcount.Size = new System.Drawing.Size(204, 38);
            this.insertpatientcount.TabIndex = 21;
            this.insertpatientcount.Text = "INT";
            this.insertpatientcount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label11.Location = new System.Drawing.Point(2, 516);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 19);
            this.label11.TabIndex = 25;
            this.label11.Text = "No. of Labs :";
            // 
            // insertnooflabs
            // 
            this.insertnooflabs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnooflabs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnooflabs.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnooflabs.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnooflabs.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnooflabs.ForeColor = System.Drawing.Color.DimGray;
            this.insertnooflabs.HintForeColor = System.Drawing.Color.Empty;
            this.insertnooflabs.HintText = "";
            this.insertnooflabs.isPassword = false;
            this.insertnooflabs.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooflabs.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooflabs.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooflabs.LineThickness = 3;
            this.insertnooflabs.Location = new System.Drawing.Point(144, 497);
            this.insertnooflabs.Margin = new System.Windows.Forms.Padding(5);
            this.insertnooflabs.MaxLength = 32767;
            this.insertnooflabs.Name = "insertnooflabs";
            this.insertnooflabs.Size = new System.Drawing.Size(204, 38);
            this.insertnooflabs.TabIndex = 24;
            this.insertnooflabs.Text = "INT";
            this.insertnooflabs.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label12.Location = new System.Drawing.Point(2, 565);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 19);
            this.label12.TabIndex = 27;
            this.label12.Text = "No. of Test kits";
            // 
            // insertnooftestkits
            // 
            this.insertnooftestkits.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnooftestkits.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnooftestkits.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnooftestkits.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnooftestkits.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnooftestkits.ForeColor = System.Drawing.Color.DimGray;
            this.insertnooftestkits.HintForeColor = System.Drawing.Color.Empty;
            this.insertnooftestkits.HintText = "";
            this.insertnooftestkits.isPassword = false;
            this.insertnooftestkits.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooftestkits.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooftestkits.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnooftestkits.LineThickness = 3;
            this.insertnooftestkits.Location = new System.Drawing.Point(144, 546);
            this.insertnooftestkits.Margin = new System.Windows.Forms.Padding(5);
            this.insertnooftestkits.MaxLength = 32767;
            this.insertnooftestkits.Name = "insertnooftestkits";
            this.insertnooftestkits.Size = new System.Drawing.Size(204, 38);
            this.insertnooftestkits.TabIndex = 26;
            this.insertnooftestkits.Text = "INT";
            this.insertnooftestkits.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.label13.Location = new System.Drawing.Point(2, 614);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 19);
            this.label13.TabIndex = 29;
            this.label13.Text = "No. of Visitors :";
            // 
            // insertnoofvisitors
            // 
            this.insertnoofvisitors.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.insertnoofvisitors.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.insertnoofvisitors.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.insertnoofvisitors.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.insertnoofvisitors.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.insertnoofvisitors.ForeColor = System.Drawing.Color.DimGray;
            this.insertnoofvisitors.HintForeColor = System.Drawing.Color.Empty;
            this.insertnoofvisitors.HintText = "";
            this.insertnoofvisitors.isPassword = false;
            this.insertnoofvisitors.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofvisitors.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofvisitors.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.insertnoofvisitors.LineThickness = 3;
            this.insertnoofvisitors.Location = new System.Drawing.Point(144, 595);
            this.insertnoofvisitors.Margin = new System.Windows.Forms.Padding(5);
            this.insertnoofvisitors.MaxLength = 32767;
            this.insertnoofvisitors.Name = "insertnoofvisitors";
            this.insertnoofvisitors.Size = new System.Drawing.Size(204, 38);
            this.insertnoofvisitors.TabIndex = 28;
            this.insertnoofvisitors.Text = "INT";
            this.insertnoofvisitors.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(461, 211);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 30;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(461, 296);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 31;
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.BackColor = System.Drawing.Color.White;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "insert";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.bunifuThinButton21.Location = new System.Drawing.Point(493, 618);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(93, 51);
            this.bunifuThinButton21.TabIndex = 23;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // inserthospitalcontrol
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.insertnoofvisitors);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.insertnooftestkits);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.insertnooflabs);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.insertpatientcount);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.insertnoofventilators);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.insertnoofnurses);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.insertstaffcount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.insertnoofdoctors);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.insertnoofwards);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.inserthospitaladdress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inserthospitalname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inserthospitalid);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "inserthospitalcontrol";
            this.Size = new System.Drawing.Size(600, 673);
            this.Load += new System.EventHandler(this.inserthospitalcontrol_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox inserthospitalid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox inserthospitalname;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuMaterialTextbox inserthospitaladdress;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofwards;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofdoctors;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertstaffcount;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofnurses;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofventilators;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertpatientcount;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnooflabs;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnooftestkits;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuMaterialTextbox insertnoofvisitors;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}
