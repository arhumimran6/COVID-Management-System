﻿namespace WindowsFormsApp3
{
    partial class admin_insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_insert));
            this.panel1 = new System.Windows.Forms.Panel();
            this.sidepanel1 = new System.Windows.Forms.Panel();
            this.insertadminpatientbutton = new System.Windows.Forms.Button();
            this.insertadminwardbutton = new System.Windows.Forms.Button();
            this.insertadminhospitalbutton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.insertwardcontrol1 = new WindowsFormsApp3.insertwardcontrol();
            this.inserthospitalcontrol1 = new WindowsFormsApp3.inserthospitalcontrol();
            this.patientcontrol1 = new WindowsFormsApp3.patientcontrol();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.sidepanel1);
            this.panel1.Controls.Add(this.insertadminpatientbutton);
            this.panel1.Controls.Add(this.insertadminwardbutton);
            this.panel1.Controls.Add(this.insertadminhospitalbutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 688);
            this.panel1.TabIndex = 1;
            // 
            // sidepanel1
            // 
            this.sidepanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel1.Location = new System.Drawing.Point(0, 128);
            this.sidepanel1.Name = "sidepanel1";
            this.sidepanel1.Size = new System.Drawing.Size(11, 55);
            this.sidepanel1.TabIndex = 4;
            // 
            // insertadminpatientbutton
            // 
            this.insertadminpatientbutton.FlatAppearance.BorderSize = 0;
            this.insertadminpatientbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.insertadminpatientbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertadminpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.insertadminpatientbutton.Location = new System.Drawing.Point(15, 250);
            this.insertadminpatientbutton.Name = "insertadminpatientbutton";
            this.insertadminpatientbutton.Size = new System.Drawing.Size(200, 55);
            this.insertadminpatientbutton.TabIndex = 3;
            this.insertadminpatientbutton.Text = "   Patient";
            this.insertadminpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.insertadminpatientbutton.UseVisualStyleBackColor = true;
            this.insertadminpatientbutton.Click += new System.EventHandler(this.insertadminpatientbutton_Click);
            // 
            // insertadminwardbutton
            // 
            this.insertadminwardbutton.FlatAppearance.BorderSize = 0;
            this.insertadminwardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.insertadminwardbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertadminwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.insertadminwardbutton.Location = new System.Drawing.Point(15, 189);
            this.insertadminwardbutton.Name = "insertadminwardbutton";
            this.insertadminwardbutton.Size = new System.Drawing.Size(200, 55);
            this.insertadminwardbutton.TabIndex = 2;
            this.insertadminwardbutton.Text = "   Ward";
            this.insertadminwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.insertadminwardbutton.UseVisualStyleBackColor = true;
            this.insertadminwardbutton.Click += new System.EventHandler(this.insertadminwardbutton_Click);
            // 
            // insertadminhospitalbutton
            // 
            this.insertadminhospitalbutton.FlatAppearance.BorderSize = 0;
            this.insertadminhospitalbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.insertadminhospitalbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertadminhospitalbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.insertadminhospitalbutton.Location = new System.Drawing.Point(12, 128);
            this.insertadminhospitalbutton.Name = "insertadminhospitalbutton";
            this.insertadminhospitalbutton.Size = new System.Drawing.Size(206, 55);
            this.insertadminhospitalbutton.TabIndex = 1;
            this.insertadminhospitalbutton.Text = "   Hospital";
            this.insertadminhospitalbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.insertadminhospitalbutton.UseVisualStyleBackColor = true;
            this.insertadminhospitalbutton.Click += new System.EventHandler(this.insertadminhomebutton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.patientcontrol1);
            this.panel2.Controls.Add(this.insertwardcontrol1);
            this.panel2.Controls.Add(this.inserthospitalcontrol1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(597, 688);
            this.panel2.TabIndex = 2;
            // 
            // insertwardcontrol1
            // 
            this.insertwardcontrol1.BackColor = System.Drawing.Color.White;
            this.insertwardcontrol1.Location = new System.Drawing.Point(0, 0);
            this.insertwardcontrol1.Name = "insertwardcontrol1";
            this.insertwardcontrol1.Size = new System.Drawing.Size(600, 673);
            this.insertwardcontrol1.TabIndex = 3;
            // 
            // inserthospitalcontrol1
            // 
            this.inserthospitalcontrol1.BackColor = System.Drawing.Color.White;
            this.inserthospitalcontrol1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inserthospitalcontrol1.Location = new System.Drawing.Point(0, 0);
            this.inserthospitalcontrol1.Margin = new System.Windows.Forms.Padding(4);
            this.inserthospitalcontrol1.Name = "inserthospitalcontrol1";
            this.inserthospitalcontrol1.Size = new System.Drawing.Size(593, 688);
            this.inserthospitalcontrol1.TabIndex = 0;
            this.inserthospitalcontrol1.Load += new System.EventHandler(this.inserthospitalcontrol1_Load);
            // 
            // patientcontrol1
            // 
            this.patientcontrol1.BackColor = System.Drawing.Color.White;
            this.patientcontrol1.Location = new System.Drawing.Point(0, 0);
            this.patientcontrol1.Name = "patientcontrol1";
            this.patientcontrol1.Size = new System.Drawing.Size(600, 673);
            this.patientcontrol1.TabIndex = 3;
            // 
            // admin_insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(818, 688);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "admin_insert";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "admin_insert";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button insertadminpatientbutton;
        private System.Windows.Forms.Button insertadminwardbutton;
        private System.Windows.Forms.Button insertadminhospitalbutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel sidepanel1;
        private System.Windows.Forms.Panel panel2;
        private inserthospitalcontrol inserthospitalcontrol1;
        private insertwardcontrol insertwardcontrol1;
        private patientcontrol patientcontrol1;
    }
}