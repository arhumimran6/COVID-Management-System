﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp3
{
    public partial class updatepatientcontrol : UserControl
    {
        public updatepatientcontrol()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection("Data Source=DESKTOP-5BTSG1D\\MSSQLSERVER02;Initial Catalog=credentials;Integrated Security=True");
        private void updatepatientbutton_Click(object sender, EventArgs e)
        {
            sqlcon.Open();

            string q_update_pid = "update patient set P_ID='" + withPid.Text + "' where P_ID='" + updatePid.Text + "'";
            SqlCommand cmd_update_pid = new SqlCommand(q_update_pid, sqlcon);
            cmd_update_pid.ExecuteNonQuery();

            string q_update_pname = "update patient set P_NAME='" + withPname.Text + "'where P_NAME='" + updatePname.Text + "'";
            SqlCommand cmd_update_pname = new SqlCommand(q_update_pname, sqlcon);
            cmd_update_pname.ExecuteNonQuery();

            string q_update_paddress = "update patient set P_ADDRESS='" + withPcontact.Text + "'where P_ADDRESS='" + updatePaddress.Text + "'";
            SqlCommand cmd_update_paddress = new SqlCommand(q_update_paddress, sqlcon);
            cmd_update_paddress.ExecuteNonQuery();

            string q_update_pemail = "update patient set P_EMAIL='" + withPemail.Text + "'where P_EMAIL='" + updatePemail.Text + "'";
            SqlCommand cmd_update_pemail = new SqlCommand(q_update_pemail, sqlcon);
            cmd_update_pemail.ExecuteNonQuery();

            string q_update_coronastatus = "update patient set CORONA_STATUS='" + withPcoronastatus.Text + "'where CORONA_STATUS='" + updatecoronastatus.Text + "'";
            SqlCommand cmd_update_coronastatus = new SqlCommand(q_update_coronastatus, sqlcon);
            cmd_update_coronastatus.ExecuteNonQuery();

            string q_update_pcond = "update patient set CONDITION='" + withPcond.Text + "'where CONDITION='" + updatePcond.Text + "'";
            SqlCommand cmd_update_pcond = new SqlCommand(q_update_pcond, sqlcon);
            cmd_update_pcond.ExecuteNonQuery();

            string q_update_bedno = "update patient set BED_NO='" + withPbedno.Text + "'where CONDITION='" + updatePbedno.Text + "'";
            SqlCommand cmd_update_bedno = new SqlCommand(q_update_bedno, sqlcon);
            cmd_update_bedno.ExecuteNonQuery();

            string q_update_page = "update patient set AGE='" + withPage.Text + "'where AGE='" + updatePage.Text + "'";
            SqlCommand cmd_update_page = new SqlCommand(q_update_page, sqlcon);
            cmd_update_page.ExecuteNonQuery();

            string q_update_pgender = "update patient set GENDER='" + withPgender.Text + "'where GENDER='" + updatePgender.Text + "'";
            SqlCommand cmd_update_pgender = new SqlCommand(q_update_pgender, sqlcon);
            cmd_update_pgender.ExecuteNonQuery();

            string q_update_phistdesc = "update patient set HISTORY_DESCRIPTION='" + withPhistdesc.Text + "'where HISTORY_DESCRIPTION='" + updatePhistdesc.Text + "'";
            SqlCommand cmd_update_phistdesc = new SqlCommand(q_update_phistdesc, sqlcon);
            cmd_update_phistdesc.ExecuteNonQuery();

            sqlcon.Close();
            MessageBox.Show("Updated Successfully");
        }
    }
}
