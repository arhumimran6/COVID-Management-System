﻿namespace WindowsFormsApp3
{
    partial class admin_display_hospital
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_display_hospital));
            this.hospitaldatagrid = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.updatepatientbutton = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.hospitaldatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // hospitaldatagrid
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.hospitaldatagrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.hospitaldatagrid.BackgroundColor = System.Drawing.Color.White;
            this.hospitaldatagrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.hospitaldatagrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.hospitaldatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.hospitaldatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hospitaldatagrid.DoubleBuffered = true;
            this.hospitaldatagrid.EnableHeadersVisualStyles = false;
            this.hospitaldatagrid.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.hospitaldatagrid.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.hospitaldatagrid.Location = new System.Drawing.Point(0, 0);
            this.hospitaldatagrid.Name = "hospitaldatagrid";
            this.hospitaldatagrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.hospitaldatagrid.Size = new System.Drawing.Size(569, 407);
            this.hospitaldatagrid.TabIndex = 0;
            // 
            // updatepatientbutton
            // 
            this.updatepatientbutton.ActiveBorderThickness = 1;
            this.updatepatientbutton.ActiveCornerRadius = 20;
            this.updatepatientbutton.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.BackColor = System.Drawing.Color.White;
            this.updatepatientbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("updatepatientbutton.BackgroundImage")));
            this.updatepatientbutton.ButtonText = "display";
            this.updatepatientbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatepatientbutton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatepatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleBorderThickness = 1;
            this.updatepatientbutton.IdleCornerRadius = 20;
            this.updatepatientbutton.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.IdleForecolor = System.Drawing.Color.White;
            this.updatepatientbutton.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.updatepatientbutton.Location = new System.Drawing.Point(463, 451);
            this.updatepatientbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updatepatientbutton.Name = "updatepatientbutton";
            this.updatepatientbutton.Size = new System.Drawing.Size(93, 51);
            this.updatepatientbutton.TabIndex = 94;
            this.updatepatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.updatepatientbutton.Click += new System.EventHandler(this.updatepatientbutton_Click);
            // 
            // admin_display_hospital
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.updatepatientbutton);
            this.Controls.Add(this.hospitaldatagrid);
            this.Name = "admin_display_hospital";
            this.Size = new System.Drawing.Size(569, 541);
            this.Load += new System.EventHandler(this.admin_display_hospital_Load);
            ((System.ComponentModel.ISupportInitialize)(this.hospitaldatagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomDataGrid hospitaldatagrid;
        private Bunifu.Framework.UI.BunifuThinButton2 updatepatientbutton;
    }
}
