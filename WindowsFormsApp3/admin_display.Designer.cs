﻿namespace WindowsFormsApp3
{
    partial class admin_display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_display));
            this.panel1 = new System.Windows.Forms.Panel();
            this.sidepanel2 = new System.Windows.Forms.Panel();
            this.displayadminpatientbutton = new System.Windows.Forms.Button();
            this.displayadminwardbutton = new System.Windows.Forms.Button();
            this.displayadminhospitalbutton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.admin_display_patient1 = new WindowsFormsApp3.admin_display_patient();
            this.admin_display_ward1 = new WindowsFormsApp3.admin_display_ward();
            this.admin_display_hospital1 = new WindowsFormsApp3.admin_display_hospital();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.panel1.Controls.Add(this.sidepanel2);
            this.panel1.Controls.Add(this.displayadminpatientbutton);
            this.panel1.Controls.Add(this.displayadminwardbutton);
            this.panel1.Controls.Add(this.displayadminhospitalbutton);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 522);
            this.panel1.TabIndex = 3;
            // 
            // sidepanel2
            // 
            this.sidepanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.sidepanel2.Location = new System.Drawing.Point(0, 128);
            this.sidepanel2.Name = "sidepanel2";
            this.sidepanel2.Size = new System.Drawing.Size(11, 55);
            this.sidepanel2.TabIndex = 4;
            // 
            // displayadminpatientbutton
            // 
            this.displayadminpatientbutton.FlatAppearance.BorderSize = 0;
            this.displayadminpatientbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayadminpatientbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayadminpatientbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayadminpatientbutton.Location = new System.Drawing.Point(15, 250);
            this.displayadminpatientbutton.Name = "displayadminpatientbutton";
            this.displayadminpatientbutton.Size = new System.Drawing.Size(200, 55);
            this.displayadminpatientbutton.TabIndex = 3;
            this.displayadminpatientbutton.Text = "   Patient";
            this.displayadminpatientbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayadminpatientbutton.UseVisualStyleBackColor = true;
            this.displayadminpatientbutton.Click += new System.EventHandler(this.displayadminpatientbutton_Click);
            // 
            // displayadminwardbutton
            // 
            this.displayadminwardbutton.FlatAppearance.BorderSize = 0;
            this.displayadminwardbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayadminwardbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayadminwardbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayadminwardbutton.Location = new System.Drawing.Point(15, 189);
            this.displayadminwardbutton.Name = "displayadminwardbutton";
            this.displayadminwardbutton.Size = new System.Drawing.Size(200, 55);
            this.displayadminwardbutton.TabIndex = 2;
            this.displayadminwardbutton.Text = "   Ward";
            this.displayadminwardbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayadminwardbutton.UseVisualStyleBackColor = true;
            this.displayadminwardbutton.Click += new System.EventHandler(this.displayadminwardbutton_Click);
            // 
            // displayadminhospitalbutton
            // 
            this.displayadminhospitalbutton.FlatAppearance.BorderSize = 0;
            this.displayadminhospitalbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayadminhospitalbutton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayadminhospitalbutton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(3)))), ((int)(((byte)(87)))));
            this.displayadminhospitalbutton.Location = new System.Drawing.Point(12, 128);
            this.displayadminhospitalbutton.Name = "displayadminhospitalbutton";
            this.displayadminhospitalbutton.Size = new System.Drawing.Size(206, 55);
            this.displayadminhospitalbutton.TabIndex = 1;
            this.displayadminhospitalbutton.Text = "   Hospital";
            this.displayadminhospitalbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.displayadminhospitalbutton.UseVisualStyleBackColor = true;
            this.displayadminhospitalbutton.Click += new System.EventHandler(this.updateadminhospitalbutton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(157)))), ((int)(((byte)(131)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.admin_display_patient1);
            this.panel2.Controls.Add(this.admin_display_ward1);
            this.panel2.Controls.Add(this.admin_display_hospital1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(221, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(588, 522);
            this.panel2.TabIndex = 4;
            // 
            // admin_display_patient1
            // 
            this.admin_display_patient1.BackColor = System.Drawing.Color.White;
            this.admin_display_patient1.Location = new System.Drawing.Point(0, 0);
            this.admin_display_patient1.Name = "admin_display_patient1";
            this.admin_display_patient1.Size = new System.Drawing.Size(588, 522);
            this.admin_display_patient1.TabIndex = 2;
            // 
            // admin_display_ward1
            // 
            this.admin_display_ward1.BackColor = System.Drawing.Color.White;
            this.admin_display_ward1.Location = new System.Drawing.Point(0, 0);
            this.admin_display_ward1.Name = "admin_display_ward1";
            this.admin_display_ward1.Size = new System.Drawing.Size(588, 522);
            this.admin_display_ward1.TabIndex = 1;
            // 
            // admin_display_hospital1
            // 
            this.admin_display_hospital1.BackColor = System.Drawing.Color.White;
            this.admin_display_hospital1.Location = new System.Drawing.Point(0, 0);
            this.admin_display_hospital1.Name = "admin_display_hospital1";
            this.admin_display_hospital1.Size = new System.Drawing.Size(588, 522);
            this.admin_display_hospital1.TabIndex = 0;
            // 
            // admin_display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(809, 522);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "admin_display";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "admin_display";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel sidepanel2;
        private System.Windows.Forms.Button displayadminpatientbutton;
        private System.Windows.Forms.Button displayadminwardbutton;
        private System.Windows.Forms.Button displayadminhospitalbutton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private admin_display_hospital admin_display_hospital1;
        private admin_display_ward admin_display_ward1;
        private admin_display_patient admin_display_patient1;
    }
}