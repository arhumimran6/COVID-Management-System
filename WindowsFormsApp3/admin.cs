﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp3
{
    public partial class admin : Form
    {
        public admin()
        {
            InitializeComponent();
            home1.Hide();
            sidepanel.Height = adminhomebutton.Height;
            sidepanel.Top = adminhomebutton.Top;
            home1.Show();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            Form1 loginform = new Form1();
            loginform.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            sidepanel.Height = adminhomebutton.Height;
            sidepanel.Top = adminhomebutton.Top; 
            home1.Show();
        }

        private void admininsertbutton_Click(object sender, EventArgs e)
        {
            home1.Hide();
            sidepanel.Height = admininsertbutton.Height;
            sidepanel.Top = admininsertbutton.Top;
            this.Hide();
            admin_insert adminform = new admin_insert();
            adminform.Show();
     
        }

        private void adminupdatebutton_Click(object sender, EventArgs e)
        {
            home1.Hide();
            sidepanel.Height = adminupdatebutton.Height;
            sidepanel.Top = adminupdatebutton.Top;
            this.Hide();
            admin_update update_form = new admin_update();
            update_form.Show();
        }

        private void admindeletebutton_Click(object sender, EventArgs e)
        {
            home1.Hide();
            sidepanel.Height = adminsearchbutton.Height;
            sidepanel.Top = adminsearchbutton.Top;
            this.Hide();
            admin_search search_form = new admin_search();
            search_form.Show();
            
        }

        private void admindisplaybutton_Click(object sender, EventArgs e)
        {
            home1.Hide();
            sidepanel.Top = admindisplaybutton.Top;
            sidepanel.Height = admindisplaybutton.Height;
            this.Hide();
            admin_display admindisplay = new admin_display();
            admindisplay.Show();
        }
    }
}
