create database credentials

create table usernameandpass (username varchar(20), pass varchar(20));

insert into usernameandpass values('Admin','54321')
insert into usernameandpass values('User','12345')

select * from usernameandpass

--Hospital

CREATE TABLE HOSPITAL(H_ID VARCHAR(20) PRIMARY KEY, 
H_NAME VARCHAR(50),
H_ADDRESS VARCHAR(100), 
NUMBER_OF_WARDS INT, STAFF_COUNT INT, NUMBER_OF_DOCTORS VARCHAR(30), 
NUMBER_OF_NURSES INT, NUMBER_OF_VENTILATORS INT, PATIENT_COUNT INT,
NUMBER_OF_LABS INT, NUMBER_OF_TESTKITS INT, NUMBER_OF_VISITORS INT, 
DOCTOR_ID VARCHAR(20) FOREIGN KEY REFERENCES DOCTOR (D_ID), N_ID VARCHAR(20) 
FOREIGN KEY REFERENCES NURSE (NURSE_ID));

insert into HOSPITAL values('H101', 'Mayo Hospital',                      'Adjacent to King Edwards Medical College, Anarkali, Lahore',   8,    40, ' 20 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L101','N101');
insert into HOSPITAL values('H102', 'Allied Hospital',                    'Muslim Town, Jail Road, Faisalabad',                           4,    10, '  2 Doctors Onboard',   6, 15, 50,  10,  50,  40, 'F102','N102');
insert into HOSPITAL values('H103', 'Paragon Medical Complex Hospital',   '47-S, Commercial Zone, Imperial garden, Paragon City, Lahore', 10,   30, ' 34 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L103','N103');
insert into HOSPITAL values('H104', 'Doctors Hospital',                   '152-G/1, Canal Bank, Johar Town, Lahore',                      15,   70, ' 96 Doctors Onboard',  80, 30, 400, 10, 100, 250, 'L104','N104');
insert into HOSPITAL values('H105', 'Health City Hospital',               '91 Midway S, Block H1, Wapda Town, Lahore',                    10,   40, ' 29 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L105','N105');
insert into HOSPITAL values('H106', 'DHQ Hospital',                       'Mall Road, Mall Road, Chiniot Faisalabad',                     10,   10, '  2 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'F106','N106');
insert into HOSPITAL values('H107', 'National Institute of Health',       'Park Rd, Chak Shahzad, Islamabad Capital Territory',           18,   40, ' 10 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'I107','N107');
insert into HOSPITAL values('H108', 'Hameed Latif Hospital',              '14 New Abu Bakar Block, New Garden Town, Lahore',              30,   59, '140 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L108','N108');
insert into HOSPITAL values('H109', 'Health City Hospital',               '91 Midway S, Block H1, Wapda Town, Lahore',                    19,   20, ' 29 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L109','N109');
insert into HOSPITAL values('H110', 'Iqra Medical Complex Hospital',      'Akbar Chowk, Moulana Shoukat Ali Road, Johar Town, Lahore',    21,   20, ' 52 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L110','N110');
insert into HOSPITAL values('H111', 'National Hospital',                  '132/3, Block L, DHA, Sports Stadium, DHA, Lahore',             10,   20, ' 21 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L111','N111');
insert into HOSPITAL values('H112', 'Life Care International Hospital',   'G-10, Markaz, Sector G, Islamabad',                            27,   20, ' 45 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'I112','N112');
insert into HOSPITAL values('H113', 'Shifa International Hospital Ltd',   'Pitras Bukhari Road, H-8/4, Sector H, Islamabad',              50,   69, '222 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'I113','N113');
insert into HOSPITAL values('H114', 'Faisal Hospital',                    '673-A, Canal Bank Road, Peoples Colony, Faisalabad',           35,   10, ' 58 Doctors Onboard',  15, 15, 200, 10, 200 ,100, 'F114','N114');
insert into HOSPITAL values('H115', 'The National Hospital',              'Jinnah Colony, Gulberg Rd, Mohalla Guru Nanakpura, Faisalabad',37,   29, '  6 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'F115','N115');
insert into HOSPITAL values('H116', 'National Hospital & Medical Centre', '32/3, L-Block, DHA, Phase 1, DHA, Lahore',                     30,   36, ' 74 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L116','N116');
insert into HOSPITAL values('H117', 'Fatima Memorial Hospital (Fmh)',     'Shadman Road, Opposite Big Mans Pizza, Shadman, Lahore',       40,   28, ' 74 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L117','N117');
insert into HOSPITAL values('H118', 'Shalamar Hospital',                  '2 Shalimar Link Road, Mughalpura, Lahore',                     26,   40, ' 50 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L118','N118');
insert into HOSPITAL values('H119', 'Faisal Hospital',                    '23-B, Abu-Ul-Asphani Road, Faisal Town, Lahore',               22,   10, ' 47 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L119','N119');
insert into HOSPITAL values('H120', 'Latif Hospital',                     'Near Allah Hoo Chowk, Block E Phase 1, Johar Town, Lahore',    14,   40, ' 30 Doctors Onboard',  15, 15, 200, 10, 200, 100, 'L120','N120');


--PATIENT
CREATE TABLE PATIENT(P_ID VARCHAR(20) PRIMARY KEY, 
P_NAME VARCHAR(30), P_CONTACT VARCHAR(20), 
P_ADDRESS VARCHAR(100), P_EMAIL VARCHAR(50), 
CORONA_STATUS VARCHAR(20), 
HOSPITAL_ID VARCHAR(20) FOREIGN KEY REFERENCES HOSPITAL(H_ID), 
WARD_NO VARCHAR(20) FOREIGN KEY REFERENCES WARD (WARD_ID), 
CONDITION VARCHAR(20), TEST_NO VARCHAR(20) FOREIGN KEY REFERENCES TEST (T_ID), 
BED_NO INT, AGE INT, GENDER VARCHAR(20), HISTORY_DESCRIPTION VARCHAR(30));

drop table PATIENT

insert into PATIENT values('P101', 'Samia Banaras',   '0306-6307801', '15-Roberts Road, Nila Gumbad, Lahore',               'samiabanarus27@gmail.com', 'Positive', 'H101', 'W101', 'Critical',     'T101', 1,  24, 'Female', 'Fever');
insert into PATIENT values('P102', 'Hadier Ejaz',     '0300-6949811', 'Muslim Town, Jail Road, Faisalabad',                 'haiderijaz7785@yahoo.com', 'Positive', 'H102', 'W102', 'Good',         'T102', 2,  22, 'Male',   'Tonsillitis');
insert into PATIENT values('P103', 'Hammad Munir',    '0306-6419641', '7-Shami Road, Cantt, Lahore',                        'm.munir5648237@gmail.com', 'Positive', 'H103', 'W103', 'Good',         'T103', 3,  25, 'Male',   'Cold');
insert into PATIENT values('P104', 'Yousaf Zahid',    '0322-0000372', 'Fortress Stadium, Cantt, Lahore',                    'yousafzahid437@gmail.com', 'Positive', 'H104', 'W104', 'Undetermined', 'T104', 4,  38, 'Male',   'Fever');
insert into PATIENT values('P105', 'Amina Javed',     '0300-6350332', '91 Midway S, Block H1, Wapda Town, Lahore',          'aminajaved1999@yahoo.com', 'Positive', 'H105', 'W105', 'Good',         'T105', 5,  27, 'Female', 'Clear');
insert into PATIENT values('P106', 'Ahsan Saleem',    '0313-4093625', '584-Jinnah Colony, Near Allied Bank, Faisalabad',    'ahsansaleem564@gmail.com', 'Positive', 'H106', 'W106', 'Critical',     'T106', 6,  43, 'Male',   'Minor absence attacks');
insert into PATIENT values('P107', 'Sharjeel Ahmad',  '0306-5639261', 'Park Rd, Chak Shahzad, Islamabad Capital Territory', 'sharjeelahmad7@gmail.com', 'Positive', 'H107', 'W107', 'Critical',     'T107', 7,  55, 'Male',   'Fever');
insert into PATIENT values('P108', 'Muhammad Tayyab', '0333-2480554', '30-KM, Ferozpur Road, Lahore',                       'm.tayyabmahar3@gmail.com', 'Positive', 'H108', 'W108', 'Good',         'T108', 8,  20, 'Male',   'Minor bruises');
insert into PATIENT values('P109', 'Shahzaib Ali',    '0306-9946224', '91 Midway S, Block H1, Wapda Town, Lahore',          'shazaibali5633@yahoo.com', 'Positive', 'H109', 'W109', 'Good',         'T109', 9,  53, 'Male',   'Clear');
insert into PATIENT values('P110', 'Ali Hassan',      '0321-5220975', '220 Bawa Park, Uper Mall, Lahore',                   'aliHassan200@outlook.com', 'Positive', 'H110', 'W110', 'Serious',      'T110', 10, 29, 'Male',   'Fever');
insert into PATIENT values('P111', 'Farrukh Ahmad',   '0321-6121192', '132/3, Block L, DHA, Sports Stadium, DHA, Lahore',   'farrukhahmad89@gmail.com', 'Positive', 'H111', 'W111', 'Fair',         'T111', 11, 32, 'Male',   'Tonsillitis');
insert into PATIENT values('P112', 'Abdul Hanan',     '0300-4433864', 'G-10, Markaz, Sector G, Islamabad',                  'm.abdulhanan88@gmail.com', 'Positive', 'H112', 'W112', 'Critical',     'T112', 12, 42, 'Male',   'Fever');
insert into PATIENT values('P113', 'Babaur Shadzad',  '0306-7832642', 'Pitras Bukhari Road, H-8/4, Sector H, Islamabad',    'babaurshadzad1@gmail.com', 'Positive', 'H113', 'W113', 'Critical',     'T113', 13, 28, 'Male',   'Treated with cancer');
insert into PATIENT values('P114', 'Mubashir Ali',    '0356-4440075', '673-A, Canal Bank Road, Peoples Colony, Faisalabad', 'm.mubashirali0@gmail.com', 'Positive', 'H114', 'W114', 'Serious',      'T114', 14, 25, 'Male',   'Fever');
insert into PATIENT values('P115', 'Talha Tanveer',   '0303-4378261', '50-Gulistan Market, Railway Road, Faisalabad',       'talhahtanveer9@gmail.com', 'Positive', 'H115', 'W115', 'Undetermined', 'T115', 15, 30, 'Male',   'Clear');
insert into PATIENT values('P116', 'Ahmad Safdar',    '0306-2297440', '32/3, L-Block, DHA, Phase 1, DHA, Lahore',           'ehmadsafdar333@yahoo.com', 'Positive', 'H116', 'W116', 'Serious',      'T116', 16, 29, 'Male',   'Cold');
insert into PATIENT values('P117', 'Zeeshan Mustafa', '0312-1001398', '16_d, PUNJ, Lahore',                                 'zeeshanmustafa@gmail.com', 'Positive', 'H117', 'W117', 'Critical',     'T117', 17, 38, 'Male',   'Tonsillitis');
insert into PATIENT values('P118', 'Rabab Zahra',     '0322-3654011', '2 Shalimar Link Road, Mughalpura, Lahore',           'rababzahra0440@gmail.com', 'Positive', 'H118', 'W118', 'Undetermined', 'T118', 18, 37, 'Female', 'Fever');
insert into PATIENT values('P119', 'Salman Ali',      '0341-9200222', '23-B, Abu-Ul-Asphani Road, Faisal Town, Lahore',     'm.salmanali123@yahoo.com', 'Positive', 'H119', 'W119', 'Serious',      'T119', 19, 41, 'Male',   'Treated with cancer');
insert into PATIENT values('P120', 'Esha Iftikhar',   '0308-2299642', '66-a, Mozang Road, Lahore',                          'eshaiftikhar65@gmail.com', 'Positive', 'H120', 'W120', 'Undetermined', 'T120', 20, 33, 'Female', 'Tonsillitis');


--Ward
CREATE TABLE WARD(WARD_ID VARCHAR(20) PRIMARY KEY, 
W_NAME VARCHAR(20), HOSP_ID VARCHAR(20) FOREIGN KEY REFERENCES HOSPITAL(H_ID),
NUM_OF_BEDS INT, NUM_OF_NURSES INT, NUM_OF_DOC INT, W_ADDRESS VARCHAR(20), 
AVAILABLE_BEDS INT, STAFF_COUNT INT, NUM_OF_VENTILATORS_PER_WARD INT);

insert into ward values('W101', 'Emergency department', 'H101', 50,  10,  20,   'Block A', 40,  20,  10);
insert into ward values('W102', 'Emergency department', 'H102', 10,   5,   2,   'Block B', 10,   8,   6);
insert into ward values('W103', 'Emergency department', 'H103',150,  10,  34,   'Block C', 40,  20,  100);
insert into ward values('W104', 'Emergency department', 'H104',180,  10,  96,   'Block D', 17,  20,  50);
insert into ward values('W105', 'Emergency department', 'H105',150,  19,  29,   'Block A', 40,  20,  19);
insert into ward values('W106', 'Emergency department', 'H106',40,   10,   2,   'Block B', 47,  20,  10);
insert into ward values('W107', 'Emergency department', 'H107', 50,  10,  10,   'Block C', 40,  20,  15);
insert into ward values('W108', 'Emergency department', 'H108',243, 110,  140,  'Block B', 55,  20,  10);
insert into ward values('W109', 'Emergency department', 'H109',150,  10,  29,   'Block A', 40,  20,  100);
insert into ward values('W110', 'Emergency department', 'H110',200,  49,  52,   'Block C', 40,  20,  10);
insert into ward values('W111', 'Emergency department', 'H111', 50,  10,  21,   'Block B', 27,  20,  33);
insert into ward values('W112', 'Emergency department', 'H112',190,  10,  45,   'Block A', 28,  20,  10);
insert into ward values('W113', 'Emergency department', 'H113',300, 200,  222,  'Block C', 40,  20,  200);
insert into ward values('W114', 'Emergency department', 'H114',200,  10,  58,   'Block D', 82,  20,  10);
insert into ward values('W115', 'Emergency department', 'H115',30,   10,   6,   'Block B', 40,  20,  10);
insert into ward values('W116', 'Emergency department', 'H116',280,  10,  74,   'Block A', 47,  20,  60);
insert into ward values('W117', 'Emergency department', 'H117',250,  80,  74,   'Block C', 40,  20,  10);
insert into ward values('W118', 'Emergency department', 'H118',250,  40,  50,   'Block A', 49,  20,  30);
insert into ward values('W119', 'Emergency department', 'H119',200,  55,  47,   'Block D', 40,  20,  1270);
insert into ward values('W120', 'Emergency department', 'H120',190,  50,  30,   'Block A', 69,  20,  40);


--Doctor
CREATE TABLE DOCTOR(D_ID VARCHAR(20) PRIMARY KEY, 
DOCTOR_NAME VARCHAR(50), DOCTOR_CONTACT VARCHAR(20), 
DOCTOR_HOSPITAL_ID VARCHAR(50),DOCTOR_ADDRESS VARCHAR(50), DOCTOR_EXPERIENCE VARCHAR(50), 
DOCTOR_CORONA_STATUS VARCHAR(20), D_GENDER VARCHAR(20), DUTY_HOURS INT);

DROP TABLE HOSPITAL
DROP TABLE DOCTOR
SELECT * FROM DOCTOR

insert into doctor values('L101', 'Dr. Rabia Rathore',        '042-99211134', 'H101','Mayo Hospital, Lahore',                       '10 YEARS',  'Negative', 'Female', 10);
insert into doctor values('F102', 'Dr. Nasir Farooq Butt',    '042-99211134', 'H102','Allied Hospital, Faisalabad',                 '15 YEARS',  'Negative', 'Male',   12);
insert into doctor values('L103', 'Dr. Hina Latif',           '042-99211134', 'H103','Paragon Medical Complex Hospital, Lahore',    '12 YEARS',  'Negative', 'Female', 10);
insert into doctor values('L104', 'Dr. Hassan Mahmood',       '042-99211134', 'H104','Doctors Hospital, Lahore',                    '16 YEARS',  'Negative', 'Male',    7);
insert into doctor values('L105', 'Dr. Muhammad Naveed',      '042-99211134', 'H105','Health City Hospital, Lahore',                'Registrar', 'Negative', 'Male',   12);
insert into doctor values('F106', 'Dr. Madiha Abbas',         '042-99211134', 'H106','DHQ Hospital, Chiniot',                       '18 YEARS',  'Negative', 'Female', 10);
insert into doctor values('I107', 'Dr. Saleem Shahzad',       '042-99211134', 'H107','National Institute of Health, Islamabad',     '22 YEARS',  'Negative', 'Female',  5);
insert into doctor values('L108', 'Dr. Zeeshan Ali',          '042-99211134', 'H108','Hameed Latif Hospital, Lahore',               'Registrar', 'Negative', 'Male',   10);
insert into doctor values('L109', 'Dr. Khawar Abbas',         '042-32591427', 'H109','Health City Hospital, Lahore',                '25 YEARS',  'Negative', 'Male',    5);
insert into doctor values('L110', 'Dr. Muhammad Hanif Saqib', '042-32591427', 'H110','Iqra Medical Complex Hospital, Lahore',       '36 YEARS',  'Negative', 'Male',   10);
insert into doctor values('L111', 'Dr. Muhammad Taimour',     '042-32591427', 'H111','National Hospital, Lahore',                   '46 YEARS',  'Negative', 'Male',    5);
insert into doctor values('I112', 'Dr. Mirza Ayub Baig',      '042-32591427', 'H112','Life Care International Hospital, Islamabad', '10 YEARS',  'Negative', 'Male',    6);
insert into doctor values('I113', 'Prof. Dr. Shoaib Alam',    '042-32591427', 'H113','Shifa International Hospital Ltd, Islamabad', '26 YEARS',  'Negative', 'Male',   12);
insert into doctor values('F114', 'Dr. Muhammad Hussain',     '042-32591427', 'H114','Faisal Hospital, Faisalabad',                 '13 YEARS',  'Negative', 'Male',    7);
insert into doctor values('F115', 'Dr. Atif Mahmood',         '042-32591427', 'H115','The National Hospital, Faisalabad',           '35 YEARS',  'Negative', 'Male',    5);
insert into doctor values('L116', 'Dr. Javed Hayat Khan',     '042-32591427', 'H116','National Hospital & Medical Centre, Lahore',  '40 YEARS',  'Negative', 'Male',   12);
insert into doctor values('L117', 'Dr. Khawaja Yassir Rahman','042-32591427', 'H117','Fatima Memorial Hospital (Fmh), Lahore',      '27 YEARS',  'Negative', 'Male',   10);
insert into doctor values('L118', 'Dr. Hassan Gul',           '042-32591427', 'H118','Shalamar Hospital, Lahore',                   ' 9 YEARS',  'Negative', 'Male',   12);
insert into doctor values('L119', 'Dr. Syed Hussain Zaidi',   '042-32591427', 'H119','Faisal Hospital, Lahore',                     '25 YEARS',  'Negative', 'Male',    7);
insert into doctor values('L120', 'Dr. Shazia Awan',          '042-32591427', 'H120','Latif Hospital, Lahore',                      '10 YEARS',  'Negative', 'Female',  5);

--Nurse
CREATE TABLE NURSE(NURSE_ID VARCHAR(20) PRIMARY KEY, 
NURSE_NAME VARCHAR(20), NURSE_CONTACT VARCHAR(20),NURSE_HOSPITAL_ID VARCHAR(20), 
NURSE_WARD_NO VARCHAR(20), 
NURSE_CORONA_STATUS VARCHAR(20), N_GENDER VARCHAR(20), DUTY_HOURS INT, N_AGE INT);

insert into NURSE values('N101', 'Rabia Jameel',    '0306-6307801', 'H101', 'Block A', 'Negative', 'Female', 12, 28);
insert into NURSE values('N102', 'Arooj Zahid',     '0300-6949811', 'H102', 'Block B', 'Negative', 'Female', 12, 30);
insert into NURSE values('N103', 'Bint-e-Fatima',   '0306-6419641', 'H103', 'Block C', 'Negative', 'Female', 12, 22);
insert into NURSE values('N104', 'Ayesha Ahmad',    '0322-0000372', 'H104', 'Block D', 'Negative', 'Female', 12, 45);
insert into NURSE values('N105', 'Hafsa Zahid',     '0300-6350332', 'H105', 'Block A', 'Negative', 'Female', 12, 28);
insert into NURSE values('N106', 'Amna Noor',       '0333-5588994', 'H106', 'Block B', 'Negative', 'Female', 12, 25);
insert into NURSE values('N107', 'Esha Asif',       '0356-4440075', 'H107', 'Block C', 'Negative', 'Female', 12, 37);
insert into NURSE values('N108', 'Nayab Fatima',    '0333-6428041', 'H108', 'Block B', 'Negative', 'Female', 12, 28);
insert into NURSE values('N109', 'Anosh Faisal',    '0356-4440075', 'H109', 'Block A', 'Negative', 'Female', 12, 27);
insert into NURSE values('N110', 'Zainab Fatima',   '0333-5743554', 'H110', 'Block C', 'Negative', 'Female', 12, 28);
insert into NURSE values('N111', 'Shanza Sarfaraz', '0333-2480554', 'H111', 'Block B', 'Negative', 'Female', 12, 36);
insert into NURSE values('N112', 'Abida Ashghar',   '0333-1199922', 'H112', 'Block A', 'Negative', 'Female', 12, 28);
insert into NURSE values('N113', 'Ayesha Nadeem',   '0300-9762815', 'H113', 'Block C', 'Negative', 'Female', 12, 33);
insert into NURSE values('N114', 'Fateha Faisal',   '0356-0003432', 'H114', 'Block D', 'Negative', 'Female', 12, 26);
insert into NURSE values('N115', 'Amina Ahmad',     '0303-4378261', 'H115', 'Block B', 'Negative', 'Female', 12, 30);
insert into NURSE values('N116', 'Tahreem Umer',    '0306-2297440', 'H116', 'Block A', 'Negative', 'Female', 12, 28);
insert into NURSE values('N117', 'Umm-e-Rubab',     '0312-1001398', 'H117', 'Block C', 'Negative', 'Female', 12, 29);
insert into NURSE values('N118', 'Noor-Ul-Ain',     '0322-3654011', 'H118', 'Block A', 'Negative', 'Female', 12, 28);
insert into NURSE values('N119', 'Drousham Fatima', '0341-9200222', 'H119', 'Block D', 'Negative', 'Female', 12, 30);
insert into NURSE values('N120', 'Abeesha Habib',   '0308-2299642', 'H120', 'Block A', 'Negative', 'Female', 12, 25);


--Lab
CREATE TABLE LAB(L_NO VARCHAR(20) PRIMARY KEY, 
L_LOCATION VARCHAR(20), 
AVAILABLE_TESTKITS INT, 
NUM_OF_TEST_CONDUCTED INT, 
POSITIVE_COUNT INT, NEGATIVE_COUNT INT, 
L_INCHARGE_ID VARCHAR(20) FOREIGN KEY REFERENCES LAB_INCHARGE(LAB_INCHARGE_ID));

insert into lab values('L101', 'Block A', 12, 500, 300, 200, 'LI101');
insert into lab values('L102', 'Block B', 50, 650, 550, 100, 'LI102');
insert into lab values('L103', 'Block C', 70, 500, 300, 200, 'LI103');
insert into lab values('L104', 'Block D', 90, 700, 600, 100, 'LI104');
insert into lab values('L105', 'Block A', 100, 500, 300, 200, 'LI105');
insert into lab values('L106', 'Block B', 54, 550, 350, 200, 'LI106');
insert into lab values('L107', 'Block C', 76, 500, 300, 200, 'LI107');
insert into lab values('L108', 'Block B', 74, 1000, 850, 150, 'LI108');
insert into lab values('L109', 'Block A', 23, 400, 300, 10, 'LI109');
insert into lab values('L110', 'Block C', 63, 700, 500, 200, 'LI110');
insert into lab values('L111', 'Block B', 52, 500, 300, 200, 'LI111');
insert into lab values('L112', 'Block A', 75, 800, 700, 100, 'LI112');
insert into lab values('L113', 'Block C', 55, 500, 300, 200, 'LI113');
insert into lab values('L114', 'Block D', 75, 500, 300, 200, 'LI114');
insert into lab values('L115', 'Block B', 129, 500, 300, 200, 'LI115');
insert into lab values('L116', 'Block A', 120, 900, 700, 200, 'LI116');
insert into lab values('L117', 'Block C', 111, 2000, 1550, 450, 'LI117');
insert into lab values('L118', 'Block A', 100, 400, 300, 100, 'LI118');
insert into lab values('L119', 'Block D', 63, 1000, 800, 200, 'LI119');
insert into lab values('L120', 'Block A', 99, 500, 300, 200, 'LI120');

--Lab Incharge
CREATE TABLE LAB_INCHARGE(LAB_INCHARGE_ID VARCHAR(20) PRIMARY KEY, 
LAB_INCHARGE_NAME VARCHAR(20), LAB_INCHARGE_CONTACT VARCHAR(20), LAB_INCHARGE_ADDRESS VARCHAR(50));

insert into LAB_INCHARGE values('LI101', 'Saqib Hayyat',       '0306-6307801', '15-Roberts Road, Nila Gumbad, Lahore');
insert into LAB_INCHARGE values('LI102', 'Usman Ghoos',        '0300-6949811', 'Muslim Town, Jail Road, Faisalabad');
insert into LAB_INCHARGE values('LI103', 'Hamid Raza',         '0306-6419641', '7-Shami Road, Cantt, Lahore');
insert into LAB_INCHARGE values('LI104', 'Mubashir Luqman',    '0322-0000372', 'Fortress Stadium, Cantt, Lahore');
insert into LAB_INCHARGE values('LI105', 'Javed Hayyat',       '0300-6350332', '91 Midway S, Block H1, Wapda Town, Lahore');
insert into LAB_INCHARGE values('LI106', 'Bashir Ahmad',       '0313-4093625', '584-Jinnah Colony, Near Allied Bank, Faisalabad');
insert into LAB_INCHARGE values('LI107', 'Mughees Ismail',     '0306-5639261', 'Park Rd, Chak Shahzad, Islamabad Capital Territory');
insert into LAB_INCHARGE values('LI108', 'Wahab Raza',         '0333-2480554', '30-KM, Ferozpur Road, Lahore');
insert into LAB_INCHARGE values('LI109', 'Abid Jamil',         '0306-9946224', '91 Midway S, Block H1, Wapda Town, Lahore');
insert into LAB_INCHARGE values('LI110', 'Rizwan-Ul-Haq',      '0321-5220975', '220 Bawa Park, Uper Mall, Lahore');
insert into LAB_INCHARGE values('LI111', 'Muhammad Harris',    '0321-6121192', '132/3, Block L, DHA, Sports Stadium, DHA, Lahore');
insert into LAB_INCHARGE values('LI112', 'Shoaib Saleem',      '0300-4433864', 'G-10, Markaz, Sector G, Islamabad');
insert into LAB_INCHARGE values('LI113', 'Suffian Haider',     '0306-7832642', 'Pitras Bukhari Road, H-8/4, Sector H, Islamabad');
insert into LAB_INCHARGE values('LI114', 'Abdul Rehman',       '0356-4440075', '673-A, Canal Bank Road, Peoples Colony, Faisalabad');
insert into LAB_INCHARGE values('LI115', 'Asim Hameed',        '0303-4378261', '50-Gulistan Market, Railway Road, Faisalabad');
insert into LAB_INCHARGE values('LI116', 'Tahir Farooq',       '0306-2297440', '32/3, L-Block, DHA, Phase 1, DHA, Lahore');
insert into LAB_INCHARGE values('LI117', 'Zaid Iftikhar',      '0312-1001398', '16_d, PUNJ, Lahore');
insert into LAB_INCHARGE values('LI118', 'Awais Azam',         '0322-3654011', '2 Shalimar Link Road, Mughalpura, Lahore');
insert into LAB_INCHARGE values('LI119', 'Arfan Shahzad',      '0341-9200222', '23-B, Abu-Ul-Asphani Road, Faisal Town, Lahore');
insert into LAB_INCHARGE values('LI120', 'Sheheryar Munawar',  '0308-2299642', '66-a, Mozang Road, Lahore');


--Test
CREATE TABLE TEST(T_ID VARCHAR(20) PRIMARY KEY, 
TEST_DATE_N_TIME VARCHAR(40), 
LAB_NO VARCHAR(20) FOREIGN KEY REFERENCES LAB (L_NO), 
BLOOD_TYPE VARCHAR(20));

insert into test values('T101', '20-JUN-2020 06:00 AM', 'L101', 'A positive');
insert into test values('T102', '10-JUN-2020 08:03 AM', 'L102', 'B positive');
insert into test values('T103', '18-JUN-2020 03:00 AM', 'L103', 'AB negative');
insert into test values('T104', '11-JUN-2020 04:00 PM', 'L104', 'A negative');
insert into test values('T105', '20-JUN-2020 05:00 PM', 'L105', 'B negative');
insert into test values('T106', '09-JUN-2020 08:03 PM', 'L106', 'AB negative');
insert into test values('T107', '16-JUN-2020 08:00 AM', 'L107', 'A negative');
insert into test values('T108', '17-JUN-2020 07:30 AM', 'L108', 'B negative');
insert into test values('T109', '03-JUN-2020 08:03 AM', 'L109', 'A positive');
insert into test values('T110', '25-JUN-2020 07:00 AM', 'L110', 'B positive');
insert into test values('T111', '28-JUN-2020 08:00 AM', 'L111', 'B positive');
insert into test values('T112', '04-JUN-2020 09:03 AM', 'L112', 'A negative');
insert into test values('T113', '14-JUN-2020 10:00 PM', 'L113', 'AB positive');
insert into test values('T114', '21-JUN-2020 08:03 PM', 'L114', 'B negative');
insert into test values('T115', '14-JUN-2020 11:03 AM', 'L115', 'A positive');
insert into test values('T116', '15-JUN-2020 01:03 PM', 'L116', 'O positive');
insert into test values('T117', '06-JUN-2020 11:13 AM', 'L117', 'AB positive');
insert into test values('T118', '18-JUN-2020 05:03 PM', 'L118', 'B positive');
insert into test values('T119', '01-JUN-2020 12:03 PM', 'L119', 'A positive');
insert into test values('T120', '03-JUN-2020 11:00 AM', 'L120', 'O positive');


--Report
CREATE TABLE REPORT(T_NO VARCHAR(20) FOREIGN KEY REFERENCES TEST(T_ID), REPORT_GEN_DATE VARCHAR(20), REPORT_STATUS VARCHAR(20));

insert into REPORT values('T101', '20-JUN-2020 06:00 AM', 'Positive');
insert into REPORT values('T102', '10-JUN-2020 08:03 AM', 'Positive');
insert into REPORT values('T103', '18-JUN-2020 03:00 AM', 'Positive');
insert into REPORT values('T104', '11-JUN-2020 04:00 PM', 'Positive');
insert into REPORT values('T105', '20-JUN-2020 05:00 PM', 'Positive');
insert into REPORT values('T106', '09-JUN-2020 08:03 PM', 'Positive');
insert into REPORT values('T107', '16-JUN-2020 08:00 AM', 'Positive');
insert into REPORT values('T108', '17-JUN-2020 07:30 AM', 'Positive');
insert into REPORT values('T109', '03-JUN-2020 08:03 AM', 'Positive');
insert into REPORT values('T110', '25-JUN-2020 07:00 AM', 'Positive');
insert into REPORT values('T111', '28-JUN-2020 08:00 AM', 'Positive');
insert into REPORT values('T112', '04-JUN-2020 09:03 AM', 'Positive');
insert into REPORT values('T113', '14-JUN-2020 10:00 PM', 'Positive');
insert into REPORT values('T114', '21-JUN-2020 08:03 PM', 'Positive');
insert into REPORT values('T115', '14-JUN-2020 11:03 AM', 'Positive');
insert into REPORT values('T116', '15-JUN-2020 01:03 PM', 'Positive');
insert into REPORT values('T117', '06-JUN-2020 11:13 AM', 'Positive');
insert into REPORT values('T118', '18-JUN-2020 05:03 PM', 'Positive');
insert into REPORT values('T119', '01-JUN-2020 12:03 PM', 'Positive');
insert into REPORT values('T120', '03-JUN-2020 11:00 AM', 'Positive');

select * from HOSPITAL
select * from ward

select * from hospital
select * from patient

select * from hospital

alter table hospital alter column NUMBER_OF_DOCTORS varchar(100)
